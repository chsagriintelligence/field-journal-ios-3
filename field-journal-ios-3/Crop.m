//
//  Crop.m
//  Field Journal
//
//  Created by Elia Freedman on 2/9/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Crop.h"
#import "Session.h"
#import "FMDB.h"


@implementation Crop

- (id)initWithId:(NSInteger)dbId name:(NSString *)name {
    self = [super init];
    if (! self) return nil;
    
    _dbId = dbId;
    _name = ((NSNull *)name == [NSNull null] ? @"" : name);
    
    return self;
}


// store and update crops, returns array of error strings or empty array
+ (NSArray *)storeCrops:(NSArray *)crops {
    Session *session = [Session shared];
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *crop in crops) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM crops WHERE server_id=?", crop[@"id"]];
            if ([results next]) {
                // update the existing record
                [db executeUpdate:@"UPDATE crops SET name=?,variety=?,core_crop=? WHERE server_id=?", crop[@"name"], crop[@"variety"], crop[@"coreCrop"], results[@"server_id"]];
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO crops (server_id,name,variety,core_crop) VALUES (?,?,?,?)", crop[@"id"], crop[@"name"], crop[@"variety"], crop[@"coreCrop"]];
            }
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeCrops: %@", [db lastErrorMessage]]];
            [results close];
        }
    
    }];
    
    return errors;
}

// returns a Crop object given a database query
+ (Crop *)cropWithResultSet:(FMResultSet *)results {
    return [[Crop alloc] initWithId:[results[@"server_id"] integerValue] name:results[@"name"]];
}

// returns an array of all crop in name order
+ (NSArray <Crop *> *)allCrops {
    Session *session = [Session shared];
    NSMutableArray *crops = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM crops ORDER BY name ASC"];
        while ([results next]) {
            Crop *crop = [Crop cropWithResultSet:results];
            [crops addObject:crop];
        }
        [results close];
    
    }];
    
    return crops;
}

// returns a Crop object given a database id
+ (Crop *)cropForId:(NSInteger)cropId {
    Session *session = [Session shared];
    __block Crop *crop;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM crops WHERE server_id=?", @(cropId)];
        if ([results next]) {
            crop = [self cropWithResultSet:results];
        }
        [results close];
    
    }];
    
    return crop;
}

@end
