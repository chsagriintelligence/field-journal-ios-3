//
//  Image.m
//  Field Journal
//
//  Created by Elia Freedman on 1/21/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Image.h"
#import "Session.h"
#import "FMDB.h"
#import "UIImage+Image.h"
#import "NSDateFormatter+DateFormat.h"


@implementation Image

- (id)initWithId:(NSInteger)dbId filename:(NSString *)filename permissions:(NSString *)permissions {
    self = [super init];
    if (! self) return nil;
    
    _dbId = dbId;
    _filename = filename;
    _permissions = ((NSNull *)permissions == [NSNull null] ? [[Session shared] permissionForRecordType:RECORD_TYPE_IMAGES] : permissions);
    
    return self;
}

// delete image
- (void)destroy {
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM images WHERE id=?", idAsObject];
    if ([results next]) {
        if (results[@"server_id"] == [NSNull null]) {
            // hasn't been synced yet, remove it immediately
            [db executeUpdate:@"DELETE FROM images WHERE id=?", idAsObject];
        } else {
            // has been synced, need to alert the server
            [db executeUpdate:@"UPDATE images SET state='deleted' WHERE id=?", idAsObject];
            if ([db hadError]) NSLog(@"save error: %@", [db lastErrorMessage]);
        }
    }
    
    [Session closeDatabase];
}

// returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE
- (BOOL)hasPermissionForType:(NSString *)permissionType {
    if (! permissionType || ! self.permissions) return NO;
    return [self.permissions containsString:permissionType];
}


// create and return a new image given an observation database id and filename
+ (Image *)newImageForObservationId:(NSInteger)observationId filename:(NSString *)filename {
    NSString *date = [NSDateFormatter nowInUTC];
    NSNumber *observationIdAsObject = [NSNumber numberWithInteger:observationId];
    NSString *uuid = [filename stringByReplacingOccurrencesOfString:@".jpg" withString:@""]; //if we plan on supporting taking of png and jpg, need to add replacing .png as well
    NSDictionary *location = [[Session shared] currentLocation];
    NSInteger createdBy = [[Session shared] currentUser];
    
    Image *image;
    
    FMDatabase *db = [Session openDatabase];
    
    [db executeUpdate:@"INSERT INTO images (observation_id, uuid, filename, state, created_at, lat, lon, created_by, permissions) VALUES (?,?,?,?,?,?,?,?,?)", observationIdAsObject, uuid, filename, @"updated", date, location[@"latitude"], location[@"longitude"], @(createdBy), FULL_PERMISSIONS];
    FMResultSet *results = [db executeQuery:@"SELECT * FROM images WHERE uuid=?", uuid];
    if ([results next]) {
        image = [self imageWithResultSet:results];
    }
    
    [Session closeDatabase];
    
    return image;
}

// set record availability, returning an array of records needed
+ (NSArray *)setAvailability:(NSArray *)images {
    NSMutableArray *records = [[NSMutableArray alloc] initWithArray:images];
    FMDatabase *db = [Session openDatabase];
    
    // look at every record in the database, decide whether to keep it or remove it, whether to update its permissions, or whether to
    // request it from the server if it doesn't already exist or if the version has changed
    FMResultSet *results = [db executeQuery:@"SELECT * FROM images"];
    while ([results next]) {
        BOOL found = NO;
        for (NSInteger i = 0; i < records.count; i++) {
            NSDictionary *image = [records objectAtIndex:i];
            if (results[@"server_id"] != [NSNull null] && [results[@"server_id"] integerValue] == [image[@"id"] integerValue]) {
                [db executeUpdate:@"UPDATE images SET permissions=? WHERE server_id=?", image[@"r"], results[@"server_id"]];
                if (RECORD_VERSION_FOR_IMAGE == [results[@"version"] integerValue]) [records removeObjectAtIndex:i];
                found = YES;
                break;
            }
        }
        if (! found) [db executeUpdate:@"UPDATE images SET state='permission_removed' WHERE server_id=?", results[@"server_id"]];
    }
    
    [Session closeDatabase];
    
    // only return an array of ids
    NSMutableArray *neededRecords = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < records.count; i++) {
        NSDictionary *image = [records objectAtIndex:i];
        [neededRecords addObject:image[@"id"]];
    }
    
    return neededRecords;
}

// store and update images for an observation, returns array of error strings or empty array
+ (NSArray *)storeImages:(NSArray *)images {
    FMResultSet *results;
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    for (NSDictionary *image in images) {
        NSString *uuid = [image[@"clientId"] uppercaseString];
        NSNumber *uploaded = [NSNumber numberWithBool:[image[@"uploaded"] boolValue]];
        NSNumber *creator = (image[@"createdBy"] == [NSNull null] ? @0 : image[@"createdBy"]);
        
        FMResultSet *observation = [db executeQuery:@"SELECT * FROM observations WHERE server_id=?", image[@"parentId"]];
        if (! [observation next]) {
            [errors addObject:[NSString stringWithFormat:@"storeImages: No observation with server id %@ for image with server id %@", image[@"parentId"], image[@"id"]]];
            continue;
        }

        results = [db executeQuery:@"SELECT * FROM images WHERE uuid=?", uuid];
        if ([results next]) {
            // update the existing record
            if ([results[@"modified_during_sync"] boolValue]) {
                [db executeUpdate:@"UPDATE images SET server_id=? WHERE id=?", image[@"id"], results[@"id"]];
            } else {
                [db executeUpdate:@"UPDATE images SET server_id=?, uuid=?, observation_id=?, state=?, created_at=?, filename=?, uploaded=?, lat=?, lon=?, created_by=?, permissions=?, version=? WHERE id=?", image[@"id"], uuid, observation[@"id"], @"", image[@"date"], image[@"fileName"], uploaded, image[@"lat"], image[@"lon"], creator, image[@"r"], @(RECORD_VERSION_FOR_IMAGE), results[@"id"]];
            }

            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeImages: %@", [db lastErrorMessage]]];
        } else {
            // no record found, add a new one
            [db executeUpdate:@"INSERT INTO images (server_id, uuid, observation_id, state, created_at, filename, uploaded, lat, lon, created_by, permissions, version) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", image[@"id"], uuid, observation[@"id"], @"", image[@"date"], image[@"fileName"], uploaded, image[@"lat"], image[@"lon"], creator, image[@"r"], @(RECORD_VERSION_FOR_IMAGE)];
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeImages: %@", [db lastErrorMessage]]];
        }
    }
    
    [Session closeDatabase];
    
    return errors;
}

// Returns an array of modified images or nil if there are none. Modified images are those with a state of deleted with no server_id or state marked updated.
+ (NSArray *)modifiedImages {
    NSMutableArray *modifiedRecords = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM images WHERE state='updated'"];
    while ([results next]) {
        NSMutableDictionary *record = [[NSMutableDictionary alloc] init];
        
        record[@"id"] = results[@"server_id"];
        record[@"clientId"] = results[@"uuid"];
        FMResultSet *observationResults = [db executeQuery:@"SELECT * FROM observations WHERE id=?", results[@"observation_id"]];
        if ([observationResults next]) record[@"parentId"] = observationResults[@"uuid"];
        record[@"fileName"] = results[@"filename"];
        record[@"uploaded"] = results[@"uploaded"];
        record[@"lat"] = results[@"lat"];
        record[@"lon"] = results[@"lon"];
        record[@"date"] = results[@"created_at"];
        
        [modifiedRecords addObject:record];
    }
    
    [Session closeDatabase];
    
    return ([modifiedRecords count] == 0 ? nil : modifiedRecords);
}

// Returns an array of deleted image server ids or nil if there are none. Will only return records where the state is marked 'deleted' and there is a server_id.
// Records without a server id are handled with a special upload flag.
+ (NSArray *)deletedImages {
    NSMutableArray *deletedRecords = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM images WHERE state='deleted'"];
    while ([results next]) {
        [deletedRecords addObject:results[@"server_id"]];
    }
    
    [Session closeDatabase];

    return ([deletedRecords count] == 0 ? nil : deletedRecords);
}

// returns an Image object given a database query
+ (Image *)imageWithResultSet:(FMResultSet *)results {
    return [[Image alloc] initWithId:[results[@"id"] integerValue] filename:results[@"filename"] permissions:results[@"permissions"]];
}

// delete images
+ (void)deleteImages:(NSArray <NSNumber *> *)images {
    FMResultSet *results;
    
    FMDatabase *db = [Session openDatabase];
    
    for (NSDictionary *imageId in images) {
        results = [db executeQuery:@"SELECT * FROM images WHERE server_id=?", imageId];
        if ([results next]) {
            [UIImage deleteImage:results[@"filename"]];
            [db executeUpdate:@"DELETE FROM images WHERE id=?", results[@"id"]];
        }
    }
    
    results = [db executeQuery:@"SELECT * FROM images WHERE state='permission_removed'"];
    while ([results next]) {
        [UIImage deleteImage:results[@"filename"]];
    }
    [db executeUpdate:@"DELETE FROM images WHERE state='permission_removed'"];
    
    [Session closeDatabase];
}

// deletes all images with a given observation id
+ (void)deleteImagesForObservation:(NSNumber *)observationId {
    FMDatabase *db = [Session openDatabase];
    FMResultSet *results = [db executeQuery:@"SELECT * FROM images WHERE observation_id=?", observationId];
    while ([results next]) {
        [UIImage deleteImage:results[@"filename"]];
        [db executeUpdate:@"DELETE FROM images WHERE id=?", results[@"id"]];
    }
    [Session closeDatabase];
}

// clear the modified_during_state flags on all records
+ (void)clearModifiedDuringSync {
    NSNumber *trueAsObject = [NSNumber numberWithBool:YES];
    NSNumber *falseAsObject = [NSNumber numberWithBool:NO];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM images WHERE modified_during_sync=?", trueAsObject];
    while ([results next]) {
        [db executeUpdate:@"UPDATE images SET modified_during_sync=? WHERE id=?", falseAsObject, results[@"id"]];
    }
    
    [Session closeDatabase];
}

// returns an array of dictionaries comprising server_id and filename keys of images that need to be uploaded
+ (NSMutableArray *)imagesToUpload {
    NSMutableArray *images = [[NSMutableArray alloc] init];
    NSNumber *falseAsObject = [NSNumber numberWithBool:NO];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM images WHERE uploaded=?", falseAsObject];
    while ([results next]) {
        BOOL imageExists = [UIImage imageExists:results[@"filename"]];
        if (imageExists) [images addObject:@{@"server_id" : results[@"server_id"], @"filename" : results[@"filename"]}];
    }
    
    [Session closeDatabase];
    
    return images;
}

// handles upload success for an image with serverId
+ (void)imageUploadSucceeded:(NSInteger)serverId {
    NSNumber *serverIdAsObject = [NSNumber numberWithInteger:serverId];
    NSNumber *trueAsObject = [NSNumber numberWithBool:YES];
    
    FMDatabase *db = [Session openDatabase];
    [db executeUpdate:@"UPDATE images SET uploaded=? WHERE server_id=?", trueAsObject, serverIdAsObject];
    [Session closeDatabase];
}

// returns an array of dictionaries comprising server_id and filename keys of images that need to be downloaded
+ (NSMutableArray *)imagesToDownload {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *imagesInFileManager = [fileManager contentsOfDirectoryAtPath:[[Session shared] imagesPath] error:nil];
    NSMutableArray *imagesToDownload = [[NSMutableArray alloc] init];
    NSNumber *trueAsObject = [NSNumber numberWithBool:YES];
    BOOL found;
    NSString *imageInDB;
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM images WHERE uploaded=?", trueAsObject];
    while ([results next]) {
        found = NO;
        imageInDB = results[@"filename"];
        for (NSString *imageInFM in imagesInFileManager) {
            if ([imageInDB isEqualToString:imageInFM]) {
                found = YES;
                break;
            }
        }
        if (! found) [imagesToDownload addObject:@{@"server_id" : results[@"server_id"], @"filename" : imageInDB, @"imageType" : @"observation"}];
    }
    
    [Session closeDatabase];
    
    return imagesToDownload;
}

// returns the images for a given observation id
+ (NSArray <Image *> *)imagesForObservationId:(NSInteger)observationId {
    NSMutableArray *images = [[NSMutableArray alloc] init];
    NSNumber *observationIdAsObject = [NSNumber numberWithInteger:observationId];
    Image *image;
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM images WHERE observation_id=? AND state != 'deleted' ORDER BY created_at", observationIdAsObject];
    while ([results next]) {
        image = [self imageWithResultSet:results];
        [images addObject:image];
    }
    
    [Session closeDatabase];
    
    return images;
}

@end
