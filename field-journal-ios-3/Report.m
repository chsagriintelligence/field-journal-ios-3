//
//  Report.m
//  Field Journal
//
//  Created by Elia Freedman on 1/20/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Report.h"
#import "Observation.h"
#import "User.h"
#import "Publish.h"
#import "Field.h"
#import "Session.h"
#import "FMDB.h"
#import "SyncExtras.h"
#import "NSDateFormatter+DateFormat.h"
#import "NSString+String.h"
#import "NSDictionary+Dictionary.h"
#import "NSArray+Array.h"
#import "Coop.h"


@implementation Report

- (id)initWithId:(NSInteger)dbId name:(NSString *)name date:(NSString *)date creator:(NSInteger)creator stage:(NSArray *)stage irrigation:(NSString *)irrigation weather:(NSString *)weather temperature:(NSNumber *)temperature recommendation:(NSString *)recommendation instructions:(NSString *)instructions fieldStatusId:(NSInteger)fieldStatusId fieldId:(NSInteger)fieldId permissions:(NSString *)permissions published:(NSDictionary *)published {
    self = [super init];
    if (! self) return nil;
    
    _dbId = dbId;
    _name = ((NSNull *)name == [NSNull null] ? nil : name);
    _date = date;
    _creator = [User userForId:creator];
    _stage = (! stage || (NSNull *)stage == [NSNull null] ? @[] : stage);
    _irrigation = ((NSNull *)irrigation == [NSNull null] ? nil : irrigation);
    _weather = ((NSNull *)weather == [NSNull null] ? nil : weather);
    _temperature = ((NSNull *)temperature == [NSNull null] ? @"" : [temperature stringValue]);
    _recommendation = ((NSNull *)recommendation == [NSNull null] ? nil : recommendation);
    _instructions = ((NSNull *)instructions == [NSNull null] ? nil : instructions);
    _permissions = ((NSNull *)permissions == [NSNull null] ? [[Session shared] permissionForRecordType:RECORD_TYPE_REPORTS] : permissions);
    
    _fieldId = fieldId;
    Field *field = [Field fieldForId:fieldId];
    _fieldBoundary = field.boundary;
    _fieldServerId = field.serverId;
    
    FieldStatus *fieldStatus = [FieldStatus fieldStatusForId:fieldStatusId];
    _fieldStatus = (! fieldStatus ? FieldStatusNotFound : fieldStatus.status);
    
    _publishToGrower = (published && published[@"grower"] ? [published[@"grower"] boolValue] : NO);
    _publishToIntel = (published && published[@"intel"] ? [published[@"intel"] boolValue] : NO);
    
    _observations = [Observation observationsForReportId:dbId lockedOnly:NO];
    
    return self;
}

- (id)initWithId:(NSInteger)dbId name:(NSString *)name date:(NSString *)date fieldStatusId:(NSInteger)fieldStatusId fieldId:(NSInteger)fieldId permissions:(NSString *)permissions {
    self = [super init];
    if (! self) return nil;

    _dbId = dbId;
    _name = ((NSNull *)name == [NSNull null] ? nil : name);
    _date = date;
    _permissions = ((NSNull *)permissions == [NSNull null] ? [[Session shared] permissionForRecordType:RECORD_TYPE_REPORTS] : permissions);

    _fieldId = fieldId;
    FieldStatus *fieldStatus = [FieldStatus fieldStatusForId:fieldStatusId];
    _fieldStatus = (! fieldStatus ? FieldStatusNotFound : fieldStatus.status);
    
    return self;
}

// save report changes
- (void)save {
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
    NSNumber *temperature = (self.temperature.length == 0 ? (NSNumber *)[NSNull null] : [NSNumber numberWithFloat:[self.temperature floatValue]]);
    FieldStatus *fieldStatus = [FieldStatus fieldStatusForStatus:self.fieldStatus];
    NSNumber *fieldStatusAsObject = (! fieldStatus ? (NSNumber *)[NSNull null] : [NSNumber numberWithInteger:fieldStatus.dbId]);
    NSDictionary *publish = @{@"grower" : @(self.publishToGrower), @"intel" : @(self.publishToIntel)};
    NSString *publishString = [publish convertJSONDictionaryToString];
    NSString *stageString = [self.stage convertJSONArrayToString];
    
    FMDatabase *db = [Session openDatabase];
    
    [db executeUpdate:@"UPDATE reports SET state='updated', stage=?, irrigation=?, weather=?, temperature=?, recommendation=?, published=?, fieldstatus_id=? WHERE id=?", stageString, self.irrigation, self.weather, temperature, self.recommendation, publishString, fieldStatusAsObject, idAsObject];
    if ([db hadError]) {
        NSLog(@"save error: %@", [db lastErrorMessage]);
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_REPORTS object:nil];
    }
    
    [Session closeDatabase];
}

// updates the field given a new fieldId
- (void)updateField:(NSInteger)fieldId {
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
    NSNumber *fieldIdAsObject = [NSNumber numberWithInteger:fieldId];

    FMDatabase *db = [Session openDatabase];
    
    [db executeUpdate:@"UPDATE reports SET state='updated', field_id=? WHERE id=?", fieldIdAsObject, idAsObject];
    if ([db hadError]) {
        NSLog(@"save error: %@", [db lastErrorMessage]);
    } else {
        self.fieldId = fieldId;
        Field *field = [Field fieldForId:fieldId];
        self.fieldBoundary = field.boundary;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_REPORTS object:nil];
    }
    
    [Session closeDatabase];
}

// delete report
- (void)destroy {
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM reports WHERE id=?", idAsObject];
    if ([results next]) {
        if (results[@"server_id"] == [NSNull null]) {
            // hasn't been synced yet, remove it immediately
            [db executeUpdate:@"DELETE FROM reports WHERE id=?", idAsObject];
        } else {
            // has been synced, need to alert the server
            [db executeUpdate:@"UPDATE reports SET state='deleted' WHERE id=?", idAsObject];
            if ([db hadError]) NSLog(@"save error: %@", [db lastErrorMessage]);
        }
        [Observation deleteObservationsForReport:idAsObject];
    }
    
    [Session closeDatabase];
}

// returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE
- (BOOL)hasPermissionForType:(NSString *)permissionType {
    if (! permissionType || ! self.permissions) return NO;
    return [self.permissions containsString:permissionType];
}

// returns an array of Publish objects
- (NSArray <Publish *> *)publishableArray {
    NSMutableArray *array = [[NSMutableArray alloc] init];
    Publish *publish;
    Session *session = [Session shared];
    
    if ([session hasCreatePermissionsForType:RECORD_TYPE_REPORT_PUBLISH_TO_GROWER]) {
        publish = [[Publish alloc] initWithType:RECORD_TYPE_REPORT_PUBLISH_TO_GROWER title:@"Publish to Grower" published:self.publishToGrower];
        [array addObject:publish];
    }

    if ([session hasCreatePermissionsForType:RECORD_TYPE_REPORT_PUBLISH_TO_INTEL]) {
        publish = [[Publish alloc] initWithType:RECORD_TYPE_REPORT_PUBLISH_TO_INTEL title:@"Publish to Intel" published:self.publishToIntel];
        [array addObject:publish];
    }

    return array;
}

// returns a count of Publish options
- (NSInteger)publishCount {
    NSInteger count = 0;
    Session *session = [Session shared];
    if ([session hasCreatePermissionsForType:RECORD_TYPE_REPORT_PUBLISH_TO_GROWER]) count += 1;
    if ([session hasCreatePermissionsForType:RECORD_TYPE_REPORT_PUBLISH_TO_INTEL]) count += 1;
    return count;
}


// create and return a new report for the field with database id fieldId
+ (Report *)newReportForFieldId:(NSInteger)fieldId {
    NSString *date = [NSDateFormatter nowInUTC];
    NSNumber *fieldIdAsObject = [NSNumber numberWithInteger:fieldId];
    NSString *uuid = [NSString nameForFileType:nil];
    NSDictionary *publish = @{@"grower" : @NO, @"intel" : @NO};
    NSString *publishString = [publish convertJSONDictionaryToString];
    NSInteger createdBy = [[Session shared] currentUser];
    NSString *stagesString = [@[] convertJSONArrayToString];
    Report *report;
    
    FMDatabase *db = [Session openDatabase];

    [db executeUpdate:@"INSERT INTO reports (field_id, uuid, state, created_at, stage, permissions, type, published, created_by) VALUES (?,?,?,?,?,?,?,?,?)", fieldIdAsObject, uuid, @"updated", date, stagesString, FULL_PERMISSIONS, @"F", publishString, @(createdBy)];
    FMResultSet *results = [db executeQuery:@"SELECT * FROM reports WHERE uuid=?", uuid];
    if ([results next]) {
        report = [[Report alloc] initWithId:[results[@"id"] integerValue] name:@"" date:results[@"created_at"] creator:[results[@"created_by"] integerValue] stage:@[] irrigation:@"" weather:@"" temperature:nil recommendation:@"" instructions:@"" fieldStatusId:NSNotFound fieldId:fieldId permissions:results[@"permissions"] published:publish];
    }
    
    [Session closeDatabase];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_REPORTS object:nil];
    
    return report;
}

// set record availability, returning an array of records needed
+ (NSArray *)setAvailability:(NSArray *)reports {
    NSMutableArray *records = [[NSMutableArray alloc] initWithArray:reports];
    FMDatabase *db = [Session openDatabase];
    
    // look at every record in the database, decide whether to keep it or remove it, whether to update its permissions, or whether to
    // request it from the server if it doesn't already exist or if the version has changed
    FMResultSet *results = [db executeQuery:@"SELECT * FROM reports"];
    while ([results next]) {
        BOOL found = NO;
        for (NSInteger i = 0; i < records.count; i++) {
            NSDictionary *report = [records objectAtIndex:i];
            if (results[@"server_id"] != [NSNull null] && [results[@"server_id"] integerValue] == [report[@"id"] integerValue]) {
                [db executeUpdate:@"UPDATE reports SET permissions=? WHERE server_id=?", report[@"r"], results[@"server_id"]];
                if (RECORD_VERSION_FOR_REPORT == [results[@"version"] integerValue]) [records removeObjectAtIndex:i];
                found = YES;
                break;
            }
        }
        if (! found) [db executeUpdate:@"UPDATE reports SET state='permission_removed' WHERE server_id=?", results[@"server_id"]];
    }
    
    [Session closeDatabase];
    
    // only return an array of ids
    NSMutableArray *neededRecords = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < records.count; i++) {
        NSDictionary *report = [records objectAtIndex:i];
        [neededRecords addObject:report[@"id"]];
    }
    
    return neededRecords;
}

// store and update reports, assumes fields have been stored first, returns array of error strings or empty array
+ (NSArray *)storeReports:(NSArray *)reports {
    FMResultSet *results;
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    FMDatabase *db = [Session openDatabase];

    for (NSDictionary *report in reports) {
        FMResultSet *field = [db executeQuery:@"SELECT * FROM fields WHERE server_id=?", report[@"field"]];
        if (! [field next]) {
            [errors addObject:[NSString stringWithFormat:@"storeReports: No field with server id %@ for report with server id %@", report[@"field"], report[@"id"]]];
            continue;
        }
        
        FMResultSet *fieldstatus = [db executeQuery:@"SELECT * FROM fieldstatuses WHERE server_id=?", report[@"fieldStatus"]];
        NSString *fieldstatusId;
        if ([fieldstatus next]) fieldstatusId = fieldstatus[@"id"];
        
        NSString *uuid = [report[@"clientId"] uppercaseString];
        NSString *tags = [SyncExtras stringForTags:report[@"tags"] database:db];
        NSString *stage = [report[@"cropStages"] convertJSONArrayToString];
        NSString *weather = (! report[@"weather"] ? @"" : report[@"weather"]);
        NSString *irrigation = (! report[@"irrigation"] ? @"" : report[@"irrigation"]);
        NSString *recommendation = (! report[@"recommendation"] ? @"" : report[@"recommendation"]);
        NSDictionary *published = @{@"intel" : report[@"publishedToIntel"], @"grower" : report[@"publishedToGrower"]};
        NSString *publishedString = [published convertJSONDictionaryToString];
        NSNumber *creator = (report[@"createdBy"] == [NSNull null] ? @0 : report[@"createdBy"]);
        
        results = [db executeQuery:@"SELECT * FROM reports WHERE uuid=?", uuid];
        if ([results next]) {
            // update the existing record
            if ([results[@"modified_during_sync"] boolValue]) {
                [db executeUpdate:@"UPDATE reports SET server_id=? WHERE id=?", report[@"id"], results[@"id"]];
            } else {
                [db executeUpdate:@"UPDATE reports SET server_id=?, field_id=?, uuid=?, state=?, created_at=?, stage=?, weather=?, irrigation=?, recommendation=?, temperature=?, fieldstatus_id=?, tags=?, type=?, name=?, instructions=?, published=?, created_by=?, permissions=?, version=? WHERE id=?", report[@"id"], field[@"id"], uuid, @"", report[@"date"], stage, weather, irrigation, recommendation, report[@"temperature"], fieldstatusId, tags, report[@"type"], report[@"name"], report[@"instructions"], publishedString, creator, report[@"r"], @(RECORD_VERSION_FOR_REPORT), results[@"id"]];
            }
            
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeReports: %@", [db lastErrorMessage]]];
        } else {
            // no record found, add a new one
            [db executeUpdate:@"INSERT INTO reports (server_id, field_id, uuid, created_at, stage, weather, irrigation, recommendation, temperature, fieldstatus_id, tags, type, permissions, name, instructions, published, created_by, version) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", report[@"id"], field[@"id"], uuid, report[@"date"], stage, weather, irrigation, recommendation, report[@"temperature"], fieldstatusId, tags, report[@"type"], report[@"r"], report[@"name"], report[@"instructions"], publishedString, creator, @(RECORD_VERSION_FOR_REPORT)];
            if ([db hadError]) {
                [errors addObject:[NSString stringWithFormat:@"storeReports: %@", [db lastErrorMessage]]];
            }
        }
    }
    
    [Session closeDatabase];
    
    return errors;
}

// Returns an array of modified reports or nil if there are none. Modified reports are those with a state of deleted with no server_id or state marked updated.
+ (NSArray *)modifiedReports {
    NSMutableArray *modifiedRecords = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM reports WHERE state='updated'"];
    while ([results next]) {
        NSMutableDictionary *record = [[NSMutableDictionary alloc] init];
        
        record[@"id"] = results[@"server_id"];
        record[@"type"] = results[@"type"];
        record[@"clientId"] = results[@"uuid"];
        FMResultSet *fieldResults = [db executeQuery:@"SELECT * FROM fields WHERE id=?", results[@"field_id"]];
        if ([fieldResults next]) record[@"field"] = fieldResults[@"uuid"];
        record[@"date"] = results[@"created_at"];
        record[@"cropStages"] = [NSArray arrayFromJSONString:results[@"stage"]];
        record[@"weather"] = results[@"weather"];
        record[@"temperature"] = results[@"temperature"];
        record[@"irrigation"] = results[@"irrigation"];
        record[@"recommendation"] = results[@"recommendation"];
        NSDictionary *published = [NSDictionary dictionaryFromJSONString:results[@"published"]];
        record[@"publishedToIntel"] = published[@"intel"];
        record[@"publishedToGrower"] = published[@"grower"];
        FMResultSet *fieldStatusResults = [db executeQuery:@"SELECT * FROM fieldstatuses WHERE id=?", results[@"fieldstatus_id"]];
        if ([fieldStatusResults next]) record[@"fieldStatus"] = fieldStatusResults[@"server_id"];
        
        [modifiedRecords addObject:record];
    }
    
    [Session closeDatabase];

    return ([modifiedRecords count] == 0 ? nil : modifiedRecords);
}

// Returns an array of deleted report server ids or nil if there are none. Will only return records where the state is marked 'deleted' and there is a server_id.
// Records without a server id are handled with a special upload flag.
+ (NSArray *)deletedReports {
    NSMutableArray *deletedRecords = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM reports WHERE state='deleted'"];
    while ([results next]) {
        [deletedRecords addObject:results[@"server_id"]];
    }

    [Session closeDatabase];

    return ([deletedRecords count] == 0 ? nil : deletedRecords);
}

// returns a Report object given a database query
+ (Report *)reportWithResultSet:(FMResultSet *)results {
    NSInteger fieldStatusId = (results[@"fieldstatus_id"] == [NSNull null] ? NSNotFound : [results[@"fieldstatus_id"] integerValue]);
    NSDictionary *publish = [NSDictionary dictionaryFromJSONString:results[@"published"]];
    NSArray *stage = results[@"stage"] == [NSNull null] ? @[] : [NSArray arrayFromJSONString:results[@"stage"]];
    return [[Report alloc] initWithId:[results[@"id"] integerValue] name:results[@"name"] date:results[@"created_at"] creator:[results[@"created_by"] integerValue] stage:stage irrigation:results[@"irrigation"] weather:results[@"weather"] temperature:results[@"temperature"] recommendation:results[@"recommendation"] instructions:results[@"instructions"] fieldStatusId:fieldStatusId fieldId:[results[@"field_id"] integerValue] permissions:results[@"permissions"] published:publish];
}

// returns a Report object that contains only a subset of data from the query
+ (Report *)reportWithResultSubset:(FMResultSet *)results {
    NSInteger fieldStatusId = (results[@"fieldstatus_id"] == [NSNull null] ? NSNotFound : [results[@"fieldstatus_id"] integerValue]);
    return [[Report alloc] initWithId:[results[@"id"] integerValue] name:results[@"name"] date:results[@"created_at"] fieldStatusId:fieldStatusId fieldId:[results[@"field_id"] integerValue] permissions:results[@"permissions"]];
}

// delete reports
+ (void)deleteReports:(NSArray *)reports {
    FMResultSet *results;
    FMDatabase *db = [Session openDatabase];
    
    for (NSNumber *reportId in reports) {
        results = [db executeQuery:@"SELECT * FROM reports WHERE server_id=?", reportId];
        if ([results next]) {
            [Observation deleteObservationsForReport:results[@"id"]];
            [db executeUpdate:@"DELETE FROM reports WHERE id=?", results[@"id"]];
        }
    }
    
    results = [db executeQuery:@"SELECT * FROM reports WHERE state='permission_removed'"];
    while ([results next]) {
        [Observation deleteObservationsForReport:results[@"id"]];
        [db executeUpdate:@"DELETE FROM reports WHERE id=?", results[@"id"]];
    }

    [Session closeDatabase];
}

// deletes all reports with a given field id
+ (void)deleteReportsForField:(NSNumber *)fieldId {
    FMDatabase *db = [Session openDatabase];
    FMResultSet *results = [db executeQuery:@"SELECT * FROM reports WHERE field_id=?", fieldId];
    while ([results next]) {
        [Observation deleteObservationsForReport:results[@"id"]];
        [db executeUpdate:@"DELETE FROM reports WHERE id=?", results[@"id"]];
    }
    [Session closeDatabase];
}

// clear the modified_during_state flags on all records
+ (void)clearModifiedDuringSync {
    NSNumber *trueAsObject = [NSNumber numberWithBool:YES];
    NSNumber *falseAsObject = [NSNumber numberWithBool:NO];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM reports WHERE modified_during_sync=?", trueAsObject];
    while ([results next]) {
        [db executeUpdate:@"UPDATE reports SET modified_during_sync=? WHERE id=?", falseAsObject, results[@"id"]];
    }
    
    [Session closeDatabase];
}

// returns a count of Task reports that are not deleted and not locked
// if coopId != NSNotFound, will also filter by coop and only count reports for fields whose farm is in that coop
+ (NSUInteger)countOfTaskReportsForCoopId:(NSInteger)coopId {
    NSUInteger count = 0;
    FMDatabase *db = [Session openDatabase];
    FMResultSet *results = [db executeQuery:[Report queryFor:REPORT_TYPE_TASKS fieldId:nil countOnly:NO]];
    while ([results next]) {
        Report *report = [self reportWithResultSubset:results];
        if (coopId == NSNotFound || [Coop coopIdForFieldId:report.fieldId] == coopId) ++count;
    }
    [Session closeDatabase];
    
    return count;
}

// returns a database query string to retrieve report records according to report type and possibly field id, user id, and/or date period as needed. If
// countOnly is true then returns a query string for performing a count of records only.
+ (NSString *)queryFor:(BOOL)isTaskType fieldId:(NSNumber *)fieldId countOnly:(BOOL)countOnly {
    Session *session = [Session shared];
    NSString *query;
    if (countOnly) {
        query = [NSString stringWithFormat:@"SELECT COUNT(id) FROM reports WHERE state!='deleted' AND locked=%@ AND type!='F'", @NO];
    } else {
        query = [NSString stringWithFormat:@"SELECT * FROM reports WHERE state!='deleted' and type%@'F'", (isTaskType ? @"!=" : @"=")];
    }
    
    if (fieldId) query = [NSString stringWithFormat:@"%@ and field_id=%@", query, fieldId];
    
    if ([session filterReportsToUser]) query = [NSString stringWithFormat:@"%@ and created_by=%@", query, @([session currentUser])];
    
    NSString *dates = [session filterReportsByDate];
    if (! [dates isEqualToString:REPORT_FILTER_DATE_ALL]) {
        NSDate *since;
        if ([dates isEqualToString:REPORT_FILTER_DATE_JAN_1]) {
            // get reports from 1/1/thisYear according to device's current local timezone
            NSCalendar *cal = [NSCalendar currentCalendar];
            NSDateComponents *comps = [cal components:NSCalendarUnitYear fromDate:[NSDate date]];
            [comps setMonth:1];
            [comps setDay:1];
            since = [cal dateFromComponents:comps]; // device local midnight of 1/1/thisYear will be translated to UTC for query below
        } else {
            // get reports for past X-many days before now
            NSInteger days = 0;
            if ([dates isEqualToString:REPORT_FILTER_DATE_7_DAYS]) days = -7;
            else if ([dates isEqualToString:REPORT_FILTER_DATE_30_DAYS]) days = -30;
            else if ([dates isEqualToString:REPORT_FILTER_DATE_90_DAYS]) days = -90;
            since = [NSDate dateWithTimeIntervalSinceNow:days * 24.0 * 60 * 60];    // interval in seconds
        }
        query = [NSString stringWithFormat:@"%@ and created_at >= '%@'", query, [NSDateFormatter timestampInUTCfor:since]];
    }
    
    return [query stringByAppendingString:@" ORDER BY created_at DESC"];
}

// returns an array of reports for either Fields or Tasks type that are not deleted in Date order
// if coopId != NSNotFound, will also filter by coop and only return reports for fields whose farm is in that coop
+ (NSArray <Report *> *)allReportsForTypeInDateOrder:(NSUInteger)type coopId:(NSInteger)coopId {
    NSMutableArray *reports = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:[Report queryFor:(type == REPORT_TYPE_TASKS) fieldId:nil countOnly:NO]];
    while ([results next]) {
        Report *report = [self reportWithResultSubset:results];
        if (coopId == NSNotFound || [Coop coopIdForFieldId:report.fieldId] == coopId) [reports addObject:report];
    }
    
    [Session closeDatabase];
    
    return reports;
}

// returns an array of reports for either Fields or Tasks type that are not deleted grouped by Field
// if coopId != NSNotFound, will also filter by coop and only return reports for fields whose farm is in that coop
+ (NSArray <NSDictionary *> *)allReportsForTypeInFieldOrder:(NSUInteger)type coopId:(NSInteger)coopId {
    NSMutableArray *reports = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    NSArray *fields = [Field allFieldsWithNamesOnly:YES];
    
    for (Field *field in fields) {
        if (coopId == NSNotFound || [Coop coopIdForFieldId:field.dbId] == coopId) {
            NSMutableArray *reportsForField = [[NSMutableArray alloc] init];

            FMResultSet *results = [db executeQuery:[Report queryFor:(type == REPORT_TYPE_TASKS) fieldId:@(field.dbId) countOnly:NO]];
            while ([results next]) {
                Report *report = [self reportWithResultSubset:results];
                [reportsForField addObject:report];
            }
            
            if (reportsForField.count > 0) [reports addObject:@{@"field" : field, @"reports" : reportsForField}];
        }
    }
    
    [Session closeDatabase];
    
    return reports;
}

// returns a date order array (descending) of Report objects for a given field
+ (NSArray <Report *> *)allReportsForFieldId:(NSInteger)fieldId {
    NSNumber *fieldIdAsObject = [NSNumber numberWithInteger:fieldId];
    NSNumber *falseAsObject = [NSNumber numberWithBool:NO];
    NSMutableArray *allReports = [[NSMutableArray alloc] init];
    NSMutableArray *reports = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM reports WHERE field_id=? AND locked=? AND state != 'deleted' ORDER BY created_at DESC", fieldIdAsObject, falseAsObject];
    while ([results next]) {
        Report *report = [self reportWithResultSubset:results];
        [allReports addObject:report];
    }
    
    // remove all reports that do not offer update permissions
    for (Report *report in allReports) {
        if ([report hasPermissionForType:PERMISSION_UPDATE]) {
            [reports addObject:report];
        }
    }
    
    [Session closeDatabase];
    
    return reports;
}

// returns a Report object given a database id
+ (Report *)reportForId:(NSInteger)reportId {
    NSNumber *reportIdAsObject = [NSNumber numberWithInteger:reportId];
    Report *report;
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM reports WHERE id=? AND state != 'deleted'", reportIdAsObject];
    if ([results next]) {
        report = [self reportWithResultSet:results];
    }
    
    [Session closeDatabase];
    
    return report;
}

@end



