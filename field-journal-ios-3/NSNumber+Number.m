//
//  NSNumber+Number.m
//  Field Journal
//
//  Created by Elia Freedman on 2/3/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "NSNumber+Number.h"
#import "NSNumberFormatter+NumberFormat.h"


@implementation NSNumber (Number)

// returns an NSNumber representing a float, removes the comma thousands separators
+ (NSNumber *)numberFromFloatString:(NSString *)str {
    str = [str stringByReplacingOccurrencesOfString:@"," withString:@""];
    return [NSNumber numberWithFloat:[str floatValue]];
}

// returns a string representation of a number with the designated decimalPlaces
- (NSString *)numberWithPlaces:(NSUInteger)decimalPlaces {
    NSNumberFormatter *format = [NSNumberFormatter decimalNumberFormat:decimalPlaces];
    NSString *formattedNumber = [format stringFromNumber:self];
    return [formattedNumber stringByReplacingOccurrencesOfString:@"," withString:@""];
}

// returns the index of the matching array element or NSNotFound
- (NSInteger)containedWithinArray:(NSArray *)array {
    if (! array) return NSNotFound;
    NSInteger value = [self integerValue];
    
    for (NSInteger i = 0; i < array.count; i++) {
        if (value == [array[i] integerValue]) return i;
    }
    return NSNotFound;
}

@end
