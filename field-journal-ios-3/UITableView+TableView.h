//
//  UITableView+TableView.h
//  Field Journal
//
//  Created by Elia Freedman on 1/26/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (TableView)

- (void)deselectRowsAnimated:(BOOL)animated;         // clears all selected rows

@end
