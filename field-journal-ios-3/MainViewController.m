//
//  MainViewController.m
//  field-journal-ios-3
//
//  Created by Elia Freedman on 1/18/17.
//  Copyright © 2017 CHS, Inc. All rights reserved.
//

#import "MainViewController.h"
#import "LoginViewController.h"


@interface MainViewController () <LoginViewDelegate, SyncDatabaseDelegate>

@property (nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic) IBOutlet UIToolbar *toolbar;
@property (nonatomic) IBOutlet UITextView *logTextView;

@end


@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncInStage:) name:NOTIFICATION_SYNC_IN_PROGRESS object:nil];
    
    [[Sync shared] setSyncDatabaseDelegate:self];
    
    self.logTextView.text = @"";
    [self configureViewVisible:NO];
    [self.activityIndicatorView stopAnimating];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    Session *session = [Session shared];
    if (! [session hasAuthToken]) {
        [self login];
    } else {
        [self configureViewVisible:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark -- Configuration

- (void)configureViewVisible:(BOOL)visible {
    self.logTextView.hidden = ! visible;
    self.toolbar.hidden = ! visible;
}


#pragma mark -- Actions

- (void)login {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    LoginViewController *loginVC = (LoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
    loginVC.delegate = self;
    loginVC.modalPresentationStyle = UIModalPresentationFormSheet;
    loginVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:loginVC animated:YES completion:nil];
}

- (IBAction)sync:(id)sender {
    self.logTextView.text = @"";
    
    Sync *sync = [Sync shared];
    sync.syncDatabaseDelegate = self;
    [sync syncDatabase];
    
    [self.activityIndicatorView startAnimating];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    self.toolbar.hidden = YES;
}

- (IBAction)logout:(id)sender {
    [[Session shared] destroy];
    [self configureViewVisible:NO];
    [self login];
}

- (void)syncInStage:(NSNotification *)notification {
    NSString *message = notification.object;
    NSString *log = self.logTextView.text;
    
    if (log.length > 0) log = [NSString stringWithFormat:@"%@\n%@", log, message];
    else log = message;
    
    self.logTextView.text = log;
    if (self.logTextView.text.length > 0) {
        NSRange bottom = NSMakeRange(self.logTextView.text.length - 1, 1);
        [self.logTextView scrollRangeToVisible:bottom];
    }
}


#pragma mark -- Delegates

- (void)loginCredentialsAccepted:(LoginViewController *)loginViewController {
    [loginViewController dismissViewControllerAnimated:YES completion:^{
        self.toolbar.hidden = YES;
        self.logTextView.hidden = NO;
        [self sync:nil];
    }];
}

- (void)syncDatabaseCompleted:(NSString *)message {
    [self.activityIndicatorView stopAnimating];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self configureViewVisible:YES];
    
    if (message) {
        BOOL authTokenExpired = [message containsString:@"403"];
        NSString *title = Localize(@"error");
        if (authTokenExpired) {
            title = Localize(@"sync.login.buttonName");
            message = Localize(@"sync.failed.expiredAuthToken");
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:Localize(@"error") message:message preferredStyle:UIAlertControllerStyleAlert];
        alert.view.tintColor = [UIColor primaryColor];
        [alert addAction:[UIAlertAction actionWithTitle:Localize(@"okay") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (authTokenExpired) {
                [[Session shared] deleteAuthToken];
                [self configureViewVisible:NO];
                [self login];
            }
        }]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

@end
