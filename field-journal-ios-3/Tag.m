//
//  Tag.m
//  Field Journal
//
//  Created by Elia Freedman on 2/9/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Tag.h"
#import "Session.h"
#import "FMDB.h"


@implementation Tag

// store and update tags, returns array of error strings or empty array
+ (NSArray *)storeTags:(NSArray *)tags {
    Session *session = [Session shared];
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *tag in tags) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM tags WHERE server_id=?", tag[@"id"]];
            if ([results next]) {
                // update the existing record
                [db executeUpdate:@"UPDATE tags SET key=?,name=?,is_custom=? WHERE server_id=?", tag[@"key"], tag[@"name"], tag[@"isCustom"], results[@"server_id"]];
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO tags (server_id,key,name,is_custom) VALUES (?,?,?,?)", tag[@"id"], tag[@"key"], tag[@"name"], tag[@"isCustom"]];
            }
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeTags: %@", [db lastErrorMessage]]];
            [results close];
        }
    
    }];
    
    return errors;
}

@end
