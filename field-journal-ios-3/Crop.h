//
//  Crop.h
//  Field Journal
//
//  Created by Elia Freedman on 2/9/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Crop : NSObject

+ (NSArray <Crop *> *)allCrops;                                     // returns an array of all crop in name order
+ (Crop *)cropForId:(NSInteger)cropId;                              // returns a Crop object given a database id

+ (NSArray *)storeCrops:(NSArray *)crops;                           // store and update crops, returns array of error strings or empty array

@property (nonatomic) NSInteger dbId;                               // database id
@property (nonatomic) NSString *name;                               // crop name

@end
