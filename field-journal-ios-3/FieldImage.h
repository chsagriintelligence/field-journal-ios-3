//
//  FieldImage.h
//  Field Journal
//
//  Created by Rick Huebner on 10/10/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@class FMDatabase;


@interface FieldImage : NSObject

+ (BOOL)hasFieldImagesForFieldId:(NSInteger)fieldId;        // returns true if there are field images for a given fieldId

// returns the images for a given field id in any category in created_at order (newest first)
+ (NSArray <FieldImage *> *)fieldImagesForFieldId:(NSInteger)fieldId;
// returns an array of field images given a fieldId and categoryKey in created_at order (newest first)
+ (NSArray <FieldImage *> *)fieldImagesForFieldId:(NSInteger)fieldId categoryKey:(NSString *)categoryKey;

+ (NSArray *)setAvailability:(NSArray *)images;             // set record availability, returning an array of records needed
+ (NSArray *)storeFieldImages:(NSArray *)images;            // store and update images, returns array of error strings or empty array
+ (void)deleteFieldImages:(NSArray <NSNumber *> *)images;

+ (NSMutableArray *)fieldImagesToDownload;                  // returns an array of dictionaries comprising server_id and filename keys of images that need to be downloaded

@property (nonatomic) NSInteger serverId;
@property (nonatomic) NSString *createdAt;                          // date stamp in yyyy-mm-dd format
@property (nonatomic) NSString *filename;                           // image's local filename
@property (nonatomic) NSString *categoryKey;                        // key for field image category table
@property (nonatomic) NSString *resolution;                         // image resolution code
@property (nonatomic) NSString *permissions;
@property (nonatomic) CLLocationCoordinate2D ulCoords;              // lat/lon of upper left corner
@property (nonatomic) CLLocationCoordinate2D lrCoords;              // lat/lon of lower right corner

- (BOOL)hasPermissionForType:(NSString *)permissionType;            // returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE

@end
