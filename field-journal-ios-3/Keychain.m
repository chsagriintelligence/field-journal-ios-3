//
//  Keychain.m
//  powerone-ios
//
//  Created by Elia Freedman on 1/7/15.
//  Copyright (c) 2015 Infinity Softworks, Inc. All rights reserved.
//  http://stackoverflow.com/questions/5247912/saving-email-password-to-keychain-in-ios
//

#import "Keychain.h"

@implementation Keychain

+ (NSMutableDictionary *)getKeychainQuery:(NSString *)service {
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:
            (__bridge id)kSecClassGenericPassword, (__bridge id)kSecClass,
            service, (__bridge id)kSecAttrService,
            service, (__bridge id)kSecAttrAccount,
            (__bridge id)kSecAttrAccessibleAfterFirstUnlock, (__bridge id)kSecAttrAccessible,
            nil];
}

+ (void)save:(NSString *)service data:(id)data group:(NSString *)group {
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
    if (group)
        [keychainQuery setObject:group forKey:(__bridge id)kSecAttrAccessGroup];
    SecItemDelete((__bridge CFDictionaryRef)keychainQuery);
    [keychainQuery setObject:[NSKeyedArchiver archivedDataWithRootObject:data] forKey:(__bridge id)kSecValueData];
    OSStatus status = SecItemAdd((__bridge CFDictionaryRef)keychainQuery, NULL);
    if (status != errSecSuccess)
        NSLog(@"Error %d saving keychain value %@", (int)status, service);
}

+ (id)load:(NSString *)service group:(NSString *)group {
    id ret = nil;
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
    if (group)
        [keychainQuery setObject:group forKey:(__bridge id)kSecAttrAccessGroup];
    [keychainQuery setObject:(id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    [keychainQuery setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    CFDataRef keyData = NULL;
    if (SecItemCopyMatching((__bridge CFDictionaryRef)keychainQuery, (CFTypeRef *)&keyData) == noErr) {
        @try {
            ret = [NSKeyedUnarchiver unarchiveObjectWithData:(__bridge NSData *)keyData];
        }
        @catch (NSException *e) {
            NSLog(@"Unarchive of %@ failed: %@", service, e);
        }
        @finally {}
    }
    if (keyData) CFRelease(keyData);
    return ret;
}

+ (void)destroy:(NSString *)service {
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
    SecItemDelete((__bridge CFDictionaryRef)keychainQuery);
}

@end
