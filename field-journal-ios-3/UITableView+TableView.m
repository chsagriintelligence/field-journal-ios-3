//
//  UITableView+TableView.m
//  Field Journal
//
//  Created by Elia Freedman on 1/26/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "UITableView+TableView.h"

@implementation UITableView (TableView)

// clears all selected rows
- (void)deselectRowsAnimated:(BOOL)animated {
    NSArray *indexPaths = [self indexPathsForSelectedRows];
    for (NSIndexPath *indexPath in indexPaths) {
        [self deselectRowAtIndexPath:indexPath animated:animated];
    }
}

@end
