//
//  NSArray+Array.h
//  Field Journal
//
//  Created by Elia Freedman on 1/20/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Array)

+ (NSArray *)arrayFromJSONString:(NSString *)jsonString;                        // converts a string containing JSON to an array

- (NSString *)convertJSONArrayToString;                                         // converts an array containing JSON to a string

@end
