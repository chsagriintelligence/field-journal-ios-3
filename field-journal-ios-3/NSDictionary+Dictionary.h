//
//  NSDictionary+Dictionary.h
//  Field Journal
//
//  Created by Elia Freedman on 1/20/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Dictionary)

+ (NSDictionary *)dictionaryFromJSONString:(NSString *)jsonString;              // converts a string containing JSON to a dictionary
- (NSString *)convertJSONDictionaryToString;                                    // converts a dictionary containing JSON to a string

@end
