//
//  Image.h
//  Field Journal
//
//  Created by Elia Freedman on 1/21/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@class FMDatabase;


@interface Image : NSObject

// returns the images for a given observation id
+ (NSArray <Image *> *)imagesForObservationId:(NSInteger)observationId;

// create and return a new image given an observation database id and filename
+ (Image *)newImageForObservationId:(NSInteger)observationId filename:(NSString *)filename;

+ (NSArray *)setAvailability:(NSArray *)images;                     // set record availability, returning an array of records needed
// store and update images, returns array of error strings or empty array
+ (NSArray *)storeImages:(NSArray *)images;

+ (void)deleteImages:(NSArray <NSNumber *> *)images;
+ (void)deleteImagesForObservation:(NSNumber *)observationId;       // deletes all images with a given observation id

+ (NSMutableArray *)imagesToUpload;                                 // returns an array of dictionaries comprising server_id and filename keys of images that need to be uploaded
+ (void)imageUploadSucceeded:(NSInteger)serverId;                   // handles upload success for an image with serverId
+ (NSMutableArray *)imagesToDownload;                               // returns an array of dictionaries comprising server_id and filename keys of images that need to be downloaded

+ (NSArray *)modifiedImages;                                        // returns an array of modified images or nil if there are none
+ (NSArray *)deletedImages;                                         // returns an array of deleted image server ids or nil if there are none
+ (void)clearModifiedDuringSync;


@property (nonatomic) NSInteger dbId;                               // database id
@property (nonatomic) NSString *filename;                           // image's filename
@property (nonatomic) NSString *permissions;                        // permissions

- (void)destroy;                                                    // delete image
- (BOOL)hasPermissionForType:(NSString *)permissionType;            // returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE

@end
