//
//  User.h
//  Field Journal
//
//  Created by Elia Freedman on 7/13/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface User : NSObject

+ (NSString *)userForId:(NSInteger)userServerId;                    // returns a user's name given a user server id

+ (NSArray *)setAvailability:(NSArray *)users;                      // set record availability, returning an array of records needed
+ (NSArray *)storeUsers:(NSArray *)users;                           // store and update users, returns array of error strings or empty array

+ (void)deleteUsers:(NSArray *)users;

@end
