//
//  Property.m
//  Field Journal
//
//  Created by Elia Freedman on 2/9/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Property.h"
#import "Session.h"
#import "FMDB.h"


@implementation Property

- (id)initWithId:(NSInteger)dbId name:(NSString *)name key:(NSString *)key {
    self = [super init];
    if (! self) return nil;
    
    _dbId = dbId;
    _name = ((NSNull *)name == [NSNull null] ? nil : name);
    _key = key;
    
    return self;
}

// store and update properties, returns array of error strings or empty array
+ (NSArray *)storeProperties:(NSArray *)properties {
    Session *session = [Session shared];
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *property in properties) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM properties WHERE server_id=?", property[@"id"]];
            if ([results next]) {
                // update the existing record
                [db executeUpdate:@"UPDATE properties SET key=?,name=?,type=? WHERE server_id=?", property[@"key"], property[@"name"], property[@"type"], results[@"server_id"]];
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO properties (server_id,key,name,type) VALUES (?,?,?,?)", property[@"id"], property[@"key"], property[@"name"], property[@"type"]];
            }
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeProperties: %@", [db lastErrorMessage]]];
            [results close];
        }
    
    }];
    
    return errors;
}

// returns a Property object given a database query
+ (Property *)propertyWithResultSet:(FMResultSet *)results {
    return [[Property alloc] initWithId:[results[@"server_id"] integerValue] name:results[@"name"] key:results[@"key"]];
}

// returns a Property object given a database id
+ (Property *)propertyForId:(NSInteger)propertyId {
    Session *session = [Session shared];
    __block Property *property;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM properties WHERE server_id=?", @(propertyId)];
        if ([results next]) {
            property = [self propertyWithResultSet:results];
        }
        [results close];
    
    }];
    
    return property;
}

// returns a property id for a given key
+ (Property *)propertyForKey:(NSString *)key {
    Session *session = [Session shared];
    __block Property *property;
    
    if (! key) return property;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM properties WHERE key=?", key];
        if ([results next]) {
            property = [self propertyWithResultSet:results];
        }
        [results close];
    
    }];
    
    return property;
}

@end
