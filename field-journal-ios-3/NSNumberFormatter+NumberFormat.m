//
//  NSNumberFormatter+NumberFormat.m
//  Field Journal
//
//  Created by Elia Freedman on 1/26/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "NSNumberFormatter+NumberFormat.h"

@implementation NSNumberFormatter (NumberFormat)

+ (NSNumberFormatter *)decimalNumberFormat:(NSUInteger)decimalPlaces {
    NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
    [numberFormat setFormatterBehavior: NSNumberFormatterBehavior10_4];
    [numberFormat setNumberStyle: NSNumberFormatterDecimalStyle];
    [numberFormat setUsesGroupingSeparator:YES];
    [numberFormat setMaximumFractionDigits:decimalPlaces];
    [numberFormat setMinimumFractionDigits:decimalPlaces];
    
    return numberFormat;
}

@end
