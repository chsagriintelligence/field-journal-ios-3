//
//  FieldImageCategory.h
//  Field Journal
//
//  Created by Rick Huebner on 10/20/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "FieldImageCategory.h"
#import "Crop.h"
#import "Session.h"
#import "FMDB.h"
#import "UIImage+Image.h"

@interface FieldImageCategory ()

@property (nonatomic) NSInteger serverId;
@property (nonatomic) NSString *legendFile;             // legend image's local filename

@end


@implementation FieldImageCategory

- (id)initWithServerId:(NSInteger)serverId name:(NSString *)name key:(NSString *)key legendFile:(NSString *)legendFile legendDesc:(NSString *)legendDesc {
    self = [super init];
    if (! self) return nil;
    
    _serverId = serverId;
    _name = ((NSNull *)name == [NSNull null] ? @"" : name);
    _key = ((NSNull *)key == [NSNull null] ? @"" : key);
    _legendFile = legendFile;
    _legendDescription = legendDesc;

    return self;
}

- (UIImage *)fieldImageLegend {
    return self.legendFile ? [UIImage retrieveImage:self.legendFile] : nil;
}

// store and update field image categories, returns array of error strings or empty array
+ (NSArray *)storeCategories:(NSArray *)categories {
    Session *session = [Session shared];
    NSMutableArray *errors = [NSMutableArray array];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *cat in categories) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldimagecategories WHERE server_id=?", cat[@"id"]];
            if ([results next]) {
                // update the existing record
                [db executeUpdate:@"UPDATE fieldimagecategories SET server_id=?, ordinal=?, name=?, key=?, url=?, description=? WHERE server_id=?", cat[@"id"], cat[@"displayOrder"], cat[@"name"], cat[@"key"], cat[@"legendUrl"], cat[@"legendDescription"], results[@"server_id"]];
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO fieldimagecategories (server_id, ordinal, name, key, url, description) VALUES (?,?,?,?,?,?)", cat[@"id"], cat[@"displayOrder"], cat[@"name"], cat[@"key"], cat[@"legendUrl"], cat[@"legendDescription"]];
            }
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeCategories: %@", [db lastErrorMessage]]];
            [results close];
        }
    
    }];
    
    return errors;
}

// returns a Field image category object given a database query
+ (FieldImageCategory *)categoryWithResultSet:(FMResultSet *)results {
    NSString *legendFile = (results[@"url"] == [NSNull null]) ? nil : [FieldImageCategory filenameFromURL:results[@"url"]];
    NSString *legendDesc = (results[@"description"] == [NSNull null]) ? nil : results[@"description"];
    return [[FieldImageCategory alloc] initWithServerId:[results[@"server_id"] integerValue] name:results[@"name"] key:results[@"key"] legendFile:legendFile legendDesc:legendDesc];
}

// returns a category object given a field image category key
+ (FieldImageCategory *)categoryForKey:(NSString *)key {
    Session *session = [Session shared];
    __block FieldImageCategory *cat = nil;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldimagecategories WHERE key=? LIMIT 1", key];
        if ([results next]) {
            cat = [FieldImageCategory categoryWithResultSet:results];
        }
        [results close];
    
    }];
    
    return cat;
}

// returns an array of all categories in ordinal order
+ (NSArray <FieldImageCategory *> *)allCategories {
    Session *session = [Session shared];
    NSMutableArray *cats = [NSMutableArray array];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldimagecategories ORDER BY ordinal ASC"];
        while ([results next]) {
            FieldImageCategory *cat = [FieldImageCategory categoryWithResultSet:results];
            [cats addObject:cat];
        }
        [results close];
    
    }];
    
    return cats;
}

// returns an array of category objects for field images with a given fieldId in ordinal order
+ (NSArray <FieldImageCategory *> *)categoriesForFieldImagesWithFieldId:(NSInteger)fieldId {
    Session *session = [Session shared];
    // get list of applicable field images
    NSArray *images = [FieldImage fieldImagesForFieldId:fieldId];
    
    // extract list of category keys from selected images
    NSMutableArray *keys = [NSMutableArray array];
    for (FieldImage *image in images) {
        if (! [keys containsObject:image.categoryKey]) [keys addObject:image.categoryKey];
    }
    
    // get list of categories using specified keys, in ordinal order
    NSMutableArray *cats = [NSMutableArray array];
    if (images.count > 0) {
        FMDatabaseQueue *queue = [session databaseQueue];
        [queue inDatabase:^(FMDatabase *db) {
        
            NSString *list = [keys componentsJoinedByString:@"', '"];   // inner quotes, next line adds outer
            NSString *query = [NSString stringWithFormat:@"SELECT * FROM fieldimagecategories WHERE key IN ('%@') ORDER BY ordinal ASC", list];
            FMResultSet *results = [db executeQuery:query];
            while ([results next]) {
                FieldImageCategory *cat = [FieldImageCategory categoryWithResultSet:results];
                [cats addObject:cat];
            }
            [results close];
        
        }];
    }
    
    return cats;
}

// converts the server-side category legend url into a local filename
+ (NSString *)filenameFromURL:(NSString *)url {
    NSRange range = [url rangeOfString:@"/" options:NSBackwardsSearch];
    NSUInteger pos = (range.location == NSNotFound) ? 0 : range.location + 1;
    return [NSString stringWithFormat:@"legend_%@", [url substringFromIndex:pos]];
}

// returns an array of dictionaries comprising urls and filenames of category legends that need to be downloaded
// also removes old legend files which are no longer needed
+ (NSMutableArray *)legendsToDownload {
    Session *session = [Session shared];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *imagesInFileManager = [fileManager contentsOfDirectoryAtPath:[session imagesPath] error:nil];
    NSMutableArray *legendsToDownload = [NSMutableArray array];
    NSMutableArray *legendsUsed = [NSMutableArray array];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        // check each legend url used in db and see if the corresponding file exists
        FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldimagecategories"];
        while ([results next]) {
            if (results[@"url"] != [NSNull null]) {    // some categories have no legend
                NSString *legendInDB = [FieldImageCategory filenameFromURL:results[@"url"]];
                [legendsUsed addObject:legendInDB]; // accumulate list of legend images referenced in db
                
                BOOL found = NO;
                for (NSString *imageInFM in imagesInFileManager) {
                    if ([legendInDB isEqualToString:imageInFM]) {
                        found = YES;
                        break;
                    }
                }
                if (! found) [legendsToDownload addObject:@{@"server_id" : results[@"url"], @"filename" : legendInDB, @"imageType" : @"legend"}]; // no such file, download it
            }
        }
        [results close];
    
    }];
    
    // clean up any old legend image files which are no longer used in the db table
    for (NSString *imageInFM in imagesInFileManager) {
        if ([imageInFM rangeOfString:@"legend_"].location == 0) {   // only check legend files
            BOOL found = NO;
            for (NSString *legendInDB in legendsUsed) {
                if ([legendInDB isEqualToString:imageInFM]) {
                    found = YES;
                    break;
                }
            }
            if (! found) [UIImage deleteImage:imageInFM];    // no longer in table, delete it
        }
    }
    
    return legendsToDownload;
}

@end
