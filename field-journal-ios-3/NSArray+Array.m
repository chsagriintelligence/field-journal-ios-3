//
//  NSArray+Array.m
//  Field Journal
//
//  Created by Elia Freedman on 1/20/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "NSArray+Array.h"

@implementation NSArray (Array)

// converts an array containing JSON to a string
- (NSString *)convertJSONArrayToString {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
    
    if (! jsonData) {
        NSLog(@"json conversion error: %@", error);
        return nil;
    }
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

// converts a string containing JSON to a dictionary
+ (NSArray *)arrayFromJSONString:(NSString *)jsonString {
    if (! jsonString) return nil;
    
    NSError *error;
    NSData *dataObj = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *data = [NSJSONSerialization JSONObjectWithData:dataObj options:0 error:&error];
    
    if (! data) {
        NSLog(@"json conversion error: %@", error);
        return nil;
    }
    
    return data;
}

@end
