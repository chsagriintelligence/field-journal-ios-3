//
//  Coop.h
//  Field Journal
//
//  Created by Rick Huebner on 11/11/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Coop : NSObject

+ (NSArray <Coop *> *)coops;                                // returns all coops in alphabetical order by name
+ (NSArray <Coop *> *)coopsForGrowerId:(NSInteger)growerId; // returns Coop objects given a grower's id sorted by name
+ (Coop *)coopForId:(NSInteger)coopId;                      // returns a Coop object given a coop id

+ (NSInteger)coopIdForFieldId:(NSInteger)fieldId;           // returns the coop id for the farm which contains the field, or NSNotFound if error

+ (NSArray *)setAvailability:(NSArray *)coops;              // set record availability, returning an array of records needed
+ (NSArray *)storeCoops:(NSArray *)coops;                   // store and update coops, returns array of error strings or empty array
+ (void)deleteCoops:(NSArray <NSNumber *> *)coops;


@property (nonatomic) NSInteger dbId;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *permissions;

- (BOOL)hasPermissionForType:(NSString *)permissionType;    // returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE

@end
