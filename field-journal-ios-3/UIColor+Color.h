//
//  UIColor+Color.h
//  Field Journal
//
//  Created by Elia Freedman on 2/16/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


// Converts hex colors to UIColor.
// i.e., UIColor *color = UIColorFromHex(0xEEEFEA);
#define UIColorFromHex(hexValue) [UIColor colorWithRed:((float)(((hexValue) & 0xFF0000) >> 16))/255.0 green:((float)(((hexValue) & 0xFF00) >> 8))/255.0 blue:((float)((hexValue) & 0xFF))/255.0 alpha:1.0]


@interface UIColor (Color)

+ (UIColor *)primaryColor;
+ (UIColor *)secondaryColor;
+ (UIColor *)selectedGrey;
+ (UIColor *)backgroundColor;
+ (UIColor *)warningColor;
+ (UIColor *)alertColor;

// returns a UIColor in exchange for a hex string. The hex string can have a leading 0x, # or include no leading character.
+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
