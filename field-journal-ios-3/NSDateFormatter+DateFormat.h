//
//  NSDateFormatter+DateFormat.h
//  Field Journal
//
//  Created by Elia Freedman on 1/16/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSDateFormatter (DateFormat)

+ (NSString *)utcDatetimeToLocalDatetime:(NSString *)utcString;
+ (BOOL)dateIsToday:(NSString *)utcString;
+ (NSString *)timestampInUTCfor:(NSDate *)date;
+ (NSString *)nowInUTC;

@end
