//
//  LoginViewController.m
//  field-journal-ios-3
//
//  Created by Elia Freedman on 1/20/17.
//  Copyright © 2017 CHS, Inc. All rights reserved.
//

#import "LoginViewController.h"


@interface LoginViewController () <SyncAuthenticateDelegate>

@property (nonatomic) IBOutlet UIView *loginView;
@property (nonatomic) IBOutlet NSLayoutConstraint *loginViewAlignCenterXConstraint;
@property (nonatomic) IBOutlet UITextField *emailField;
@property (nonatomic) IBOutlet UITextField *passwordField;
@property (nonatomic) IBOutlet UIButton *loginButton;
@property (nonatomic) IBOutlet UISegmentedControl *serverSegmentedControl;

@property (nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end


@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    Session *session = [Session shared];
    
    [self.loginButton setTitle:Localize(@"sync.login.buttonName") forState:UIControlStateNormal];
    [self.loginButton setTitleColor:[UIColor secondaryColor] forState:UIControlStateNormal];
    self.loginButton.backgroundColor = [UIColor primaryColor];
    
    NSString *email = [session emailAddress];
    if (email) self.emailField.text = email;
    
    NSString *server = [session domain];
    if ([server isEqualToString:SESSION_DOMAIN_STAGING]) self.serverSegmentedControl.selectedSegmentIndex = 1;
    else if ([server isEqualToString:SESSION_DOMAIN_TESTING]) self.serverSegmentedControl.selectedSegmentIndex = 2;
    else self.serverSegmentedControl.selectedSegmentIndex = 0;
    
    self.activityIndicatorView.color = [UIColor primaryColor];
    [self.activityIndicatorView stopAnimating];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.emailField.text.length == 0) [self.emailField becomeFirstResponder];
    else if (self.passwordField.text.length == 0) [self.passwordField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark -- Actions

- (IBAction)serverSelected:(UISegmentedControl *)segmentedControl {
    Session *session = [Session shared];
    if (segmentedControl.selectedSegmentIndex == 1) [session setDomain:SESSION_DOMAIN_STAGING];
    else if (segmentedControl.selectedSegmentIndex == 2) [session setDomain:SESSION_DOMAIN_TESTING];
    else [session setDomain:SESSION_DOMAIN_PRODUCTION];
}

- (IBAction)login:(id)sender {
    if ([self.emailField isFirstResponder]) [self.emailField resignFirstResponder];
    else if ([self.passwordField isFirstResponder]) [self.passwordField resignFirstResponder];

    [UIView animateWithDuration:ANIMATION_LENGTH animations:^{
        self.loginView.alpha = 0.0;
    } completion:^(BOOL finished) {
        self.loginView.hidden = YES;
        self.loginView.alpha = 1.0;
        [self.activityIndicatorView startAnimating];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

        Sync *sync = [Sync shared];
        sync.authenticateDelegate = self;
        [sync login:self.emailField.text withPassword:self.passwordField.text];
    }];
}


#pragma mark -- Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == self.emailField) {
        [self.passwordField becomeFirstResponder];
    } else {
        [self login:nil];
    }
    
    return NO;
}

// delegate call to handle authenticate failed with an error message
- (void)authenticateFailed:(NSString *)message {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.activityIndicatorView stopAnimating];
    
    if (message) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:Localize(@"error") message:message preferredStyle:UIAlertControllerStyleAlert];
        alert.view.tintColor = [UIColor primaryColor];
        
        [alert addAction:[UIAlertAction actionWithTitle:Localize(@"okay") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            self.loginView.alpha = 0.0;
            self.loginView.hidden = NO;
            [UIView animateWithDuration:ANIMATION_LENGTH animations:^{
                self.loginView.alpha = 1.0;
            }];
        }]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

// delegate call to handle authenticate succeeded with an authorization token returned
- (void)authenticateCompletedWithToken:(NSString *)authToken {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.activityIndicatorView stopAnimating];
    if (authToken) {
        Session *session = [Session shared];
        [session setAuthToken:authToken];
        [session setEmailAddress:self.emailField.text];
        [self.delegate loginCredentialsAccepted:self];
    } else {
        [self authenticateFailed:Localize(@"sync.failed.missingAuthToken")];
    }
}


- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect viewFrame = [self.view convertRect:self.view.frame toView:self.parentViewController.view];
    
    CGFloat offset = -(0.25 * (- viewFrame.origin.y + keyboardFrame.size.height));
    if (offset > 0) offset = 0.0;
    
    self.loginViewAlignCenterXConstraint.constant = offset;
    [UIView animateWithDuration:ANIMATION_LENGTH animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    self.loginViewAlignCenterXConstraint.constant = 0.0;
    [UIView animateWithDuration:ANIMATION_LENGTH animations:^{
        [self.view layoutIfNeeded];
    }];
}

@end
