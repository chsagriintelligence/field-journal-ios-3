//
//  Session.m
//  field-journal-ios-3
//
//  Created by Elia Freedman on 1/18/17.
//  Copyright © 2017 CHS, Inc. All rights reserved.
//

#import "Session.h"
#import "Migration.h"
#import "Keychain.h"


#define DATABASE_NAME                       @"database.sqlite"
#define DATA_FOLDER_NAME                    @"field journal"
#define LOCATION_UPDATE_METERS              1.0                             // meters of movement required to trigger GPS location update callback

#define KEYCHAIN_EMAIL_ADDRESS              @"fieldjournalEmailAddress"     // set to user's email address
#define KEYCHAIN_AUTH_TOKEN                 @"fieldjournalAuthToken"        // set to auth token string
#define KEYCHAIN_DOMAIN                     @"fieldjournalDomain"           // URL domain for the endpoint API or destroyed if the default DOMAINS_DEFAULT_INDEX
#define KEYCHAIN_PIN_CODE                   @"pin_code"                     // login 4-digit PIN code
#define KEYCHAIN_PIN_CODE_ACTIVATED_AT      @"pin_code_activated_at"        // last datetime the user logged in

#define SETTING_BUILD_NUMBER                @"build_number"                 // application's build number
#define SETTING_MIGRATION_VERSION           @"migration_version"            // current database migration version
#define SETTING_SERVER_MIGRATION_VERSION    @"server_migration_version"
#define SETTING_SYNCED_AT                   @"synced_at"
#define SETTING_SYNC_ERRORS                 @"sync_errors"
#define SETTING_USER_PERMISSIONS            @"user_permissions"
#define SETTING_USER_ID                     @"user_id"
#define SETTING_MIGRATION_VERSION           @"migration_version"


@implementation Session

- (id)init {
    self = [super init];
    if (! self) return nil;
    
    [self setPaths];
    
    _lastLocation = [[CLLocation alloc] initWithLatitude:0 longitude:0];
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = LOCATION_UPDATE_METERS;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if (CLLocationManager.authorizationStatus == kCLAuthorizationStatusNotDetermined) {
        // On iOS 8+ must ask permission or location services won't start
        // Must also define NSLocationWhenInUseUsageDescription string in info.plist to provide dialog text
        if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) // only in iOS 8+
            [_locationManager requestWhenInUseAuthorization];
    } else if (CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) [_locationManager startUpdatingLocation];
    else [_locationManager stopUpdatingLocation];

    return self;
}

+ (Session *)shared {
    static Session *shared = nil;
    if (! shared) shared = [[super allocWithZone:nil] init];
    return shared;
}

// returns a UUID
+ (NSString *)UUID {
    CFUUIDRef uniqueId = CFUUIDCreate(kCFAllocatorDefault);
    CFStringRef uidStr = CFUUIDCreateString(kCFAllocatorDefault, uniqueId);
    NSString *key = (__bridge NSString *)uidStr;
    CFRelease(uniqueId);
    CFRelease(uidStr);
    return key;
}

// returns a string representing the app version and build
+ (NSString *)appVersionAndBuild {
    NSDictionary *infoPListDict = [NSDictionary dictionaryWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"Info.plist"]];
    NSString *versionNumber = [infoPListDict objectForKey:@"CFBundleShortVersionString"];
    if (! versionNumber) versionNumber = @"";
    NSString *buildNumber = [infoPListDict objectForKey:@"CFBundleVersion"];
    if (! buildNumber) buildNumber = @"";
    NSString *label = [NSString stringWithFormat:@"%@ (%@)", versionNumber, buildNumber];
    
    return label;
}

// destroy session and reinitialize
- (void)destroy {
    [self deleteEmailAddress];
    [self deletePinCode];
    [self deletePinCodeActivatedAt];
    [self deleteAuthToken];
    [self deleteDatabase];
    
    // reset stored and temporary data
    [self updateFiles];
    [self createSettings];
    [Migration migrate];
    
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalNever];    // halt background fetch while logged out
}


#pragma mark - Authorization Token

// set the authorization token to the keychain
- (void)setAuthToken:(NSString *)authToken {
    [Keychain save:KEYCHAIN_AUTH_TOKEN data:authToken group:nil];
}

// recover the authorization token from the keychain; return token or nil if no token available
- (NSString *)authToken {
    NSString *authToken = [Keychain load:KEYCHAIN_AUTH_TOKEN group:nil];
    if (! authToken || authToken.length == 0) return nil;
    return authToken;
}

// delete the authorization token from the keychain
- (void)deleteAuthToken {
    [Keychain destroy:KEYCHAIN_AUTH_TOKEN];
}

// return YES if there is a token, false otherwise
- (BOOL)hasAuthToken {
    if (self.authToken && self.authToken.length > 0) return true;
    return false;
}


#pragma mark - Endpoint API

// set the API endpoint domain
- (void)setDomain:(NSString *)domain {
    [Keychain save:KEYCHAIN_DOMAIN data:domain group:nil];
}

// recover the API endpoint domain
- (NSString *)domain {
    NSString *domain = [Keychain load:KEYCHAIN_DOMAIN group:nil];
    if (! domain || domain.length == 0) return SESSION_DOMAIN_PRODUCTION;
    return domain;
}

// delete the domain from the keychain
- (void)deleteDomain {
    [Keychain destroy:KEYCHAIN_DOMAIN];
}


#pragma mark - Email Address

// set the email address
- (void)setEmailAddress:(NSString *)emailAddress {
    [Keychain save:KEYCHAIN_EMAIL_ADDRESS data:emailAddress group:nil];
}

// recover the email address
- (NSString *)emailAddress {
    NSString *emailAddress = [Keychain load:KEYCHAIN_EMAIL_ADDRESS group:nil];
    if (! emailAddress || emailAddress.length == 0) return nil;
    return emailAddress;
}

// delete the email address from the keychain
- (void)deleteEmailAddress {
    [Keychain destroy:KEYCHAIN_EMAIL_ADDRESS];
}


#pragma mark - PIN Code

// set the pin code to the keychain
- (void)setPinCode:(NSString *)pinCode {
    [Keychain save:KEYCHAIN_PIN_CODE data:pinCode group:nil];
}

// recover the pin code from the keychain; return pin code or nil if no token available
- (NSString *)pinCode {
    NSString *pinCode = [Keychain load:KEYCHAIN_PIN_CODE group:nil];
    if (! pinCode || pinCode.length == 0) return nil;
    return pinCode;
}

// delete the pin code from the keychain
- (void)deletePinCode {
    [Keychain destroy:KEYCHAIN_PIN_CODE];
}


#pragma mark - PIN Code Last Activated Datetime

// set the pin code last activated datetime to the keychain
- (void)setPinCodeActivatedAt:(NSString *)pinCodeActivatedAt {
    [Keychain save:KEYCHAIN_PIN_CODE_ACTIVATED_AT data:pinCodeActivatedAt group:nil];
}

// recover the pin code last activated datetime from the keychain; return string or nil if no token available
- (NSString *)pinCodeActivatedAt {
    NSString *pinCodeActivatedAt = [Keychain load:KEYCHAIN_PIN_CODE_ACTIVATED_AT group:nil];
    if (! pinCodeActivatedAt || pinCodeActivatedAt.length == 0) return nil;
    return pinCodeActivatedAt;
}

// delete the pin code last activated datetime from the keychain
- (void)deletePinCodeActivatedAt {
    [Keychain destroy:KEYCHAIN_PIN_CODE_ACTIVATED_AT];
}


#pragma mark - Database and Files

// create and/or return singleton database queue object for multithreading-safe db access
- (FMDatabaseQueue *)databaseQueue {
    if (! _databaseQueue) _databaseQueue = [FMDatabaseQueue databaseQueueWithPath:self.databasePath];
    return _databaseQueue;
}

// set the default settings, database must exist first
// initialize the path locations
- (void)setPaths {
    NSArray *appSupportPaths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *appSupportDir = [appSupportPaths objectAtIndex:0];
    
    self.storagePath = [appSupportDir stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/", DATA_FOLDER_NAME]];
    self.databasePath = [self.storagePath stringByAppendingPathComponent:DATABASE_NAME];
    
    NSLog(@"%@", self.databasePath);
}

// check on database, update settings if build changed, handle migrations
- (void)updateFiles {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager fileExistsAtPath:self.databasePath];
    if (success) return;
    
    BOOL isDirectory = NO;
    BOOL exists = [fileManager fileExistsAtPath:self.storagePath isDirectory:&isDirectory];
    if (! exists) {
        NSError *error;
        BOOL success = [[NSFileManager defaultManager] createDirectoryAtPath:self.storagePath withIntermediateDirectories:YES attributes:nil error:&error];
        if (! success) NSLog(@"could not create directory at path %@", self.storagePath);
        [[NSURL fileURLWithPath:self.storagePath] setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
        if (error) NSLog(@"could not set excluded from backup on the storage path correctly");
    }
    
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DATABASE_NAME];
    [fileManager copyItemAtPath:databasePathFromApp toPath:self.databasePath error:nil];
}

// remove the database
- (void)deleteDatabase {
    if (self.databaseQueue) {
        [self.databaseQueue close]; // close the db and free the queue memory
        self.databaseQueue = nil;   // force db to be reopened fresh on next access
    }
    [[NSFileManager defaultManager] removeItemAtPath:self.storagePath error:nil];
}


#pragma mark - Settings Storage and Recall

// update a setting's value if it is changed, also update the updated_at datetime stamp for the setting, value == nil saves @"" instead
- (void)setString:(NSString *)value forKey:(NSString *)key {
    if (! value) value = @"";
    
    FMDatabaseQueue *queue = [[Session shared] databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *results = [db executeQuery:@"SELECT * FROM settings WHERE key=?", key];
        [results next];
        NSString *dbValue = results[@"value"];
        if (! [dbValue isEqualToString:value]) {
            [db executeUpdate:@"UPDATE settings SET value=? WHERE key=?", value, key];
        }
        [results close];
        
    }];
}

- (void)setDictionary:(NSDictionary *)value forKey:(NSString *)key {
    NSString *valueAsString = [NSString stringWithDictionary:value];
    [self setString:valueAsString forKey:key];
}

- (void)setInteger:(NSInteger)value forKey:(NSString *)key {
    NSString *valueAsString = [NSString stringWithFormat:@"%ld", (long)value];
    [self setString:valueAsString forKey:key];
}

- (void)setBoolean:(BOOL)value forKey:(NSString *)key {
    [self setInteger:(NSInteger)value forKey:key];
}

// returns a string for a value or nil if not found
- (NSString *)stringForKey:(NSString *)key {
    __block NSString *value = nil;
    
    FMDatabaseQueue *queue = [[Session shared] databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *results = [db executeQuery:@"SELECT * FROM settings WHERE key=?", key];
        if ([results next]) value = results[@"value"];
        [results close];
        
    }];
    
    return value;
}

- (NSDictionary *)dictionaryForKey:(NSString *)key {
    NSString *valueAsString = [self stringForKey:key];
    return [valueAsString dictionaryValue];
}

- (NSInteger)integerForKey:(NSString *)key {
    NSString *value = [self stringForKey:key];
    return [value integerValue];
}

- (BOOL)boolForKey:(NSString *)key {
    NSString *value = [self stringForKey:key];
    return [value boolValue];
}


#pragma mark - Settings

// Returns default values in a dictionary of key:value pairs. If keys is not nil then returned dictionary is limited to those keys.
- (NSDictionary *)defaultSettings:(NSArray *)keys {
    NSDictionary *defaults;
    defaults = @{
                 SETTING_MIGRATION_VERSION: @0,
                 SETTING_SERVER_MIGRATION_VERSION: @0,
                 SETTING_SYNCED_AT : @"",
                 SETTING_SYNC_ERRORS : @"",
                 SETTING_USER_PERMISSIONS : @"",
                 SETTING_USER_ID : @"0",
                };
    
    // if keys is nil then return all defaults
    if (! keys) return defaults;
    
    // otherwise return a limited dictionary with only those keys requested
    NSMutableDictionary *mutableDefaults = [[NSMutableDictionary alloc] init];
    for (NSInteger i = 0; i < keys.count; i++) {
        [mutableDefaults setObject:[defaults objectForKey:keys[i]] forKey:keys[i]];
    }
    return mutableDefaults;
}

// set the default settings, database must exist first
- (void)createSettings {
    NSDictionary *defaults = [self defaultSettings:nil];
    
    FMDatabaseQueue *queue = [[Session shared] databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
        
        
        for (NSString *key in defaults) {
            NSString *value = [defaults objectForKey:key];
            FMResultSet *results = [db executeQuery:@"SELECT * FROM settings WHERE key=?", key];
            if (! [results next]) {
                [db executeUpdate:@"INSERT INTO settings (key,value) VALUES (?,?)", key, value];
            }
            [results close];
        }
        
    }];
}


#pragma mark - Users and Permissions

// current user's server_id
- (NSInteger)currentUser { return [self integerForKey:SETTING_USER_ID]; }
- (void)setCurrentUser:(NSInteger)currentUser { [self setInteger:currentUser forKey:SETTING_USER_ID]; }

// permissions for each record type, combination of [c]reate, [u]pdate, [d]elete or empty string for read-only; these are used if not defined for the record itself
- (NSDictionary *)userPermissions {
    NSString *permissions = [self stringForKey:SETTING_USER_PERMISSIONS];
    if (permissions.length == 0) return nil;
    return [NSDictionary dictionaryFromJSONString:permissions];
}
- (void)setUserPermissions:(NSDictionary *)userPermissions {
    NSString *permissions = [userPermissions convertJSONDictionaryToString];
    [self setString:permissions forKey:SETTING_USER_PERMISSIONS];
}

// returns the permission string (combination of [c]reate, [u]pdate, [d]elete or empty string for read-only) for a recordType string, or nil if that recordType doesn't exist
- (NSString *)permissionForRecordType:(NSString *)recordType {
    NSDictionary *permissions = [self userPermissions];
    if (permissions[recordType]) return permissions[recordType];
    return nil;
}

// returns YES if the user has create permissions for record type
- (BOOL)hasCreatePermissionsForType:(NSString *)recordType {
    NSDictionary *permissions = [self userPermissions];
    NSString *permission = permissions[recordType];
    if (! permission) return NO;
    return [permission containsString:PERMISSION_CREATE];
}


#pragma mark - Sync

// current server migration version, set to NO_SERVER_MIGRATION_REQUIRED if no migration required at this time
- (NSInteger)migrationVersion { return [self integerForKey:SETTING_MIGRATION_VERSION]; }
- (void)setMigrationVersion:(NSInteger)migrationVersion { [self setInteger:migrationVersion forKey:SETTING_MIGRATION_VERSION]; }

// current database migration version
- (NSInteger)serverMigrationVersion { return [self integerForKey:SETTING_SERVER_MIGRATION_VERSION]; }
- (void)setServerMigrationVersion:(NSInteger)serverMigrationVersion { [self setInteger:serverMigrationVersion forKey:SETTING_SERVER_MIGRATION_VERSION]; }

// UTC datetime of last sync, stored only
- (NSString *)syncedAt { return [self stringForKey:SETTING_SYNCED_AT]; }
- (void)setSyncedAt:(NSString *)syncedAt { [self setString:syncedAt forKey:SETTING_SYNCED_AT]; }

// array of sync error strings
- (NSArray *)syncErrors {
    NSString *errors = [self stringForKey:SETTING_SYNC_ERRORS];
    
    NSArray *syncErrors;
    if (errors.length == 0) syncErrors = @[];
    else syncErrors = [errors componentsSeparatedByString:@"~||~"];
    
    return syncErrors;
}
- (void)setSyncErrors:(NSArray *)syncErrors {
    NSString *errors;
    if (! syncErrors || syncErrors.count == 0) errors = @"";
    else errors = [syncErrors componentsJoinedByString:@"~||~"];
    
    [self setString:errors forKey:SETTING_SYNC_ERRORS];
}


#pragma mark - GPS Location

// delegate callback for location services, called whenever location changes significantly with app in foreground
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    self.lastLocation = newLocation;
    
    // notify anyone who cares that our location has updated
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_GPS_LOCATION_CHANGED object:nil];
    });
}

// delegate callback invoked whenever location service usage permission changes (initial dialog or via Settings)
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) [self.locationManager startUpdatingLocation];  // on whenever allowed
    else [self.locationManager stopUpdatingLocation];                                                       // off while not allowed
}

- (BOOL)locationServiceEnabled { return CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse; }
- (double)currentLatitude  { return self.lastLocation.coordinate.latitude; }
- (double)currentLongitude { return self.lastLocation.coordinate.longitude; }

// returns dictionary with most recent GPS location update lat/long as NSNumber doubles, or chs headquarters if not set yet
- (NSDictionary *)currentLocation {
    double lat = [self currentLatitude];
    double lon = [self currentLongitude];
    if (lat == 0 && lon == 0) {
        lat = 44.869479;
        lon = -93.062548;
    }
    
    return @{ @"latitude": [NSNumber numberWithDouble:lat],
              @"longitude":[NSNumber numberWithDouble:lon] };
}

@end
