//
//  FieldStatus.m
//  Field Journal
//
//  Created by Elia Freedman on 2/9/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "FieldStatus.h"
#import "Session.h"
#import "FMDB.h"
#import "UIColor+Color.h"


@implementation FieldStatus

- (id)initWithId:(NSInteger)dbId name:(NSString *)name key:(NSString *)key {
    self = [super init];
    if (! self) return nil;
    
    _dbId = dbId;
    _name = ((NSNull *)name == [NSNull null] ? nil : name);
    _key = key;
    
    UIImage *icon;
    FieldStatusType status = FieldStatusNotFound;
    if ([key isEqualToString:@"GOOD"]) {
        icon = [UIImage imageNamed:@"StatusGreen"];
        status = FieldStatusGood;
    } else if ([key isEqualToString:@"ATTENTION_SOON"]) {
        icon = [UIImage imageNamed:@"StatusYellow"];
        status = FieldStatusWarning;
    } else if ([key isEqualToString:@"IMMEDIATE_ATTENTION"]) {
        icon = [UIImage imageNamed:@"StatusRed"];
        status = FieldStatusAlert;
    }
    _icon = icon;
    _status = status;
    
    return self;
}

// store and update field statuses, returns array of error strings or empty array
+ (NSArray *)storeFieldStatuses:(NSArray *)statuses {
    Session *session = [Session shared];
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *status in statuses) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldstatuses WHERE server_id=?", status[@"id"]];
            if ([results next]) {
                // update the existing record
                [db executeUpdate:@"UPDATE fieldstatuses SET key=?,name=?,score=? WHERE server_id=?", status[@"key"], status[@"name"], status[@"score"], results[@"server_id"]];
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO fieldstatuses (server_id,key,name,score) VALUES (?,?,?,?)", status[@"id"], status[@"key"], status[@"name"], status[@"score"]];
            }
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeFieldStatuses: %@", [db lastErrorMessage]]];
            [results close];
        }
    
    }];
    
    return errors;
}

// returns a FieldStatus object given a database query
+ (FieldStatus *)fieldStatusWithResultSet:(FMResultSet *)results {
    return [[FieldStatus alloc] initWithId:[results[@"id"] integerValue] name:results[@"name"] key:results[@"key"]];
}

// returns a FieldStatus object given an id or null if no match
+ (FieldStatus *)fieldStatusForId:(NSInteger)fieldStatusId {
    Session *session = [Session shared];
    __block FieldStatus *fieldStatus;

    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldstatuses WHERE server_id=?", @(fieldStatusId)];
        if ([results next]) {
            fieldStatus = [self fieldStatusWithResultSet:results];
        }
        [results close];
    
    }];
    
    return fieldStatus;
}

// returns a FieldStatus object given a status
+ (FieldStatus *)fieldStatusForStatus:(FieldStatusType)status {
    Session *session = [Session shared];
    __block FieldStatus *fieldStatus;
    NSString *key;
    
    switch (status) {
        case FieldStatusGood :      key = @"GOOD";                      break;
        case FieldStatusWarning :   key = @"ATTENTION_SOON";            break;
        case FieldStatusAlert :     key = @"IMMEDIATE_ATTENTION";       break;
        default:                    key = @"";
    }
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldstatuses WHERE key=?", key];
        if ([results next]) {
            fieldStatus = [self fieldStatusWithResultSet:results];
        }
        [results close];
    
    }];
    
    return fieldStatus;
}

@end
