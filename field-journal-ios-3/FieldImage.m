//
//  FieldImage.m
//  Field Journal
//
//  Created by Rick Huebner on 10/10/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "FieldImage.h"
#import "Session.h"
#import "FMDB.h"
#import "UIImage+Image.h"
#import "NSDictionary+Dictionary.h"
#import "NSDateFormatter+DateFormat.h"


@implementation FieldImage

- (id)initWithServerId:(NSInteger)serverId createdAt:(NSString *)createdAt filename:(NSString *)filename category:(NSString *)category resolution:(NSString *)resolution permissions:(NSString *)permissions ulCoords:(NSString *)ul lrCoords:(NSString *)lr {
    self = [super init];
    if (! self) return nil;
    
    _serverId = serverId;
    _createdAt = createdAt;
    _filename = filename;
    _categoryKey = category;
    _resolution = resolution;
    _permissions = ((NSNull *)permissions == [NSNull null] ? [[Session shared] permissionForRecordType:RECORD_TYPE_FIELD_IMAGES] : permissions);

    NSDictionary *ulDict = [NSDictionary dictionaryFromJSONString:ul];
    if ([ulDict[@"type"] isEqualToString:@"Point"]) {
        NSArray *coords = ulDict[@"coordinates"];
        _ulCoords.latitude = [coords[1] doubleValue];
        _ulCoords.longitude = [coords[0] doubleValue];
    }
    
    NSDictionary *lrDict = [NSDictionary dictionaryFromJSONString:lr];
    if ([lrDict[@"type"] isEqualToString:@"Point"]) {
        NSArray *coords = lrDict[@"coordinates"];
        _lrCoords.latitude = [coords[1] doubleValue];
        _lrCoords.longitude = [coords[0] doubleValue];
    }
    
    return self;
}

// returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE
- (BOOL)hasPermissionForType:(NSString *)permissionType {
    if (! permissionType || ! self.permissions) return NO;
    return [self.permissions containsString:permissionType];
}


// set record availability, returning an array of records needed
+ (NSArray *)setAvailability:(NSArray *)images {
    Session *session = [Session shared];
    NSMutableArray *records = [NSMutableArray arrayWithArray:images];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        // look at every record in the database, decide whether to keep it or remove it, whether to update its permissions, or whether to
        // request it from the server if it doesn't already exist or if the version has changed
        FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldimages"];
        while ([results next]) {
            BOOL found = NO;
            for (NSInteger i = 0; i < records.count; i++) {
                NSDictionary *image = [records objectAtIndex:i];
                if ([results[@"server_id"] integerValue] == [image[@"id"] integerValue]) {
                    [db executeUpdate:@"UPDATE fieldimages SET permissions=? WHERE server_id=?", image[@"r"], results[@"server_id"]];
                    if (RECORD_VERSION_FOR_FIELD_IMAGE == [results[@"version"] integerValue]) [records removeObjectAtIndex:i];
                    found = YES;
                    break;
                }
            }
            if (! found) [db executeUpdate:@"UPDATE fieldimages SET state='permission_removed' WHERE server_id=?", results[@"server_id"]];
        }
        [results close];
    
    }];
    
    // only return an array of ids
    NSMutableArray *neededRecords = [NSMutableArray array];
    for (NSInteger i = 0; i < records.count; i++) {
        NSDictionary *image = [records objectAtIndex:i];
        [neededRecords addObject:image[@"id"]];
    }
    
    return neededRecords;
}

// store and update images for fields, returns array of error strings or empty array
+ (NSArray *)storeFieldImages:(NSArray *)images {
    Session *session = [Session shared];
    NSMutableArray *errors = [NSMutableArray array];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *image in images) {
            FMResultSet *field = [db executeQuery:@"SELECT * FROM fields WHERE server_id=?", image[@"field"]];
            if (! [field next]) {
                [errors addObject:[NSString stringWithFormat:@"storeFieldImages: No field with server id %@ for field image with server id %@", image[@"field"], image[@"id"]]];
                [field close];
                continue;
            }
            
            FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldimages WHERE server_id=?", image[@"id"]];
            NSString *filename = [NSString stringWithFormat:@"field_image_%@.png", image[@"id"]];
            NSString *ul = [image[@"ulLocation"] convertJSONDictionaryToString];
            NSString *lr = [image[@"lrLocation"] convertJSONDictionaryToString];
            if ([results next]) {
                // update the existing record
                [db executeUpdate:@"UPDATE fieldimages SET field_id=?, filename=?, category=?, ul=?, lr=?, resolution=?, state=?, created_at=?, permissions=?, version=? WHERE server_id=?", image[@"field"], filename, image[@"category"], ul, lr,  image[@"resolution"], @"", image[@"imageDate"], image[@"r"], @(RECORD_VERSION_FOR_FIELD_IMAGE), results[@"server_id"]];
                
                if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeFieldImages: %@", [db lastErrorMessage]]];
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO fieldimages (server_id, field_id, filename, category, ul, lr, resolution, state, created_at, permissions, version) VALUES (?,?,?,?,?,?,?,?,?,?,?)", image[@"id"], image[@"field"], filename, image[@"category"], ul, lr, image[@"resolution"], @"", image[@"imageDate"], image[@"r"], @(RECORD_VERSION_FOR_FIELD_IMAGE)];
                if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeFieldImages: %@", [db lastErrorMessage]]];
            }
            [results close];
            [field close];
        }
    
    }];
    
    return errors;
}

// returns a\n FieldImage object given a database query
+ (FieldImage *)fieldImageWithResultSet:(FMResultSet *)results {
    return [[FieldImage alloc] initWithServerId:[results[@"server_id"] integerValue] createdAt:results[@"created_at"] filename:results[@"filename"] category:results[@"category"] resolution:results[@"resolution"] permissions:results[@"permissions"] ulCoords:results[@"ul"] lrCoords:results[@"lr"]];
}

// delete images
+ (void)deleteFieldImages:(NSArray <NSNumber *> *)images {
    Session *session = [Session shared];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSNumber *imageId in images) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldimages WHERE server_id=?", imageId];
            if ([results next]) {
                [UIImage deleteImage:results[@"filename"]];
                [db executeUpdate:@"DELETE FROM fieldimages WHERE server_id=?", results[@"server_id"]];
            }
            [results close];
        }
        
        FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldimages WHERE state='permission_removed'"];
        while ([results next]) {
            [UIImage deleteImage:results[@"filename"]];
        }
        [results close];
        [db executeUpdate:@"DELETE FROM fieldimages WHERE state='permission_removed'"];
    
    }];
}

// returns an array of dictionaries comprising server_id and filename keys of images that need to be downloaded
+ (NSMutableArray *)fieldImagesToDownload {
    Session *session = [Session shared];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *imagesInFileManager = [fileManager contentsOfDirectoryAtPath:[session imagesPath] error:nil];
    NSMutableArray *imagesToDownload = [NSMutableArray array];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM fieldimages"];
        while ([results next]) {
            BOOL found = NO;
            NSString *imageInDB = results[@"filename"];
            for (NSString *imageInFM in imagesInFileManager) {
                if ([imageInDB isEqualToString:imageInFM]) {
                    found = YES;
                    break;
                }
            }
            if (! found) [imagesToDownload addObject:@{@"server_id" : results[@"server_id"], @"filename" : imageInDB, @"imageType" : @"field"}];
        }
        [results close];
    
    }];
    
    return imagesToDownload;
}

// returns true if there are field images for a given fieldId
+ (BOOL)hasFieldImagesForFieldId:(NSInteger)fieldId {
    Session *session = [Session shared];
    __block int count;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        count = [db intForQuery:@"SELECT COUNT(*) FROM fieldimages WHERE field_id=?", @(fieldId)];
    
    }];
    
    return count > 0;
}

// returns an array of field images given a fieldId and categoryKey in created_at order (newest first)
+ (NSArray <FieldImage *> *)fieldImagesForFieldId:(NSInteger)fieldId categoryKey:(NSString *)categoryKey {
    Session *session = [Session shared];
    NSMutableArray *images = [NSMutableArray array];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        NSString *catClause = categoryKey ? [NSString stringWithFormat:@"AND category='%@' ", categoryKey] : @"";
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM fieldimages WHERE field_id=? %@AND state != 'deleted' ORDER BY created_at DESC", catClause];
        FMResultSet *results = [db executeQuery:query, @(fieldId)];
        while ([results next]) {
            FieldImage *image = [self fieldImageWithResultSet:results];
            [images addObject:image];
        }
        [results close];
    
    }];
    
    return images;
}

// returns the images for a given field id in any category in created_at order (newest first)
+ (NSArray <FieldImage *> *)fieldImagesForFieldId:(NSInteger)fieldId {
    return [FieldImage fieldImagesForFieldId:fieldId categoryKey:nil];
}

@end
