//
//  Tag.h
//  Field Journal
//
//  Created by Elia Freedman on 2/9/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Tag : NSObject

+ (NSArray *)storeTags:(NSArray *)tags;

@end
