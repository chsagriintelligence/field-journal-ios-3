//
//  Globals.h
//  field-journal-ios-3
//
//  Created by Elia Freedman on 1/18/17.
//  Copyright © 2017 CHS, Inc. All rights reserved.
//

#ifndef Globals_h
#define Globals_h


#define Localize(key)                   NSLocalizedString(key, @"")


#define ANIMATION_LENGTH_VERY_SHORT     0.10
#define ANIMATION_LENGTH                0.35
#define ANIMATION_LENGTH_LONG           1.00
#define ROTATION_DELAY   0.5


#define FULL_PERMISSIONS                                    @"crud"

// notifications
//#define NOTIFICATION_LOGIN_REQUIRED                         @"notificationLoginRequired"
#define NOTIFICATION_UPDATE_REPORTS_BADGE                   @"notificationReportsBadge"
#define NOTIFICATION_UPDATE_DATA_GROWERS                    @"notificationUpdateGrowers"
#define NOTIFICATION_UPDATE_DATA_FARMS                      @"notificationUpdateFarms"
//#define NOTIFICATION_UPDATE_DATA_FIELDS                     @"notificationUpdateFields"
//#define NOTIFICATION_UPDATE_DATA_REPORTS                    @"notificationUpdateReports"
//#define NOTIFICATION_UPDATE_DATA_OBSERVATIONS               @"notificationUpdateObservations"
//#define NOTIFICATION_UPDATE_DATA_LAYERS                     @"notificationUpdateLayers"
#define NOTIFICATION_SYNC_IN_PROGRESS                       @"notificationSyncInProgress"
#define NOTIFICATION_VISIBLE_RECORD_CHANGED                 @"notificationVisibleRecordChanged"
#define NOTIFICATION_GPS_LOCATION_CHANGED                   @"notificationGpsLocationChanged"


// the current expected version of each record in the database
#define RECORD_VERSION_FOR_USER             1
#define RECORD_VERSION_FOR_GROWER           2
#define RECORD_VERSION_FOR_FARM             2
#define RECORD_VERSION_FOR_FIELD            1
#define RECORD_VERSION_FOR_REPORT           2
#define RECORD_VERSION_FOR_OBSERVATION      2
#define RECORD_VERSION_FOR_IMAGE            2
#define RECORD_VERSION_FOR_FIELD_IMAGE      1
#define RECORD_VERSION_FOR_COOP             1


#endif /* Globals_h */
