//
//  LoginViewController.h
//  field-journal-ios-3
//
//  Created by Elia Freedman on 1/20/17.
//  Copyright © 2017 CHS, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LoginViewController;


@protocol LoginViewDelegate <NSObject>

- (void)loginCredentialsAccepted:(LoginViewController *)loginViewController;

@end


@interface LoginViewController : UIViewController

@property (retain) id <LoginViewDelegate> delegate;

@end
