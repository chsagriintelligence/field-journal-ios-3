//
//  FieldImageCategory.h
//  Field Journal
//
//  Created by Rick Huebner on 10/20/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FieldImage.h"

@interface FieldImageCategory : NSObject

+ (FieldImageCategory *)categoryForKey:(NSString *)key; // returns a category object given a field image category key

+ (NSArray <FieldImageCategory *> *)allCategories;      // returns an array of all categories in ordinal order
// returns an array of category objects for field images with a given fieldId in ordinal order
+ (NSArray <FieldImageCategory *> *)categoriesForFieldImagesWithFieldId:(NSInteger)fieldId;

+ (NSArray *)storeCategories:(NSArray *)categories;     // store and update field image categories

// returns an array of dictionaries comprising urls and filenames of category legends that need to be downloaded
// also removes old legend files which are no longer needed
+ (NSMutableArray *)legendsToDownload;

@property (nonatomic) NSString *name;                   // category displayable name
@property (nonatomic) NSString *key;                    // category lookup key as used in field image items
@property (nonatomic) NSString *legendDescription;      // category legend description if set, or nil if none

- (UIImage *)fieldImageLegend;                          // loaded legend image if set, or nil if none

@end
