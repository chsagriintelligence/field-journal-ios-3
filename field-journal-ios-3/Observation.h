//
//  Observation.h
//  Field Journal
//
//  Created by Elia Freedman on 1/20/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIColor+Color.h"
@class Image;
@class FMDatabase;


@interface Observation : NSObject

+ (Observation *)observationForId:(NSInteger)observationId;         // returns an Observation object given a database id

+ (NSArray <Observation *> *)allObservations;                       // returns an array of observations that are not associated with locked reports

// returns the observations for a given report id
+ (NSArray <Observation *> *)observationsForReportId:(NSInteger)reportId lockedOnly:(BOOL)lockedOnly;

// create and return a new observation given a report database id, layer database id, and dictionary of latitude and longitude coordinates
+ (Observation *)newObservationForReportId:(NSInteger)reportId layerId:(NSInteger)layerId coordinates:(NSDictionary *)coordinates;

+ (NSArray *)setAvailability:(NSArray *)observations;               // set record availability, returning an array of records needed
// store and update observations, returns array of error strings or empty array
+ (NSArray *)storeObservations:(NSArray *)observations;

+ (void)deleteObservations:(NSArray *)observations;                 // delete observations
+ (void)deleteObservationsForReport:(NSNumber *)reportId;           // deletes all observations with a given report id

+ (NSArray *)modifiedObservations;                                  // returns an array of modified observations or nil if there are none
+ (NSArray *)deletedObservations;                                   // returns an array of deleted observation server ids or nil if there are none
+ (void)clearModifiedDuringSync;                                    // clear the modified_during_state flags on all records


@property (nonatomic) NSInteger dbId;                               // database id
@property (nonatomic) NSInteger reportId;                           // database id for the associated report
@property (nonatomic) NSInteger layerId;                            // database id for the associated layer (i.e., observation type)
@property (nonatomic) NSString *date;                               // date-time stamp for when observation occurred in UTC
@property (nonatomic) NSString *name;                               // observation's name
@property (nonatomic) NSString *comment;                            // observation's comment
@property (nonatomic) NSString *creator;                            // creator of this observation
@property (nonatomic) NSDictionary *properties;                     // dictionary of properties where each key is the properties database id
@property (nonatomic) NSDictionary *boundary;                       // dictionary describing the observation's boundary
@property (nonatomic) BOOL locked;                                  // YES if the observation is locked, NO otherwise
@property (nonatomic) NSString *permissions;                        // permissions
@property (nonatomic) NSArray <Image *> *images;                    // array of Image objects


// returns a screen-printable name for a layer's name and key
- (NSString *)nameForLayerName:(NSString *)layerName layerKey:(NSString *)layerKey;

- (void)save;                                                       // save observation changes
- (void)updateReport:(NSInteger)reportId;                           // updates the report given a new reportId
- (void)loadImages;                                                 // load images into the Observation object
- (void)destroy;                                                    // delete observation
- (BOOL)hasPermissionForType:(NSString *)permissionType;            // returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE

- (id)propertyObjectForKey:(NSString *)key;                         // returns a property object for a given key
- (Image *)imageForImageId:(NSInteger)imageId;                      // returns an image object given an image id or nil if not found
- (void)addImageWithFilename:(NSString *)filename;                  // creates a new image and adds it to the observation object's images

@end
