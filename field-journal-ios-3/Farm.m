//
//  Farm.m
//  Field Journal
//
//  Created by Elia Freedman on 1/19/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Farm.h"
#import "Field.h"
#import "Grower.h"
#import "Session.h"
#import "FMDB.h"
#import "NSString+String.h"
#import "NSNumber+Number.h"


@implementation Farm

- (id)initWithId:(NSInteger)dbId name:(NSString *)name growerId:(NSInteger)growerId permissions:(NSString *)permissions coopId:(NSInteger)coopId {
    self = [super init];
    if (! self) return nil;
    
    _dbId = dbId;
    _name = ((NSNull *)name == [NSNull null] ? @"" : name);
    _growerId = growerId;
    _permissions = ((NSNull *)permissions == [NSNull null] ? [[Session shared] permissionForRecordType:RECORD_TYPE_FARMS] : permissions);
    _coopId = coopId;

    return self;
}

// save farm changes
- (void)save {
    Session *session = [Session shared];
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
    NSString *name = (self.name.length == 0 ? (NSString *)[NSNull null] : self.name);
    NSNumber *coopIdAsObject = (self.coopId == NSNotFound) ? (NSNumber *)[NSNull null] : [NSNumber numberWithInteger:self.coopId];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        [db executeUpdate:@"UPDATE farms SET state='updated',name=?,coop_id=? WHERE server_id=?", name, coopIdAsObject, idAsObject];
        if ([db hadError]) {
            NSLog(@"save error: %@", [db lastErrorMessage]);
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_FARMS object:nil];
        }
    
    }];
}

// updates the farm given a new growerId
- (void)updateGrower:(NSInteger)growerId {
    Session *session = [Session shared];
    NSNumber *farmIdAsObject = @(self.dbId);
    NSNumber *originalGrowerIdAsObject = @(self.growerId);
    NSNumber *growerIdAsObject = @(growerId);
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        [db executeUpdate:@"DELETE FROM farm_grower WHERE grower_id=? AND farm_id=?", originalGrowerIdAsObject, farmIdAsObject];
        [db executeUpdate:@"INSERT INTO farm_grower (farm_id,grower_id) VALUES (?,?)", farmIdAsObject, growerIdAsObject];
        if ([db hadError]) {
            NSLog(@"save error: %@", [db lastErrorMessage]);
        } else {
            self.growerId = growerId;
        }
    
    }];
}

// delete farm
- (void)destroy {
    Session *session = [Session shared];
    NSNumber *idAsObject = @(self.dbId);
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE server_id=?", idAsObject];
        if ([results next]) {
            if (results[@"server_id"] == [NSNull null]) {
                // hasn't been synced yet, remove it immediately
                [db executeUpdate:@"DELETE FROM farms WHERE server_id=?", idAsObject];
                [db executeUpdate:@"DELETE FROM farm_grower WHERE grower_id=? AND farm_id=?", @(self.growerId), idAsObject];
            } else {
                // has been synced, need to alert the server
                [db executeUpdate:@"UPDATE farms SET state='deleted' WHERE server_id=?", idAsObject];
                if ([db hadError]) NSLog(@"delete error: %@", [db lastErrorMessage]);
            }
            [Field deleteFieldsForFarm:idAsObject];
        }
        [results close];
    
    }];
}

// returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE
- (BOOL)hasPermissionForType:(NSString *)permissionType {
    if (! permissionType || ! self.permissions) return NO;
    return [self.permissions containsString:permissionType];
}

// returns Coop object this farm is member of, or nil if none
- (Coop *)coop {
    return [Coop coopForId:self.coopId];
}


// create and return a new farm given a growerId
+ (Farm *)newFarmForGrowerId:(NSInteger)growerId {
    Session *session = [Session shared];
    NSString *uuid = [NSString nameForFileType:nil];
    __block Farm *farm;
    
    // new farms default to being in the grower's first selected coop, or null if none selected
    NSArray *coops = [Coop coopsForGrowerId:growerId];
    NSNumber *coopIdAsObject = (coops.count > 0) ? @([coops[0] dbId]) : (NSNumber *)[NSNull null];
    NSInteger coopId = (coops.count > 0) ? [coops[0] dbId] : NSNotFound;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        [db executeUpdate:@"INSERT INTO farms (uuid, state, permissions, coop_id, version) VALUES (?,?,?,?,?)", uuid, @"updated", FULL_PERMISSIONS, coopIdAsObject, @(RECORD_VERSION_FOR_FARM)];
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE uuid=?", uuid];
        if ([results next]) {
            [db executeUpdate:@"INSERT INTO farm_grower (farm_id,grower_id) VALUES (?,?)", results[@"server_id"], @(growerId)];
            farm = [[Farm alloc] initWithId:[results[@"server_id"] integerValue] name:@"" growerId:growerId permissions:results[@"permissions"] coopId:coopId];
        }
        [results close];
    
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_FARMS object:nil];
    
    return farm;
}

// set record availability, returning an array of records needed
+ (NSArray *)setAvailability:(NSArray *)farms {
    Session *session = [Session shared];
    NSMutableArray *records = [[NSMutableArray alloc] initWithArray:farms];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        // look at every record in the database, decide whether to keep it or remove it, whether to update its permissions, or whether to
        // request it from the server if it doesn't already exist or if the version has changed
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms"];
        while ([results next]) {
            BOOL found = NO;
            for (NSInteger i = 0; i < records.count; i++) {
                NSDictionary *farm = [records objectAtIndex:i];
                if (results[@"server_id"] != [NSNull null] && [results[@"server_id"] integerValue] == [farm[@"id"] integerValue]) {
                    [db executeUpdate:@"UPDATE farms SET permissions=? WHERE server_id=?", farm[@"r"], results[@"server_id"]];
                    if (RECORD_VERSION_FOR_FARM == [results[@"version"] integerValue]) [records removeObjectAtIndex:i];
                    found = YES;
                    break;
                }
            }
            if (! found) [db executeUpdate:@"UPDATE farms SET state='permission_removed' WHERE server_id=?", results[@"server_id"]];
        }
        
        // update the farm-grower relationships
        for (NSDictionary *farm in farms) {
            FMResultSet *farmResult = [db executeQuery:@"SELECT * FROM farms WHERE server_id=?", farm[@"id"]];
            if ([farmResult next]) {
                NSNumber *dbId = farmResult[@"server_id"];
                
                // create an array of grower database ids
                NSMutableArray <NSNumber *> *growers = [[NSMutableArray alloc] init];
                for (NSNumber *grower in farm[@"growers"]) {
                    FMResultSet *growerResult = [db executeQuery:@"SELECT * FROM growers WHERE server_id=?", grower];
                    if ([growerResult next]) {
                        [growers addObject:growerResult[@"server_id"]];
                    }
                }
                
                // add missing records
                for (NSNumber *grower in growers) {
                    FMResultSet *farmGrowerResults = [db executeQuery:@"SELECT * FROM farm_grower WHERE farm_id=? AND grower_id=?", dbId, grower];
                    if (! [farmGrowerResults next]) {
                        [db executeUpdate:@"INSERT INTO farm_grower (farm_id, grower_id) VALUES (?,?)", dbId, grower];
                    }
                }
                
                // remove extra records (except for new farms/growers)
                FMResultSet *growersForFarmResults = [db executeQuery:@"SELECT * FROM farm_grower WHERE farm_id=?", farmResult[@"server_id"]];
                while ([growersForFarmResults next]) {
                    FMResultSet *growerResult = [db executeQuery:@"SELECT * FROM growers WHERE server_id=?", growersForFarmResults[@"grower_id"]];
                    [growerResult next];
                    if (growerResult[@"server_id"] != [NSNull null] && [growersForFarmResults[@"grower_id"] containedWithinArray:growers] == NSNotFound) {
                         [db executeUpdate:@"DELETE FROM farm_grower WHERE farm_id=? AND grower_id=?", growersForFarmResults[@"farm_id"], growersForFarmResults[@"grower_id"]];
                    }
                }
                [growersForFarmResults close];
            }
            [farmResult close];
        }
        [results close];
    
    }];

    // only return an array of ids
    NSMutableArray *neededRecords = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < records.count; i++) {
        NSDictionary *farm = [records objectAtIndex:i];
        [neededRecords addObject:farm[@"id"]];
    }
    
    return neededRecords;
}

// store and update farms, returns array of error strings or empty array
+ (NSArray *)storeFarms:(NSArray *)farms {
    Session *session = [Session shared];
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *farm in farms) {
            NSString *uuid = [farm[@"clientId"] uppercaseString];

            FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE uuid=?", uuid];
            if ([results next]) {
                // update the existing record
                if ([results[@"modified_during_sync"] boolValue]) {
                    [db executeUpdate:@"UPDATE farms SET server_id=? WHERE server_id=?", farm[@"id"], results[@"server_id"]];
                } else {
                    [db executeUpdate:@"UPDATE farms SET server_id=?, name=?, coop_id=?, uuid=?, state='', permissions=?, version=? WHERE id=?", farm[@"id"], farm[@"name"], farm[@"coop"], uuid, farm[@"r"], @(RECORD_VERSION_FOR_FARM), results[@"id"]];
                }
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO farms (server_id, name, coop_id, uuid, permissions, version) VALUES (?,?,?,?,?,?)", farm[@"id"], farm[@"name"], farm[@"coop"], uuid, farm[@"r"], @(RECORD_VERSION_FOR_FARM)];
            }
            
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeFarms: %@", [db lastErrorMessage]]];
            [results close];
        }
    
    }];
    
    return errors;
}

// store grower-farm relationships
+ (NSArray *)storeGrowersForFarms:(NSArray *)farms {
    Session *session = [Session shared];
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *farm in farms) {
            NSArray *growers = farm[@"growers"];
            for (NSNumber *grower_server_id in growers) {
                FMResultSet *farmResult = [db executeQuery:@"SELECT * FROM farms WHERE server_id=?", farm[@"id"]];
                [farmResult next];
                
                FMResultSet *growerResult = [db executeQuery:@"SELECT * FROM growers WHERE server_id=?", grower_server_id];
                if ([growerResult next]) {
                    FMResultSet *farmGrowerResult = [db executeQuery:@"SELECT * FROM farm_grower WHERE farm_id=? AND grower_id=?", farmResult[@"server_id"], growerResult[@"server_id"]];
                    if (! [farmGrowerResult next]) {
                        [db executeUpdate:@"INSERT INTO farm_grower (farm_id,grower_id) VALUES (?,?)", farmResult[@"server_id"], growerResult[@"server_id"]];
                        if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeGrowerFarm (storeFarms object): %@", [db lastErrorMessage]]];
                    }
                } else {
                    [errors addObject:[NSString stringWithFormat:@"storeGrowerFarm: No grower %@ for farm server id %@", grower_server_id, farm[@"id"]]];
                }
                [growerResult close];
                [farmResult close];
            }
        }
    
    }];
    
    return errors;
}

// returns an array of modified farms or nil if there are none
+ (NSArray *)modifiedFarms {
    Session *session = [Session shared];
    NSMutableArray *modifiedRecords = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE state='updated'"];
        while ([results next]) {
            if (results[@"name"] != [NSNull null]) {
                NSMutableDictionary *record = [[NSMutableDictionary alloc] init];
                
                record[@"id"] = results[@"server_id"];
                record[@"clientId"] = results[@"uuid"];
                record[@"coop"] = results[@"coop_id"];
                FMResultSet *farmGrowerResults = [db executeQuery:@"SELECT * FROM farm_grower WHERE farm_id=?", results[@"id"]];
                [farmGrowerResults next];
                FMResultSet *growerResults = [db executeQuery:@"SELECT * FROM growers WHERE id=?", farmGrowerResults[@"grower_id"]];
                if ([growerResults next]) record[@"grower"] = growerResults[@"uuid"];
                record[@"name"] = results[@"name"];
                
                [modifiedRecords addObject:record];
            }
        }
        [results close];
    
    }];
    
    return ([modifiedRecords count] == 0 ? nil : modifiedRecords);
}

// check if there are incomplete farm records
+ (BOOL)hasIncompleteFarms {
    Session *session = [Session shared];
    __block BOOL error = NO;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE state='updated' AND name IS NULL"];
        if ([results next]) error = YES;
        [results close];
    }];
    
    return error;
}

// Returns an array of deleted farm server ids or nil if there are none. Will only return records where the state is marked 'deleted' and there is a server_id.
// Records without a server id are handled with a special upload flag.
+ (NSArray *)deletedFarms {
    Session *session = [Session shared];
    NSMutableArray *deletedRecords = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE state='deleted'"];
        while ([results next]) {
            [deletedRecords addObject:results[@"server_id"]];
        }
        [results close];
    
    }];
    
    return ([deletedRecords count] == 0 ? nil : deletedRecords);
}

// delete farms
+ (void)deleteFarms:(NSArray *)farms {
    Session *session = [Session shared];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSNumber *farmId in farms) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE server_id=?", farmId];
            if ([results next]) {
                [Field deleteFieldsForFarm:results[@"id"]];
                [db executeUpdate:@"DELETE FROM farms WHERE id=?", results[@"id"]];
                [db executeUpdate:@"DELETE FROM farm_grower WHERE farm_id=?", results[@"id"]];
            }
            [results close];
        }
        
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE state='permission_removed'"];
        while ([results next]) {
            [Field deleteFieldsForFarm:results[@"id"]];
            [db executeUpdate:@"DELETE FROM farms WHERE id=?", results[@"id"]];
            [db executeUpdate:@"DELETE FROM farm_grower WHERE farm_id=?", results[@"id"]];
        }
        [results close];
    
    }];
}

// deletes all farms with a given grower id
+ (void)deleteFarmsForGrower:(NSNumber *)growerId {
    Session *session = [Session shared];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farm_grower WHERE grower_id=?", growerId];
        while ([results next]) {
            [Field deleteFieldsForFarm:results[@"farm_id"]];
            [db executeUpdate:@"DELETE FROM farm_grower WHERE grower_id=? AND farm_id=?", growerId, results[@"farm_id"]];
            [db executeUpdate:@"DELETE FROM farms WHERE id=?", results[@"farm_id"]];
        }
        [results close];
    }];
}

// clear the modified_during_state flags on all records
+ (void)clearModifiedDuringSync {
    Session *session = [Session shared];
    NSNumber *trueAsObject = [NSNumber numberWithBool:YES];
    NSNumber *falseAsObject = [NSNumber numberWithBool:NO];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE modified_during_sync=?", trueAsObject];
        while ([results next]) {
            [db executeUpdate:@"UPDATE farms SET modified_during_sync=? WHERE id=?", falseAsObject, results[@"id"]];
        }
        [results close];
    
    }];
}

// returns YES if this farm is a duplicate of another
+ (BOOL)isDuplicateName:(NSString *)name {
    Session *session = [Session shared];
    __block BOOL isDuplicate = NO;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE name=? AND state!='deleted' COLLATE NOCASE", name];
        if ([results next]) {
            isDuplicate = YES;
        }
        [results close];
    
    }];
    
    return isDuplicate;
}

// returns a Farm object given a database query
+ (Farm *)farmWithResultSet:(FMResultSet *)results {
    Session *session = [Session shared];
    __block NSInteger growerId = NSNotFound;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *growerResults = [db executeQuery:@"SELECT * FROM farm_grower WHERE farm_id=?", results[@"id"]];
        if ([growerResults next]) {
            growerId = [growerResults[@"grower_id"] integerValue];
        }
        [growerResults close];
    }];
    
    NSInteger coopId = (results[@"coop_id"] == [NSNull null]) ? NSNotFound : [results[@"coop_id"] integerValue];
    return [[Farm alloc] initWithId:[results[@"id"] integerValue] name:results[@"name"] growerId:growerId permissions:results[@"permissions"] coopId:coopId];
}

// returns an array of all farms in alphabetical order; if onlyNamedFarms is YES than skip blank names
+ (NSArray <Farm *> *)allFarmsWithNamesOnly:(BOOL)onlyNamedFarms {
    Session *session = [Session shared];
    NSMutableArray *farms = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE state!='deleted' ORDER BY name ASC"];
        while ([results next]) {
            Farm *farm = [self farmWithResultSet:results];
            if (! onlyNamedFarms || (onlyNamedFarms && farm.name.length > 0)) [farms addObject:farm];
        }
        [results close];
    
    }];
    
    return farms;
}

// Returns an array of all farms in alphabetical order, organized by grower in alphabetical order. Each array object is a dictionary of @"grower" and @"farms" where
// @"grower" is a Grower object and farms is an array of Farm objects.
+ (NSArray <NSDictionary *> *)allFarmsAndGrowers {
    Session *session = [Session shared];
    NSMutableArray *farms = [[NSMutableArray alloc] init];
    NSArray *growers = [Grower allGrowersWithNamesOnly:YES];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (Grower *grower in growers) {
            NSMutableArray *farmsForGrower = [[NSMutableArray alloc] init];
            FMResultSet *results = [db executeQuery:@"SELECT * FROM farm_grower WHERE grower_id=?", @(grower.dbId)];
            while ([results next]) {
                FMResultSet *farmResults = [db executeQuery:@"SELECT * FROM farms WHERE id=? AND state!='deleted'", results[@"farm_id"]];
                if ([farmResults next]) {
                    Farm *farm = [self farmWithResultSet:farmResults];
                    if (farm.name.length > 0) [farmsForGrower addObject:farm];
                }
                [farmResults close];
            }
            
            if (farmsForGrower.count > 0) {
                [farmsForGrower sortUsingComparator:^NSComparisonResult(Farm *farm1, Farm *farm2) {
                    return [farm1.name compare:farm2.name];
                }];
                [farms addObject:@{@"grower" : grower, @"farms" : farmsForGrower}];
            }
            [results close];
        }
    
    }];
    
    return farms;
}

// returns a Farm object given a database id
+ (Farm *)farmForId:(NSInteger)farmId {
    Session *session = [Session shared];
    NSNumber *farmIdAsObject = [NSNumber numberWithInteger:farmId];
    __block Farm *farm;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE id=?", farmIdAsObject];
        if ([results next]) {
            farm = [self farmWithResultSet:results];
        }
        [results close];
    
    }];
    
    return farm;
}

// returns a Farm object given a field's database id
+ (Farm *)farmForFieldId:(NSInteger)fieldId {
    Session *session = [Session shared];
    NSNumber *fieldIdAsObject = [NSNumber numberWithInteger:fieldId];
    __block Farm *farm;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
    FMResultSet *fieldResults = [db executeQuery:@"SELECT * FROM fields WHERE id=?", fieldIdAsObject];
    if ([fieldResults next]) {
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farms WHERE id=?", fieldResults[@"farm_id"]];
        if ([results next]) {
            farm = [self farmWithResultSet:results];
        }
        [results close];
    }
    
    }];
    
    return farm;
}

@end
