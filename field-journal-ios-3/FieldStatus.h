//
//  FieldStatus.h
//  Field Journal
//
//  Created by Elia Freedman on 2/9/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIColor;
@class UIImage;


typedef enum {
    FieldStatusGood = 0,
    FieldStatusWarning,
    FieldStatusAlert,
    FieldStatusNotFound = NSNotFound
} FieldStatusType;


@interface FieldStatus : NSObject

+ (FieldStatus *)fieldStatusForId:(NSInteger)fieldStatusId;     // returns a FieldStatus object given an id or null if no match
+ (FieldStatus *)fieldStatusForStatus:(FieldStatusType)status;  // returns a FieldStatus object given a FieldStatusType

+ (NSArray *)storeFieldStatuses:(NSArray *)statuses;            // store and update fieldstatuses, returns array of error strings or empty array

@property (nonatomic) NSInteger dbId;
@property (nonatomic) FieldStatusType status;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *key;
@property (nonatomic) UIImage *icon;

@end
