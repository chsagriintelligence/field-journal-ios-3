//
//  Report.h
//  Field Journal
//
//  Created by Elia Freedman on 1/20/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FieldStatus.h"
@class Observation;
@class Publish;


@interface Report : NSObject


// returns an array of reports for either Fields or Tasks type that are not deleted in Date order
+ (NSArray <Report *> *)allReportsForTypeInDateOrder:(NSUInteger)type coopId:(NSInteger)coopId;
// returns an array of reports for either Fields or Tasks type that are not deleted grouped by Field
+ (NSArray <NSDictionary *> *)allReportsForTypeInFieldOrder:(NSUInteger)type coopId:(NSInteger)coopId;
#define REPORT_TYPE_FIELDS          0
#define REPORT_TYPE_TASKS           1

+ (NSArray <Report *> *)allReportsForFieldId:(NSInteger)fieldId;    // returns a date order array (descending) of Report objects for a given field
+ (Report *)reportForId:(NSInteger)reportId;                        // returns a Report object given a database id

+ (NSUInteger)countOfTaskReportsForCoopId:(NSInteger)coopId;        // returns a count of Task reports that are not deleted and not locked

+ (Report *)newReportForFieldId:(NSInteger)fieldId;                 // create and return a new report for the field with database id fieldId
+ (NSArray *)setAvailability:(NSArray *)reports;                    // set record availability, returning an array of records needed
+ (NSArray *)storeReports:(NSArray *)reports;                       // store and update reports, assumes fields have been stored first, returns array of error strings or empty array

+ (void)deleteReports:(NSArray *)reports;                           // delete reports
+ (void)deleteReportsForField:(NSNumber *)fieldId;                  // deletes all reports with a given field id

+ (NSArray *)modifiedReports;                                       // returns an array of modified reports or nil if there are none
+ (NSArray *)deletedReports;                                        // returns an array of deleted report server ids or nil if there are none
+ (void)clearModifiedDuringSync;                                    // clear the modified_during_state flags on all records


@property (nonatomic) NSInteger dbId;                               // database id
@property (nonatomic) NSString *name;                               // name
@property (nonatomic) NSString *date;                               // date-time stamp for when report occurred in UTC
@property (nonatomic) NSString *creator;                            // creator of this report
@property (nonatomic) NSArray *stage;                               // array of crop stages
@property (nonatomic) NSString *irrigation;                         // irrigation description
@property (nonatomic) NSString *weather;                            // weather description
@property (nonatomic) NSString *temperature;                        // temperature in farenheit
@property (nonatomic) NSString *recommendation;                     // recommendation
@property (nonatomic) NSString *instructions;                       // instructions
@property (nonatomic) NSInteger fieldId;                            // id of the field
@property (nonatomic) NSInteger fieldServerId;                      // server id of the field
@property (nonatomic) NSDictionary *fieldBoundary;                  // boundary for the field
@property (nonatomic) FieldStatusType fieldStatus;                  // status for the field
@property (nonatomic) NSArray <Observation *> *observations;        // array of observations associated with the report
@property (nonatomic) NSString *permissions;                        // permissions

// publish flags
@property (nonatomic) BOOL publishToIntel;
@property (nonatomic) BOOL publishToGrower;
- (NSArray <Publish *> *)publishableArray;                          // returns an array of Publish objects
- (NSInteger)publishCount;                                          // returns a count of Publish options

- (void)save;                                                       // save report changes
- (void)updateField:(NSInteger)fieldId;                             // updates the field given a new fieldId
- (void)destroy;                                                    // delete report
- (BOOL)hasPermissionForType:(NSString *)permissionType;            // returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE


@end
