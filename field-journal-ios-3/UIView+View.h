//
//  UIView+View.h
//  Field Journal
//
//  Created by Elia Freedman on 1/29/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (View)

- (void)circleOfColor:(UIColor *)color;         // change view to be a circle filled with color

@end
