//
//  Observation.m
//  Field Journal
//
//  Created by Elia Freedman on 1/20/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Observation.h"
#import "User.h"
#import "Property.h"
#import "Layer.h"
#import "Image.h"
#import "Session.h"
#import "FMDB.h"
#import "SyncExtras.h"
#import "GeoJSON.h"
#import "NSDictionary+Dictionary.h"
#import "NSDateFormatter+DateFormat.h"
#import "NSString+String.h"
#import "NSNumber+Number.h"



@implementation Observation

- (id)initWithId:(NSInteger)dbId reportId:(NSInteger)reportId layerId:(NSInteger)layerId date:(NSString *)date creator:(NSInteger)creator name:(NSString *)name comment:(NSString *)comment properties:(NSDictionary *)properties boundary:(NSDictionary *)boundary permissions:(NSString *)permissions {
    self = [super init];
    if (! self) return nil;
    
    _dbId = dbId;
    _reportId = reportId;
    _layerId = layerId;
    _date = date;
    _name = ((NSNull *)name == [NSNull null] ? nil : name);
    _creator = [User userForId:creator];
    _comment = ((NSNull *)comment == [NSNull null] ? nil : comment);
    _properties = ((NSNull *)properties == [NSNull null] ? nil : properties);
    _boundary = ((NSNull *)boundary == [NSNull null] ? nil : boundary);
    _permissions = ((NSNull *)permissions == [NSNull null] ? [[Session shared] permissionForRecordType:RECORD_TYPE_OBSERVATIONS] : permissions);
    [self loadImages];

    return self;
}

- (id)initWithId:(NSInteger)dbId reportId:(NSInteger)reportId layerId:(NSInteger)layerId date:(NSString *)date name:(NSString *)name permissions:(NSString *)permissions {
    self = [super init];
    if (! self) return nil;

    _dbId = dbId;
    _reportId = reportId;
    _layerId = layerId;
    _date = date;
    _name = ((NSNull *)name == [NSNull null] ? nil : name);
    _permissions = ((NSNull *)permissions == [NSNull null] ? [[Session shared] permissionForRecordType:RECORD_TYPE_OBSERVATIONS] : permissions);

    return self;
}


// returns a screen-printable name for a layer's name and key
- (NSString *)nameForLayerName:(NSString *)layerName layerKey:(NSString *)layerKey {
    NSString *propertyIdAsString;
    NSString *name = @"";
    NSNumber *average;
    Property *property;
    
    if ([layerKey isEqualToString:@"OBSERVATION_CORN_YIELD"]) {
        property = [Property propertyForKey:@"KERNEL_COUNT_AVERAGE"];
        propertyIdAsString = [NSString stringWithFormat:@"%ld", (long)property.dbId];
        average = self.properties[propertyIdAsString];
        if (average) {
            name = [NSString stringWithFormat:@"Kernel Count: %@ avg", [average numberWithPlaces:1]];
        } else {
            name = layerName;
        }
    } else if ([layerKey isEqualToString:@"OBSERVATION_STAND_COUNT"]) {
        property = [Property propertyForKey:@"STAND_COUNT_AVERAGE"];
        propertyIdAsString = [NSString stringWithFormat:@"%ld", (long)property.dbId];
        average = self.properties[propertyIdAsString];
        if (average) {
            name = [NSString stringWithFormat:@"Stand Count: %@ avg", [average numberWithPlaces:1]];
        } else {
            name = layerName;
        }
    } else {
        name = (self.name ? self.name : layerName);
    }

    return name;
}

// save observation changes
- (void)save {
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
    NSString *boundaryAsString = [self.boundary convertJSONDictionaryToString];
    
    // remove all nulls from the dictionary
    NSMutableDictionary *properties = [NSMutableDictionary dictionaryWithDictionary:self.properties];
    for (NSString * key in [properties allKeys]) {
        if ([[properties objectForKey:key] isKindOfClass:[NSNull class]])
            [properties removeObjectForKey:key];
    }
    self.properties = properties;
    if (properties.count == 0) self.properties = nil;
    NSString *propertiesAsString = [self.properties convertJSONDictionaryToString];
    
    NSString *name = self.name;
    if ([name length] == 0) {
        self.name = nil;
        name = (NSString *)[NSNull null];
    }
    
    NSString *comment = self.comment;
    if ([comment length] == 0) {
        self.comment = nil;
        comment = (NSString *)[NSNull null];
    }
    
    Observation *observation = [Observation observationForId:self.dbId];

    FMDatabase *db = [Session openDatabase];

    [db executeUpdate:@"UPDATE observations SET state='updated',name=?,comment=?,properties=?,boundary=? WHERE id=?", name, comment, propertiesAsString, boundaryAsString, idAsObject];
    if ([db hadError]) {
        NSLog(@"save error: %@", [db lastErrorMessage]);
    } else if (! [observation.name isEqualToString:self.name]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_OBSERVATIONS object:nil];
    }
    
    [Session closeDatabase];
}

// lock or unlock the observation
- (void)setLocked:(BOOL)locked {
    if (_locked != locked) {
        _locked = locked;
        NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
        NSNumber *lockedAsObject = [NSNumber numberWithInteger:self.locked];
        
        FMDatabase *db = [Session openDatabase];
        
        [db executeUpdate:@"UPDATE observations SET state='updated',locked=? WHERE id=?", lockedAsObject, idAsObject];
        if ([db hadError]) NSLog(@"save error: %@", [db lastErrorMessage]);
        
        [Session closeDatabase];
    }
}

// updates the report given a new reportId
- (void)updateReport:(NSInteger)reportId {
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
    NSNumber *reportIdAsObject = [NSNumber numberWithInteger:reportId];

    FMDatabase *db = [Session openDatabase];

    [db executeUpdate:@"UPDATE observations SET state='updated',report_id=? WHERE id=?", reportIdAsObject, idAsObject];
    if ([db hadError]) {
        NSLog(@"save error: %@", [db lastErrorMessage]);
    } else {
        self.reportId = reportId;
    }

    [Session closeDatabase];
}

// delete observation
- (void)destroy {
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM observations WHERE id=?", idAsObject];
    if ([results next]) {
        if (results[@"server_id"] == [NSNull null]) {
            // hasn't been synced yet, remove it immediately
            [db executeUpdate:@"DELETE FROM observations WHERE id=?", idAsObject];
        } else {
            // has been synced, need to alert the server
            [db executeUpdate:@"UPDATE observations SET state='deleted' WHERE id=?", idAsObject];
            if ([db hadError]) NSLog(@"save error: %@", [db lastErrorMessage]);
        }
        [Image deleteImagesForObservation:idAsObject];
    }
    
    [Session closeDatabase];
}

// Returns a property object for a given property key. @"AVERAGE" and @"SAMPLES" are converted to their stand and yield equivalents.
// [NSNull null] will be returned if the property is not found.
- (id)propertyObjectForKey:(NSString *)key {
    Layer *layer;
    
    if ([key isEqualToString:@"AVERAGE"]) {
        layer = [Layer layerForId:self.layerId];
        if ([layer.key isEqualToString:@"OBSERVATION_STAND_COUNT"]) key = @"STAND_COUNT_AVERAGE";
        else if ([layer.key isEqualToString:@"OBSERVATION_CORN_YIELD"]) key = @"KERNEL_COUNT_AVERAGE";
    } else if ([key isEqualToString:@"SAMPLES"]) {
        layer = [Layer layerForId:self.layerId];
        if ([layer.key isEqualToString:@"OBSERVATION_STAND_COUNT"]) key = @"STAND_COUNTS";
        else if ([layer.key isEqualToString:@"OBSERVATION_CORN_YIELD"]) key = @"KERNEL_COUNTS";
    }
    
    Property *property = [Property propertyForKey:key];
    if (! property) return nil;
    
    NSString *propertiesKey = [NSString stringWithFormat:@"%ld", (long)property.dbId];
    id prop = self.properties[propertiesKey];
    if ((NSNull *)prop == [NSNull null]) prop = nil;
    
    return prop;
}

// returns an image object given an image id or nil if not found
- (Image *)imageForImageId:(NSInteger)imageId {
    for (Image *image in self.images) {
        if (image.dbId == imageId) return image;
    }
    return nil;
}

// load images into the Observation object
- (void)loadImages {
    _images = [Image imagesForObservationId:self.dbId];
}

// creates a new image and adds it to the observation object's images
- (void)addImageWithFilename:(NSString *)filename {
    Image *image = [Image newImageForObservationId:self.dbId filename:filename];
    NSMutableArray *images = [[NSMutableArray alloc] initWithArray:_images];
    [images addObject:image];
    _images = images;
}

// returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE
- (BOOL)hasPermissionForType:(NSString *)permissionType {
    if (! permissionType || ! self.permissions) return NO;
    return [self.permissions containsString:permissionType];
}


// create and return a new observation given a report database id, layer database id, and dictionary of latitude and longitude coordinates
+ (Observation *)newObservationForReportId:(NSInteger)reportId layerId:(NSInteger)layerId coordinates:(NSDictionary *)coordinates {
    NSString *date = [NSDateFormatter nowInUTC];
    NSNumber *reportIdAsObject = [NSNumber numberWithInteger:reportId];
    NSNumber *layerIdAsObject = [NSNumber numberWithInteger:layerId];
    NSString *uuid = [NSString nameForFileType:nil];
    NSDictionary *geoJSONBoundary = [GeoJSON pointFromCoordinates:coordinates];
    NSString *boundaryAsString = [geoJSONBoundary convertJSONDictionaryToString];
    NSInteger createdBy = [[Session shared] currentUser];
    
    Observation *observation;
    
    FMDatabase *db = [Session openDatabase];
    
    [db executeUpdate:@"INSERT INTO observations (report_id, layer_id, uuid, state, created_at, created_by, boundary, permissions) VALUES (?,?,?,?,?,?,?,?)", reportIdAsObject, layerIdAsObject, uuid, @"updated", date, @(createdBy), boundaryAsString, FULL_PERMISSIONS];
    FMResultSet *results = [db executeQuery:@"SELECT * FROM observations WHERE uuid=?", uuid];
    if ([results next]) {
        observation = [[Observation alloc] initWithId:[results[@"id"] integerValue] reportId:reportId layerId:layerId date:date creator:[results[@"created_by"] integerValue] name:nil comment:nil properties:nil boundary:geoJSONBoundary permissions:results[@"permissions"]];
    }
    
    [Session closeDatabase];
    
    return observation;
}

// set record availability, returning an array of records needed
+ (NSArray *)setAvailability:(NSArray *)observations {
    NSMutableArray *records = [[NSMutableArray alloc] initWithArray:observations];
    FMDatabase *db = [Session openDatabase];
    
    // look at every record in the database, decide whether to keep it or remove it, whether to update its permissions, or whether to
    // request it from the server if it doesn't already exist or if the version has changed
    FMResultSet *results = [db executeQuery:@"SELECT * FROM observations"];
    while ([results next]) {
        BOOL found = NO;
        for (NSInteger i = 0; i < records.count; i++) {
            NSDictionary *observation = [records objectAtIndex:i];
            if (results[@"server_id"] != [NSNull null] && [results[@"server_id"] integerValue] == [observation[@"id"] integerValue]) {
                [db executeUpdate:@"UPDATE observations SET permissions=? WHERE server_id=?", observation[@"r"], results[@"server_id"]];
                if (RECORD_VERSION_FOR_OBSERVATION == [results[@"version"] integerValue]) [records removeObjectAtIndex:i];
                found = YES;
                break;
            }
        }
        if (! found) [db executeUpdate:@"UPDATE observations SET state='permission_removed' WHERE server_id=?", results[@"server_id"]];
    }
    
    [Session closeDatabase];
    
    // only return an array of ids
    NSMutableArray *neededRecords = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < records.count; i++) {
        NSDictionary *observation = [records objectAtIndex:i];
        [neededRecords addObject:observation[@"id"]];
    }
    
    return neededRecords;
}

// store and update observations, returns array of error strings or empty array
+ (NSArray *)storeObservations:(NSArray *)observations {
    FMResultSet *results;
    NSMutableArray *errors = [[NSMutableArray alloc] init];    
    FMDatabase *db = [Session openDatabase];
    
    for (NSDictionary *ob in observations) {
        FMResultSet *layer = [db executeQuery:@"SELECT * FROM layers WHERE server_id=?", ob[@"layer"]];
        if (! [layer next]) {
            [errors addObject:[NSString stringWithFormat:@"storeObservations: No layer with server id %@ for observation with server id %@", ob[@"layer"], ob[@"id"]]];
            continue;
        }
        
        FMResultSet *report = [db executeQuery:@"SELECT * FROM reports WHERE server_id=?", ob[@"parentId"]];
        if (! [report next]) {
            [errors addObject:[NSString stringWithFormat:@"storeObservations: No report with server id %@ for observation with server id %@", ob[@"parentId"], ob[@"id"]]];
            continue;
        }
        
        NSString *uuid = [ob[@"clientId"] uppercaseString];
        NSString *properties = [self stringForProperties:ob[@"props"] database:db];
        NSString *tags = [SyncExtras stringForTags:ob[@"tags"] database:db];
        NSString *boundary = [ob[@"boundary"] convertJSONDictionaryToString];
        NSNumber *creator = (ob[@"createdBy"] == [NSNull null] ? @0 : ob[@"createdBy"]);
        
        results = [db executeQuery:@"SELECT * FROM observations WHERE uuid=?", uuid];
        if ([results next]) {
            // update the existing record
            if ([results[@"modified_during_sync"] boolValue]) {
                [db executeUpdate:@"UPDATE observations SET server_id=? WHERE id=?", ob[@"id"], results[@"id"]];
            } else {
                [db executeUpdate:@"UPDATE observations SET server_id=?, uuid=?, report_id=?, state=?, layer_id=?, created_at=?, name=?, comment=?, properties=?, tags=?, boundary=?, created_by=?, permissions=?, version=? WHERE id=?", ob[@"id"], uuid, report[@"id"], @"", layer[@"id"], ob[@"date"], ob[@"name"], ob[@"comment"], properties, tags, boundary, creator, ob[@"r"], @(RECORD_VERSION_FOR_OBSERVATION), results[@"id"]];
            }
            
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeObservations: %@", [db lastErrorMessage]]];
        } else {
            // no record found, add a new one
            [db executeUpdate:@"INSERT INTO observations (server_id, uuid, report_id, layer_id, created_at, name, comment, properties, tags, boundary, created_by, permissions, version) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ob[@"id"], uuid, report[@"id"], layer[@"id"], ob[@"date"], ob[@"name"], ob[@"comment"], properties, tags, boundary, creator, ob[@"r"], @(RECORD_VERSION_FOR_OBSERVATION)];
            if ([db hadError]) {
                [errors addObject:[NSString stringWithFormat:@"storeObservations: %@", [db lastErrorMessage]]];
            }
        }
        
    }
    
    [Session closeDatabase];
    
    return errors;
}

// Returns an array of modified observations or nil if there are none. Modified observations are those with a state of deleted with no server_id or state marked updated.
+ (NSArray *)modifiedObservations {
    NSMutableArray *modifiedRecords = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM observations WHERE state='updated'"];
    while ([results next]) {
        NSMutableDictionary *record = [[NSMutableDictionary alloc] init];
        
        record[@"id"] = results[@"server_id"];
        record[@"clientId"] = results[@"uuid"];
        FMResultSet *reportResults = [db executeQuery:@"SELECT * FROM reports WHERE id=?", results[@"report_id"]];
        if ([reportResults next]) record[@"parentId"] = reportResults[@"uuid"];
        FMResultSet *layerResults = [db executeQuery:@"SELECT * FROM layers WHERE id=?", results[@"layer_id"]];
        if ([layerResults next]) record[@"layer"] = layerResults[@"server_id"];
        record[@"props"] = [self convertProperties:results[@"properties"] database:db];
        record[@"boundary"] = [NSDictionary dictionaryFromJSONString:results[@"boundary"]];
        record[@"name"] = results[@"name"];
        record[@"comment"] = results[@"comment"];
        record[@"date"] = results[@"created_at"];
        
        [modifiedRecords addObject:record];
    }
    
    [Session closeDatabase];
    
    return ([modifiedRecords count] == 0 ? nil : modifiedRecords);
}

// Returns an array of deleted observation server ids or nil if there are none. Will only return records where the state is marked 'deleted' and there is a server_id.
// Records without a server id are handled with a special upload flag.
+ (NSArray *)deletedObservations {
    NSMutableArray *deletedRecords = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM observations WHERE state='deleted'"];
    while ([results next]) {
        [deletedRecords addObject:results[@"server_id"]];
    }
    
    [Session closeDatabase];

    return ([deletedRecords count] == 0 ? nil : deletedRecords);
}

// converts a dictionary of properties containing server_id property keys to a string of properties containing database ids
+ (NSString *)stringForProperties:(NSDictionary *)properties database:(FMDatabase *)db {
    if (! properties) return nil;
    NSMutableDictionary *updatedProperties = [[NSMutableDictionary alloc] init];
    
    for (NSString *key in properties) {
        FMResultSet *results = [db executeQuery:@"SELECT * FROM properties WHERE server_id=?", key];
        if (! [results next]) return nil;
        [updatedProperties setObject:properties[key] forKey:[NSString stringWithFormat:@"%@", results[@"id"]]];
    }
    
    return [updatedProperties convertJSONDictionaryToString];
}

// converts a dictionary of properties containing database ids to a dictionary of properties containing server_ids
+ (NSDictionary *)convertProperties:(NSString *)propertiesStr database:(FMDatabase *)db {
    if ((NSNull *)propertiesStr == [NSNull null]) return (NSDictionary *)[NSNull null];
    
    NSDictionary *properties = [NSDictionary dictionaryFromJSONString:propertiesStr];
    NSMutableDictionary *updatedProperties = [[NSMutableDictionary alloc] init];
    for (NSString *key in properties) {
        FMResultSet *results = [db executeQuery:@"SELECT * FROM properties WHERE id=?", key];
        if ([results next]) {
            NSString *serverId = [NSString stringWithFormat:@"%@", results[@"server_id"]];
            [updatedProperties setObject:properties[key] forKey:serverId];
        }
    }
    
    return updatedProperties;
}

// returns an Observation object given a database query
+ (Observation *)observationWithResultSet:(FMResultSet *)results {
    NSDictionary *properties = [NSDictionary dictionaryFromJSONString:results[@"properties"]];
    NSDictionary *boundary = [NSDictionary dictionaryFromJSONString:results[@"boundary"]];
    
    return [[Observation alloc] initWithId:[results[@"id"] integerValue] reportId:[results[@"report_id"] integerValue] layerId:[results[@"layer_id"] integerValue] date:results[@"created_at"] creator:[results[@"created_by"] integerValue] name:results[@"name"] comment:results[@"comment"] properties:properties boundary:boundary permissions:results[@"permissions"]];
}

// returns an Observation object that contains only a subset of data from the query
+ (Observation *)observationWithResultSubset:(FMResultSet *)results {
    return [[Observation alloc] initWithId:[results[@"id"] integerValue] reportId:[results[@"report_id"] integerValue] layerId:[results[@"layer_id"] integerValue] date:results[@"created_at"] name:results[@"name"] permissions:results[@"permissions"]];
}

// delete observations
+ (void)deleteObservations:(NSArray *)observations {
    FMResultSet *results;
    FMDatabase *db = [Session openDatabase];
    
    for (NSNumber *observationId in observations) {
        results = [db executeQuery:@"SELECT * FROM observations WHERE server_id=?", observationId];
        if ([results next]) {
            [Image deleteImagesForObservation:results[@"id"]];
            [db executeUpdate:@"DELETE FROM observations WHERE id=?", results[@"id"]];
        }
    }
    
    results = [db executeQuery:@"SELECT * FROM observations WHERE state='permission_removed'"];
    while ([results next]) {
        [Image deleteImagesForObservation:results[@"id"]];
        [db executeUpdate:@"DELETE FROM observations WHERE id=?", results[@"id"]];
    }

    [Session closeDatabase];
}

// deletes all observations with a given report id
+ (void)deleteObservationsForReport:(NSNumber *)reportId {
    FMDatabase *db = [Session openDatabase];
    FMResultSet *results = [db executeQuery:@"SELECT * FROM observations WHERE report_id=?", reportId];
    while ([results next]) {
        [Image deleteImagesForObservation:results[@"id"]];
        [db executeUpdate:@"DELETE FROM observations WHERE id=?", results[@"id"]];
    }
    [Session closeDatabase];
}

// clear the modified_during_state flags on all records
+ (void)clearModifiedDuringSync {
    NSNumber *trueAsObject = [NSNumber numberWithBool:YES];
    NSNumber *falseAsObject = [NSNumber numberWithBool:NO];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM observations WHERE modified_during_sync=?", trueAsObject];
    while ([results next]) {
        [db executeUpdate:@"UPDATE observations SET modified_during_sync=? WHERE id=?", falseAsObject, results[@"id"]];
    }
    
    [Session closeDatabase];
}

// returns an array of observations that are not associated with locked reports
+ (NSArray <Observation *> *)allObservations {
    NSMutableArray *observations = [[NSMutableArray alloc] init];
    Observation *observation;
    NSNumber *no = @NO;
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM observations WHERE state != 'deleted' ORDER BY created_at DESC"];
    while ([results next]) {
        FMResultSet *reportResults = [db executeQuery:@"SELECT * FROM reports WHERE id=? AND locked=?", results[@"report_id"], no];
        if ([reportResults next]) {
            observation = [self observationWithResultSubset:results];
            [observations addObject:observation];
        }
    }
    
    [Session closeDatabase];
    
    return observations;
}

// returns an Observation object given a database id
+ (Observation *)observationForId:(NSInteger)observationId {
    NSNumber *observationIdAsObject = [NSNumber numberWithInteger:observationId];
    Observation *observation;
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM observations WHERE id=?", observationIdAsObject];
    if ([results next]) {
        observation = [self observationWithResultSet:results];
    }
    
    [Session closeDatabase];
    
    return observation;
}

// returns the observations for a given report id
+ (NSArray <Observation *> *)observationsForReportId:(NSInteger)reportId lockedOnly:(BOOL)lockedOnly {
    NSMutableArray *observations = [[NSMutableArray alloc] init];
    NSNumber *reportIdAsObject = [NSNumber numberWithInteger:reportId];
    NSNumber *trueAsObject = [NSNumber numberWithBool:YES];
    Observation *observation;
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results;
    if (lockedOnly) {
        results = [db executeQuery:@"SELECT * FROM observations WHERE report_id=? AND state != 'deleted' AND locked = ? ORDER BY created_at DESC", reportIdAsObject, trueAsObject];
    } else {
        results = [db executeQuery:@"SELECT * FROM observations WHERE report_id=? AND state != 'deleted' ORDER BY created_at DESC", reportIdAsObject];
    }

    while ([results next]) {
        observation = [self observationWithResultSet:results];
        [observations addObject:observation];
    }
    
    [Session closeDatabase];
    
    return observations;
}

@end
