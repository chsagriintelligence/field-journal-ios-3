//
//  Sync.m
//  field-journal-ios-3
//
//  Created by Elia Freedman on 1/18/17.
//  Copyright © 2017 CHS, Inc. All rights reserved.
//

#import "Sync.h"
#import "Crop.h"
#import "CropStage.h"
#import "FieldStatus.h"
#import "Layer.h"
#import "Property.h"
#import "Tag.h"
#import "FieldImageCategory.h"
#import "User.h"
#import "Coop.h"
#import "Grower.h"
#import "Farm.h"
#import "Field.h"
#import "Report.h"
#import "Observation.h"
#import "Image.h"


#define SYNC_TEST_URL           @"https://fieldjournal-test.yieldpoint.ag/api"
#define SYNC_STAGING_URL        @"https://fieldjournal-stage.yieldpoint.ag/api"
#define SYNC_PRODUCTION_URL     @"https://fieldjournal.yieldpoint.ag/api"
#define SYNC_APP                @"apps/fieldjournal"
#define SYNC_OBS_IMAGE_URL      @"getObservationImageUrl"
#define SYNC_FIELD_IMAGE_URL    @"getFieldImageUrl"

#define CONCURRENT_FILE_TRANSFERS       10      // number of up/downloads to perform in parallel
#define CURRENT_SYNC_VERSION_NUMBER     2       // must match the version returned from the server otherwise stops syncing
#define MAX_DOWNLOAD_ATTEMPTS           3       // total tries to download before giving up and logging the error


// HTTP status codes; undefined by iOS suprisingly
enum {
    HTTP_OK = 200, HTTP_UNAUTHORIZED = 401, HTTP_SERVICE_UNAVAILABLE = 503
} httpCode;


@interface Sync ()
    
@property (nonatomic) BOOL canSynchronize;                  // true if can synchronize, false otherwise
@property (nonatomic) NSMutableArray *syncErrors;           // array of error strings that occurred during sync
@property (nonatomic) NSString *errorMessage;               // error text displayed if any transfers failed
@property (nonatomic) NSRecursiveLock *imageQueueLock;      // coordinate multithreaded image queue access
@property (nonatomic) NSMutableArray *imageQueue;           // array of dictionaries describing images to be transferred
@property (nonatomic) NSInteger imageResponsesReceived;     // number of image transfers completed so far
@property (nonatomic) NSInteger numberOfImages;             // total number of images to be transferred

@end


@implementation Sync

- (id)init {
    self = [super init];
    if (! self) return nil;
    
    return self;
}

+ (Sync *)shared {
    static Sync *shared = nil;
    if (! shared) shared = [[super allocWithZone:nil] init];
    return shared;
}

+ (NSString *)syncUrl {
    NSString *endpoint = [[Session shared] domain];
    if ([endpoint isEqualToString:SESSION_DOMAIN_STAGING]) return SYNC_STAGING_URL;
    if ([endpoint isEqualToString:SESSION_DOMAIN_TESTING]) return SYNC_TEST_URL;
    return SYNC_PRODUCTION_URL;
}


#pragma mark - Internet Connection
    
// constantly monitors the Internet connection, updating canSynchronize as status changes. This monitors
// all connections, wifi or cellular.
- (void)monitorInternetConnection {
    Reachability* reach = [Reachability reachabilityForInternetConnection];
    self.canSynchronize = [reach isReachable];
    reach.reachableBlock = ^(Reachability* reach) { self.canSynchronize = [reach isReachable]; };
    reach.unreachableBlock = ^(Reachability* reach) { self.canSynchronize = [reach isReachable]; };
    [reach startNotifier];
}
    
// returns YES if sync url is reachable with any Internet connection
+ (BOOL)syncUrlIsReachable {
    Reachability *reachability = [Reachability reachabilityWithHostname:[Sync syncUrl]];
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    if (netStatus == NotReachable) return NO;
    return YES;
}
    
// returns YES if the internet connection is wifi
+ (BOOL)isWifiConnection {
    Reachability* reach = [Reachability reachabilityForLocalWiFi];
    return [reach isReachableViaWiFi];
}
    
    
#pragma mark - API
    
- (void)login:(NSString*)email withPassword:(NSString*)password {
    if (! [Sync syncUrlIsReachable])
    [self.authenticateDelegate authenticateFailed:Localize(@"sync.failed.websiteUnavailable")];
    else if (! [[Sync shared] canSynchronize]) {
        [self.authenticateDelegate authenticateFailed:Localize(@"sync.failed.noInternetConnection")];
    } else if ((! email || email.length == 0) && (! password || password.length == 0)) {
        [self.authenticateDelegate authenticateFailed:Localize(@"sync.failed.missingEmailAndPassword")];
    } if (! email || email.length == 0) {
        [self.authenticateDelegate authenticateFailed:Localize(@"sync.failed.missingEmail")];
    } else if (! password || password.length == 0) {
        [self.authenticateDelegate authenticateFailed:Localize(@"sync.failed.missingPassword")];
    } else {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[@"userName"] = email;
        params[@"password"] = password;
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/requestToken", [Sync syncUrl]]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        request.HTTPBody = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
        request.HTTPMethod = @"POST";
        
        NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSInteger status;
            NSString *token;
            NSString *message = [self requestFailed:data response:response error:error status:&status];
            if (! message) {
                NSError *jsonError = nil;
                NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                if (! results) {
                    message = [NSString stringWithFormat:@"Invalid JSON result: %@", jsonError];
                } else {
                    token = results[@"token"];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (message) {
                    [self.authenticateDelegate authenticateFailed:message];
                } else {
                    [self.authenticateDelegate authenticateCompletedWithToken:token];
                }
            });
        }];
        
        [task resume];
    }
}
    
- (void)syncDatabase {
    Session *session = [Session shared];
    session.syncInProgress += 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating System State"];
    NSString *message = nil;
    
    // wrapped in delay just so can see the animations occur correctly
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(ANIMATION_LENGTH_LONG * NSEC_PER_SEC));
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
//        [self.syncDatabaseDelegate syncDatabaseCompleted:message];
//    });
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.HTTPMaximumConnectionsPerHost = 1;    // only allow sync tasks to execute single-file, not concurrently
    NSURLSession *urlsess = [NSURLSession sessionWithConfiguration:config];
    
    NSString *migration = @"";
    NSInteger migrationVersion = [session serverMigrationVersion];
    if (migrationVersion != 0) migration = [NSString stringWithFormat:@"?migration=%ld", (long)migrationVersion];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/serverState/2%@", [Sync syncUrl], SYNC_APP, migration]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[session authToken] forHTTPHeaderField:@"x-access-token"];
    request.HTTPMethod = @"GET";
    
    NSURLSessionDataTask *task = [urlsess dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSInteger status;
        NSString *message = [self requestFailed:data response:response error:error status:&status];
        
        if (! message) {
            NSError *jsonError = nil;
            NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (! results) {
                message = [NSString stringWithFormat:@"Invalid JSON result: %@", jsonError];
            } else if ([results[@"version"] integerValue] != CURRENT_SYNC_VERSION_NUMBER) {
                message = @"The iOS version of Field Journal is not compatible with the web version. Please upgrade before synchronizing.";
            } else {
                NSDictionary *records;
                NSArray *errors;
                Session *session = [Session shared];
                
                if (results[@"migrations"]) {
//                    NSInteger serverMigration = [session serverMigrationVersion];
//                    if (serverMigration == 1) [Migration serverMigration1:results[@"migrations"]];
//                    [session setServerMigrationVersion:0];      // reset the server migration version after all server migrations have finished
                }
                
                if (results[@"system"]) {
                    records = results[@"system"];
                    if (records[@"crops"]) errors = [Crop storeCrops:records[@"crops"]];
                    if (errors) [self.syncErrors addObjectsFromArray:errors];
                    
                    if (records[@"cropStages"]) errors = [CropStage storeCropStages:records[@"cropStages"]];
                    if (errors) [self.syncErrors addObjectsFromArray:errors];
                    
                    if (records[@"fieldStatuses"]) errors = [FieldStatus storeFieldStatuses:records[@"fieldStatuses"]];
                    if (errors) [self.syncErrors addObjectsFromArray:errors];
                    
                    if (records[@"layers"]) errors = [Layer storeLayers:records[@"layers"]];
                    if (errors) [self.syncErrors addObjectsFromArray:errors];
                    
                    if (records[@"properties"]) errors = [Property storeProperties:records[@"properties"]];
                    if (errors) [self.syncErrors addObjectsFromArray:errors];
                    
                    if (records[@"tags"]) errors = [Tag storeTags:records[@"tags"]];
                    if (errors) [self.syncErrors addObjectsFromArray:errors];
                    
                    if (records[@"fieldImageCategories"]) errors = [FieldImageCategory storeCategories:records[@"fieldImageCategories"]];
                    if (errors) [self.syncErrors addObjectsFromArray:errors];
                }
                
                NSMutableDictionary *permissions = [[NSMutableDictionary alloc] init];
                NSMutableDictionary *requiredRecords = [[NSMutableDictionary alloc] init];
                NSArray *required;
                
                if (results[@"users"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating User State"];
                    permissions[RECORD_TYPE_USERS] = results[@"users"][@"r"];
                    required = [User setAvailability:results[@"users"][@"items"]];
                    if (required.count != 0) requiredRecords[@"users"] = required;
                }
                
                if (results[@"coops"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Co-op State"];
                    permissions[RECORD_TYPE_COOPS] = results[@"coops"][@"r"];
                    required = [Coop setAvailability:results[@"coops"][@"items"]];
                    if (required.count != 0) requiredRecords[@"coops"] = required;
                }
                
                if (results[@"growers"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Grower State"];
                    permissions[RECORD_TYPE_GROWERS] = results[@"growers"][@"r"];
                    required = [Grower setAvailability:results[@"growers"][@"items"]];
                    if (required.count != 0) requiredRecords[@"growers"] = required;
                }
                
                if (results[@"farms"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Farm State"];
                    permissions[RECORD_TYPE_FARMS] = results[@"farms"][@"r"];
                    required = [Farm setAvailability:results[@"farms"][@"items"]];
                    if (required.count != 0) requiredRecords[@"farms"] = required;
                }
                
                if (results[@"fields"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Field State"];
                    permissions[RECORD_TYPE_FIELDS] = results[@"fields"][@"r"];
                    required = [Field setAvailability:results[@"fields"][@"items"]];
                    if (required.count != 0) requiredRecords[@"fields"] = required;
                }
                
                if (results[@"reports"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Report State"];
                    permissions[RECORD_TYPE_REPORTS] = results[@"reports"][@"r"];
                    permissions[RECORD_TYPE_REPORT_PUBLISH_TO_GROWER] = ([@"PUB_TO_GROWERS" containedWithinArray:results[@"reports"][@"actions"]] != NSNotFound ? PERMISSION_CREATE : PERMISSION_NONE);
                    permissions[RECORD_TYPE_REPORT_PUBLISH_TO_INTEL] = ([@"PUB_TO_INTEL" containedWithinArray:results[@"reports"][@"actions"]] != NSNotFound ? PERMISSION_CREATE : PERMISSION_NONE);
                    required = [Report setAvailability:results[@"reports"][@"items"]];
                    if (required.count != 0) requiredRecords[@"reports"] = required;
                }
                
                if (results[@"observations"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Observation State"];
                    permissions[RECORD_TYPE_OBSERVATIONS] = results[@"observations"][@"r"];
                    required = [Observation setAvailability:results[@"observations"][@"items"]];
                    if (required.count != 0) requiredRecords[@"observations"] = required;
                }
                
                if (results[@"observationImages"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Image State"];
                    permissions[RECORD_TYPE_IMAGES] = results[@"observationImages"][@"r"];
                    required = [Image setAvailability:results[@"observationImages"][@"items"]];
                    if (required.count != 0) requiredRecords[@"observationImages"] = required;
                }
                
                if (results[@"fieldImages"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Field Image State"];
                    permissions[RECORD_TYPE_FIELD_IMAGES] = results[@"fieldImages"][@"r"];
                    required = [FieldImage setAvailability:results[@"fieldImages"][@"items"]];
                    if (required.count != 0) requiredRecords[@"fieldImages"] = required;
                }
                
                [session setUserPermissions:permissions];
                
                session.syncInProgress -= 1;
                if ([self.syncErrors count] == 0) {
                    [self requestRecords:(requiredRecords.count == 0 ? nil : requiredRecords)];
                } else {
                    message = @"Error during sync. All data could not be imported. Please view log to contact support or select OK and attempt to sync again later.";
                }
            }
        }
        
        if (message) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self syncCompleted:message];
            });
        }
    }];
    
    [task resume];
}

- (void)requestRecords:(NSDictionary *)requiredRecords {
    Session *session = [Session shared];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Records"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (requiredRecords) params[@"required"] = requiredRecords;
    NSString *syncDate = [session syncedAt];
    if ([syncDate length] > 0) params[@"syncDate"] = syncDate;
/*
    // deleted records to be uploaded
    NSMutableDictionary *deletedRecords = [[NSMutableDictionary alloc] init];
    NSArray *uploadRecords = [Report deletedReports];
    if (uploadRecords) deletedRecords[@"reports"] = uploadRecords;
    uploadRecords = [Observation deletedObservations];
    if (uploadRecords) deletedRecords[@"observations"] = uploadRecords;
    uploadRecords = [Image deletedImages];
    if (uploadRecords) deletedRecords[@"observationImages"] = uploadRecords;
    uploadRecords = [Grower deletedGrowers];
    if (uploadRecords) deletedRecords[@"growers"] = uploadRecords;
    uploadRecords = [Farm deletedFarms];
    if (uploadRecords) deletedRecords[@"farms"] = uploadRecords;
    uploadRecords = [Field deletedFields];
    if (uploadRecords) deletedRecords[@"fields"] = uploadRecords;
    if ([deletedRecords count] > 0) params[@"deleted"] = deletedRecords;
    
    // modified records to be uploaded, these records could be added, changed or new records that are deleted
    NSMutableDictionary *modifiedRecords = [[NSMutableDictionary alloc] init];
    uploadRecords = [Report modifiedReports];
    if (uploadRecords) modifiedRecords[@"reports"] = uploadRecords;
    uploadRecords = [Observation modifiedObservations];
    if (uploadRecords) modifiedRecords[@"observations"] = uploadRecords;
    uploadRecords = [Image modifiedImages];
    if (uploadRecords) modifiedRecords[@"observationImages"] = uploadRecords;
    uploadRecords = [Grower modifiedGrowers];
    if (uploadRecords) modifiedRecords[@"growers"] = uploadRecords;
    if ([Grower hasIncompleteGrowers]) [self.syncErrors addObject:@"Sync succeeded but one or more growers have neither a company name nor a personal name. That grower or growers need a name in order for them to be uploaded."];
    uploadRecords = [Farm modifiedFarms];
    if (uploadRecords) modifiedRecords[@"farms"] = uploadRecords;
    if ([Farm hasIncompleteFarms]) [self.syncErrors addObject:@"Sync succeeded but one or more farms do not have a name. That farm or farms need a name in order for them to be uploaded."];
    uploadRecords = [Field modifiedFields];
    if (uploadRecords) modifiedRecords[@"fields"] = uploadRecords;
    if ([Field hasIncompleteFields]) [self.syncErrors addObject:@"Sync succeeded but one or more fields do not have a name. That field or fields need a name in order for them to be uploaded."];
    if ([modifiedRecords count] > 0) params[@"updated"] = modifiedRecords;
*/
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.HTTPMaximumConnectionsPerHost = 1;    // only allow sync tasks to execute single-file, not concurrently
    NSURLSession *urlsess = [NSURLSession sessionWithConfiguration:config];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/synchronizeDatabase2/2", [Sync syncUrl], SYNC_APP]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[session authToken] forHTTPHeaderField:@"x-access-token"];
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    request.HTTPMethod = @"POST";
    
    NSURLSessionDataTask *task = [urlsess dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSInteger status;
        NSString *message = [self requestFailed:data response:response error:error status:&status];
        
        if (! message) {
            NSError *jsonError = nil;
            NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (! results) {
                message = [NSString stringWithFormat:@"Invalid JSON result: %@", jsonError];
            } else {
                NSDictionary *records;
                NSArray *deletedImages;
                NSArray *errors;
                
                if (results[@"errors"]) {
                    for (id key in results[@"errors"]) {
                        NSArray *errorsForType = [results[@"errors"] objectForKey:key];
                        for (NSDictionary *errorForType in errorsForType) {
                            [self.syncErrors addObject:errorForType[@"error"]];
                        }
                    }
                }
                
                if (results[@"updated"]) {
                    records = results[@"updated"];
                    if (records[@"users"]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Users"];
                        errors = [User storeUsers:records[@"users"]];
                        if (errors) [self.syncErrors addObjectsFromArray:errors];
                    }
                    
                    if (records[@"coops"]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Co-ops"];
                        errors = [Coop storeCoops:records[@"coops"]];
                        if (errors) [self.syncErrors addObjectsFromArray:errors];
                    }
                    
                    if (records[@"growers"]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Growers"];
                        errors = [Grower storeGrowers:records[@"growers"]];
                        if (errors) [self.syncErrors addObjectsFromArray:errors];
                    }
                    
                    if (records[@"farms"]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Farms"];
                        errors = [Farm storeFarms:records[@"farms"]];
                        if (errors) [self.syncErrors addObjectsFromArray:errors];
                        
                        // have to store both farms and growers before updating the relationships
                        errors = [Farm storeGrowersForFarms:records[@"farms"]];
                        if (errors) [self.syncErrors addObjectsFromArray:errors];
                    }
                    
                    if (records[@"growers"]) {
                        // have to store both farms and growers before updating the relationships
                        errors = [Grower storeFarmsForGrowers:records[@"growers"]];
                        if (errors) [self.syncErrors addObjectsFromArray:errors];
                    }
                    
                    if (records[@"fields"]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Fields"];
                        errors = [Field storeFields:records[@"fields"]];
                        if (errors) [self.syncErrors addObjectsFromArray:errors];
                    }
                    
                    if (records[@"reports"]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Reports"];
                        errors = [Report storeReports:records[@"reports"]];
                        if (errors) [self.syncErrors addObjectsFromArray:errors];
                    }
                    
                    if (records[@"observations"]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Observations"];
                        errors = [Observation storeObservations:records[@"observations"]];
                        if (errors) [self.syncErrors addObjectsFromArray:errors];
                    }
                    
                    if (records[@"observationImages"]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Images"];
                        errors = [Image storeImages:records[@"observationImages"]];
                        if (errors) [self.syncErrors addObjectsFromArray:errors];
                    }
                    
                    if (records[@"fieldImages"]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Updating Field Images"];
                        errors = [FieldImage storeFieldImages:records[@"fieldImages"]];
                        if (errors) [self.syncErrors addObjectsFromArray:errors];
                    }
                }
                
                if (results[@"deleted"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Finalizing Records"];
                    records = results[@"deleted"];
                    [User deleteUsers:records[@"users"]];
                    [Coop deleteCoops:records[@"coops"]];
                    [Grower deleteGrowers:records[@"growers"]];
                    [Farm deleteFarms:records[@"farms"]];
                    [Field deleteFields:records[@"fields"]];
                    [Report deleteReports:records[@"reports"]];
                    [Observation deleteObservations:records[@"observations"]];
                    [Image deleteImages:records[@"observationImages"]];
                    [FieldImage deleteFieldImages:records[@"fieldImages"]];
                }
                
                [Grower clearModifiedDuringSync];
                [Farm clearModifiedDuringSync];
                [Field clearModifiedDuringSync];
                [Report clearModifiedDuringSync];
                [Observation clearModifiedDuringSync];
                [Image clearModifiedDuringSync];
                
                session.syncInProgress -= 1;
                if ([self.syncErrors count] == 0) session.syncedAt = results[@"syncDate"];
//                [self uploadImages:deletedImages];
                [self downloadImages];  // TODO: going straight to downloading for now, no UI to modify recs yet
            }
        }
        
        if (message) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self syncCompleted:message];
            });
        }
    }];
    
    [task resume];
}

// determine images to download and start the first download background task
- (void)downloadImages {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.syncDatabaseDelegate syncBeginDownloadingImage];
    });
    
    if (! self.imageQueueLock) self.imageQueueLock = [[NSRecursiveLock alloc] init];
    [self.imageQueueLock lock];
    //NSLog(@"Starting downloads");
    
    self.imageQueue = [Image imagesToDownload];
    [self.imageQueue addObjectsFromArray:[FieldImageCategory legendsToDownload]];
    [self.imageQueue addObjectsFromArray:[FieldImage fieldImagesToDownload]];
    self.imageResponsesReceived = 0;
    self.errorMessage = @"Error during sync. All images could not be downloaded. Please view log to contact support or select OK and attempt to sync again later.";
    
    self.numberOfImages = self.imageQueue.count;
    if (self.numberOfImages == 0) {
        //NSLog(@"No downloads needed");
        if ([self.syncErrors count] == 0) self.errorMessage = nil;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(ROTATION_DELAY * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self syncCompleted:self.errorMessage];
        });
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:@"Downloading Images"];
        
        for (int i = 0; i < CONCURRENT_FILE_TRANSFERS; ++i) [self downloadNextImage];
    }
    
    [self.imageQueueLock unlock];
}

// downloads the first file in the list of pending downloads
// download queue should already be locked before calling this
- (void)downloadNextImage {
    if (! self.imageQueue || self.imageQueue.count == 0) return;
    NSDictionary *image = self.imageQueue[0];
    [self.imageQueue removeObjectAtIndex:0];
    //NSLog(@"Requesting URL for id %@", image[@"server_id"]);
    
    // if this is a field image category legend, server already sent the whole url instead of an id, skip directly to using it
    if ([image[@"imageType"] isEqualToString:@"legend"]) {
        [self downloadImageData:[NSURL URLWithString:image[@"server_id"]] image:image];
        return;
    }
    
    // build a url to request the final image url for this server id
    BOOL fieldImage = [image[@"imageType"] isEqualToString:@"field"];
    NSURL *getImageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@", [Sync syncUrl], SYNC_APP, fieldImage ? SYNC_FIELD_IMAGE_URL : SYNC_OBS_IMAGE_URL, image[@"server_id"]]];
    NSMutableURLRequest *getImageRequest = [NSMutableURLRequest requestWithURL:getImageUrl];
    [getImageRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [getImageRequest setValue:[[Session shared] authToken] forHTTPHeaderField:@"x-access-token"];
    
    // start a background task to fetch the url for this image
    NSURLSessionDataTask *getImageUrlTask = [[NSURLSession sharedSession] dataTaskWithRequest:getImageRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSInteger status;
        NSString *message = [self requestFailed:data response:response error:error status:&status];
        if (message) {
            [self downloadCompleted:[NSString stringWithFormat:@"Cannot retrieve url for image with server id %@: %@", image[@"server_id"], message]];
        } else {
            NSError *jsonError = nil;
            NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (! results) {
                [self downloadCompleted:[NSString stringWithFormat:@"Invalid JSON result for image server id %@: %@", image[@"server_id"], jsonError]];
            } else {
                // got back a correct url, now download the image in another background task
                if (results[@"url"] && results[@"url"] != [NSNull null]) {
                    [self downloadImageData:[NSURL URLWithString:results[@"url"]] image:image];
                } else {
                    [self downloadCompleted:[NSString stringWithFormat:@"Cannot download image with server id %@: No url for image", image[@"server_id"]]];
                }
            }
        }
    }];
    
    [getImageUrlTask resume];
}

// start a background task to download the actual image file contents
- (void)downloadImageData:(NSURL *)url image:(NSDictionary *)image {
    //NSLog(@"Id %@ = %@, downloading", response.URL.path.lastPathComponent, url.path.lastPathComponent);
    NSURLSessionDownloadTask *downloadImageTask = [[NSURLSession sharedSession] downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *)response;
        NSString *errorMsg = nil;
        if (httpResp.statusCode != HTTP_OK) {
            // if error 503 then requeue at end for up to max total tries
            NSInteger triesRemaining = (image[@"triesRemaining"] ? [image[@"triesRemaining"] integerValue] : MAX_DOWNLOAD_ATTEMPTS) - 1;
            if (httpResp.statusCode == HTTP_SERVICE_UNAVAILABLE && triesRemaining > 0) {
                //NSLog(@"Attempt %d of %d for %@", MAX_DOWNLOAD_ATTEMPTS-triesRemaining+1, MAX_DOWNLOAD_ATTEMPTS, image[@"filename"]);
                // requeue another copy of the image entry with decremented tries counter
                NSMutableDictionary *retry = image.mutableCopy;
                retry[@"triesRemaining"] = @(triesRemaining);
                [self.imageQueueLock lock];
                [self.imageQueue addObject:retry];
                // preemptively undo the response counter increment that's about to happen
                --self.imageResponsesReceived;
                [self.imageQueueLock unlock];
            } else {
                NSString *message = [NSString stringWithFormat:@"HTTP request failed, HTML status = %ld, headers = %@, %@ code %ld: %@", (long)httpResp.statusCode, httpResp.allHeaderFields, error.domain, (long)error.code, error.localizedDescription];
                errorMsg = [NSString stringWithFormat:@"Cannot download image with server id %@: %@", image[@"server_id"], message];
            }
        } else {
            if ([image[@"imageType"] isEqualToString:@"observation"]) {
                UIImage *downloadedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:location]];
                [downloadedImage storeScaledPNGImage:image[@"filename"]];
            } else {
                [UIImage storeImage:image[@"filename"] withData:[NSData dataWithContentsOfURL:location]];
            }
            //NSLog(@"Completed %@", response.URL.path.lastPathComponent);
        }
        
        NSString *str = [NSString stringWithFormat:@"Downloaded Image %d of %d", (int)self.imageResponsesReceived+1, (int)self.numberOfImages];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SYNC_IN_PROGRESS object:str];
        
        [self downloadCompleted:errorMsg];
    }];
    
    [downloadImageTask resume];
}

// removes entry for completed image from pending downloads list and begins next download if any, or calls syncCompleted if done
// "completed" doesn't necessarily mean it downloaded successfully, it just means we're done with it and moving on to the next
- (void)downloadCompleted:(NSString *)errorMsg {
    [self.imageQueueLock lock];
    
    if (errorMsg) {
        [self.syncErrors addObject:errorMsg];
        //NSLog(@"Download error: %@", errorMsg);
    }
    
    self.imageResponsesReceived += 1;
    if (self.imageResponsesReceived == self.numberOfImages) {
        self.imageQueue = nil;
        if ([self.syncErrors count] == 0) self.errorMessage = nil;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self syncCompleted:self.errorMessage];
        });
        //NSLog(@"Downloads complete");
    } else {
        [self downloadNextImage];
    }
    
    [self.imageQueueLock unlock];
}

//- (void)syncBeginDownloadingImage {
//    if (self.syncShield) {
//        [self.syncShield removeFromSuperview];
//        self.syncShield = nil;
//    }
//}

- (void)syncCompleted:(NSString *)message {
    Session *session = [Session shared];
    
    [session setSyncErrors:self.syncErrors];
    
/*    Tab currentTab = [session currentTab];
    if (currentTab == TabReport) [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_REPORTS object:nil];
    else if (currentTab == TabReview) [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_OBSERVATIONS object:nil];
    else if (currentTab == TabRecord) [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_LAYERS object:nil];
    else if (currentTab == TabGrower) [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_GROWERS object:nil];
    else if (currentTab == TabFarm) [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_FARMS object:nil];
    else if (currentTab == TabField) [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_FIELDS object:nil];
*/
    // many changes can trigger a change to the visible record. It could be deleted, a parent could be deleted, a report could be visible but its
    // farm may have changed its name, etc. Always update the visible record after a sync to ensure all changes are visible. If the visible
    // record hadn't changed, it will just reload the same data.
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_VISIBLE_RECORD_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_REPORTS_BADGE object:nil];
    
    [self.syncDatabaseDelegate syncDatabaseCompleted:message];
}

// Returns an error message if a server HTTP request communications error was detected or nil.
// Returns false and the received status code when request worked, even if returned result was an error (e.g. bad password)
- (NSString *)requestFailed:(NSData *)data response:(NSURLResponse *)response error:(NSError *)error status:(NSInteger *)status {
    NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *)response;
    *status = httpResp.statusCode;
    
    if (! data) {
        // iOS coding error, request didn't even execute
        if (! error) return Localize(@"sync.failed.unknown");
        return [NSString stringWithFormat:@"HTTP request failed, %@ code %ld: %@", error.domain, (long)error.code, error.localizedDescription];
    } else if (httpResp.statusCode == HTTP_UNAUTHORIZED) {
        // unauthorized access, incorrect email or password
        return Localize(@"sync.failed.unknownEmailOrPassword");
    } else if (httpResp.statusCode != HTTP_OK) {
        // Unexpected HTTP result code, some failure other than our intentional error results
        NSError *jsonError = nil;
        NSString *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if (jsonError) json = @"";
        return [NSString stringWithFormat:@"HTTP request failed, HTML status = %ld, headers = %@, %@ code %ld: %@ %@", (long)httpResp.statusCode, httpResp.allHeaderFields, error.domain, (long)error.code, error.localizedDescription, json];
    }
    
    return nil;
}
    

@end
