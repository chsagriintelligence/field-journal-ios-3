//
//  UIColor+Color.m
//  Field Journal
//
//  Created by Elia Freedman on 2/16/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "UIColor+Color.h"

@implementation UIColor (Color)

+ (UIColor *)primaryColor {
    return [UIColor colorFromHexString:@"0x007AFF"];
}

+ (UIColor *)secondaryColor {
    return [UIColor colorFromHexString:@"0xFFFFFF"];
}

+ (UIColor *)selectedGrey {
    return [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0];
}

+ (UIColor *)warningColor {
    return [UIColor colorWithRed:227.0/255.0 green:225.0/255.0 blue:95.0/255.0 alpha:1.0];
}

+ (UIColor *)alertColor {
    return [UIColor colorWithRed:196.0/255.0 green:22.0/255.0 blue:41.0/255.0 alpha:1.0];
}

+ (UIColor *)backgroundColor {
    return [UIColor colorWithRed:231.0/255.0 green:225.0/255.0 blue:214.0/255.0 alpha:1.0];
}

// returns a UIColor in exchange for a hex string. The hex string can have a leading 0x, # or include
// no leading character.
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned result = 0;
    
    if ([hexString characterAtIndex:0] == '#') {
        hexString = [NSString stringWithFormat:@"0x%@", [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""]];
    } else if ([hexString characterAtIndex:1] != 'x') {
        hexString = [NSString stringWithFormat:@"0x%@", hexString];
    }
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner scanHexInt:&result];
    
    return UIColorFromHex(result);
}

@end
