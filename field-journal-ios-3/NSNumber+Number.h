//
//  NSNumber+Number.h
//  Field Journal
//
//  Created by Elia Freedman on 2/3/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Number)

// returns an NSNumber representing a float, removes the comma thousands separators
+ (NSNumber *)numberFromFloatString:(NSString *)str;

// returns a string representation of a number with the designated decimalPlaces
- (NSString *)numberWithPlaces:(NSUInteger)decimalPlaces;

- (NSInteger)containedWithinArray:(NSArray *)array;             // returns the index of the matching array element or NSNotFound

@end
