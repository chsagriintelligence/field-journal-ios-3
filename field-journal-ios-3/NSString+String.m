//
//  NSString+String.m
//  Field Journal
//
//  Created by Elia Freedman on 2/3/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "NSString+String.h"
#import "NSNumberFormatter+NumberFormat.h"
#import "NSNumber+Number.h"

@implementation NSString (String)

// returns the float value of an NSString, removes comma thousands separators
- (CGFloat)floatVal {
    NSString *str = [self stringByReplacingOccurrencesOfString:@"," withString:@""];
    return [str floatValue];
}

// remove comma thousands separator from NSStrings
- (NSString *)unformatNumberString {
    return [self stringByReplacingOccurrencesOfString:@"," withString:@""];
}

- (NSString *)formatIntegerString {
    NSNumberFormatter *format = [NSNumberFormatter decimalNumberFormat:0];
    return [format stringFromNumber:[NSNumber numberFromFloatString:self]];
}

// Returns a unique filename. If extension is provided then ".<extension>" added to the end of the file name. The period
// separator is added automatically if the extension is provided.
+ (NSString *)nameForFileType:(NSString *)extension {
    CFUUIDRef uniqueId = CFUUIDCreate(kCFAllocatorDefault);
    CFStringRef uidStr = CFUUIDCreateString(kCFAllocatorDefault, uniqueId);
    NSString *key = (__bridge NSString *)uidStr;
    CFRelease(uniqueId);
    CFRelease(uidStr);
    
    if (! extension) return [NSString stringWithFormat:@"%@", key];
    return [NSString stringWithFormat:@"%@.%@", key, extension];
}

// returns the index of the matching array element or NSNotFound
- (NSInteger)containedWithinArray:(NSArray *)array {
    if (! array) return NSNotFound;
    
    for (NSInteger i = 0; i < array.count; i++) {
        if ([self isEqualToString:array[i]]) return i;
    }
    return NSNotFound;
}

// return a string consisting of dictionary data. If an error then returns nil.
+ (NSString *)stringWithDictionary:(NSDictionary *)data {
    NSString *jsonString;
    
    if (data) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
        if (! jsonData) {
            NSLog(@"json data error: %@", error);
            jsonString = nil;
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
    }
    
    return jsonString;
}

// converts a string to a dictionary. If an error then returns nil.
- (NSDictionary *)dictionaryValue {
    NSError *error;
    NSData *dataObj = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:dataObj options:0 error:&error];
    
    if (! data) {
        NSLog(@"json conversion error: %@", error);
        return nil;
    }
    
    return data;
}

// return a string consisting of array data. If an error then returns nil.
+ (NSString *)stringWithArray:(NSArray *)data {
    NSString *jsonString;
    
    if (data) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
        if (! jsonData) {
            NSLog(@"json data error: %@", error);
            jsonString = nil;
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
    }
    
    return jsonString;
}

// converts a string to an array. If an error then returns nil.
- (NSArray *)arrayValue {
    NSError *error;
    NSData *dataObj = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *data = [NSJSONSerialization JSONObjectWithData:dataObj options:0 error:&error];
    
    if (! data) {
        NSLog(@"json conversion error: %@", error);
        return nil;
    }
    
    return data;
}

@end
