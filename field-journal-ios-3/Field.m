//
//  Field.m
//  Field Journal
//
//  Created by Elia Freedman on 1/19/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Field.h"
#import "Farm.h"
#import "Report.h"
#import "Session.h"
#import "FMDB.h"
#import "NSDictionary+Dictionary.h"
//#import "MapViewController.h"
#import "GeoJSON.h"
#import "NSString+String.h"


@implementation Field

- (id)initWithId:(NSInteger)dbId serverId:(NSInteger)serverId name:(NSString *)name boundary:(NSDictionary *)boundary farmId:(NSInteger)farmId permissions:(NSString *)permissions {
    self = [super init];
    if (! self) return nil;
    
    _dbId = dbId;
    _serverId = serverId;
    _name = ((NSNull *)name == [NSNull null] ? @"" : name);
    _boundary = ((NSNull *)boundary == [NSNull null] ? nil : boundary);
    _distance = nil;
    _farmId = farmId;
    _permissions = ((NSNull *)permissions == [NSNull null] ? [[Session shared] permissionForRecordType:RECORD_TYPE_FIELDS] : permissions);
    
    return self;
}

// save field changes
- (void)save {
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
    NSString *name = (self.name.length == 0 ? (NSString *)[NSNull null] : self.name);
    NSString *boundaryAsString = [self.boundary convertJSONDictionaryToString];
    
    FMDatabase *db = [Session openDatabase];
    
    [db executeUpdate:@"UPDATE fields SET state='updated',name=?,boundary=? WHERE id=?", name, boundaryAsString, idAsObject];
    if ([db hadError]) {
        NSLog(@"save error: %@", [db lastErrorMessage]);
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_FIELDS object:nil];
    }
    
    [Session closeDatabase];
}

// updates the field given a new farmId
- (void)updateFarm:(NSInteger)farmId {
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
    NSNumber *farmIdAsObject = [NSNumber numberWithInteger:farmId];
    
    FMDatabase *db = [Session openDatabase];
    
    [db executeUpdate:@"UPDATE fields SET state='updated',farm_id=? WHERE id=?", farmIdAsObject, idAsObject];
    if ([db hadError]) {
        NSLog(@"save error: %@", [db lastErrorMessage]);
    } else {
        self.farmId = farmId;
    }
    
    [Session closeDatabase];
}

// delete field
- (void)destroy {
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.dbId];
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields WHERE id=?", idAsObject];
    if ([results next]) {
        if (results[@"server_id"] == [NSNull null]) {
            // hasn't been synced yet, remove it immediately
            [db executeUpdate:@"DELETE FROM fields WHERE id=?", idAsObject];
        } else {
            // has been synced, need to alert the server
            [db executeUpdate:@"UPDATE fields SET state='deleted' WHERE id=?", idAsObject];
            if ([db hadError]) NSLog(@"save error: %@", [db lastErrorMessage]);
        }
        [Report deleteReportsForField:idAsObject];
    }
    
    [Session closeDatabase];
}

// returns YES if this field is a duplicate of another assigned to the same farm
- (BOOL)isDuplicateName:(NSString *)name {
    NSNumber *idAsObject = [NSNumber numberWithInteger:self.farmId];
    FMDatabase *db = [Session openDatabase];
    BOOL isDuplicate = NO;
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields WHERE farm_id=? AND state!='deleted'", idAsObject];
    while ([results next]) {
        if (results[@"name"] != [NSNull null] && [results[@"name"] caseInsensitiveCompare:name] == NSOrderedSame) {
            isDuplicate = YES;
            break;
        }
    }
    
    [Session closeDatabase];
    
    return isDuplicate;
}

// returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE
- (BOOL)hasPermissionForType:(NSString *)permissionType {
    if (! permissionType || ! self.permissions) return NO;
    return [self.permissions containsString:permissionType];
}


// create and return a new field given a farmId and a dictionary of latitude and longitude coordinates
+ (Field *)newFieldForFarmId:(NSInteger)farmId coordinates:(NSDictionary *)coordinates {
    NSString *uuid = [NSString nameForFileType:nil];
    NSDictionary *geoJSONBoundary = [GeoJSON pointFromCoordinates:coordinates];
    NSString *boundaryAsString = [geoJSONBoundary convertJSONDictionaryToString];
    Field *field;
    
    FMDatabase *db = [Session openDatabase];
    
    [db executeUpdate:@"INSERT INTO fields (uuid, state, farm_id, permissions, boundary) VALUES (?,?,?,?,?)", uuid, @"updated", @(farmId), FULL_PERMISSIONS, boundaryAsString];
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields WHERE uuid=?", uuid];
    if ([results next]) {
        NSInteger serverId = NSNotFound;
        if (results[@"server_id"] != [NSNull null]) serverId = [results[@"server_id"] integerValue];
        field = [[Field alloc] initWithId:[results[@"id"] integerValue] serverId:serverId name:@"" boundary:geoJSONBoundary farmId:farmId permissions:results[@"permissions"]];
    }
    
    [Session closeDatabase];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_FIELDS object:nil];
    
    return field;
}

// set record availability, returning an array of records needed
+ (NSArray *)setAvailability:(NSArray *)fields {
    NSMutableArray *records = [[NSMutableArray alloc] initWithArray:fields];
    FMDatabase *db = [Session openDatabase];
    
    // look at every record in the database, decide whether to keep it or remove it, whether to update its permissions, or whether to
    // request it from the server if it doesn't already exist or if the version has changed
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields"];
    while ([results next]) {
        BOOL found = NO;
        for (NSInteger i = 0; i < records.count; i++) {
            NSDictionary *field = [records objectAtIndex:i];
            if (results[@"server_id"] != [NSNull null] && [results[@"server_id"] integerValue] == [field[@"id"] integerValue]) {
                [db executeUpdate:@"UPDATE fields SET permissions=? WHERE server_id=?", field[@"r"], results[@"server_id"]];
                if (RECORD_VERSION_FOR_FIELD == [results[@"version"] integerValue]) [records removeObjectAtIndex:i];
                found = YES;
                break;
            }
        }
        if (! found) [db executeUpdate:@"UPDATE fields SET state='permission_removed' WHERE server_id=?", results[@"server_id"]];
    }
    
    [Session closeDatabase];
    
    // only return an array of ids
    NSMutableArray *neededRecords = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < records.count; i++) {
        NSDictionary *field = [records objectAtIndex:i];
        [neededRecords addObject:field[@"id"]];
    }
    
    return neededRecords;
}

// store and update fields, assumes farms have been stored first, returns array of error strings or empty array
+ (NSArray *)storeFields:(NSArray *)fields {
    FMResultSet *results;
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    for (NSDictionary *field in fields) {
        FMResultSet *farm = [db executeQuery:@"SELECT * FROM farms WHERE server_id=?", field[@"farm"]];
        NSString *boundary = [field[@"boundary"] convertJSONDictionaryToString];
        NSString *uuid = [field[@"clientId"] uppercaseString];

        if ([farm next]) {
            results = [db executeQuery:@"SELECT * FROM fields WHERE uuid=?", uuid];
            if ([results next]) {
                // update the existing record
                if ([results[@"modified_during_sync"] boolValue]) {
                    [db executeUpdate:@"UPDATE fields SET server_id=? WHERE id=?", field[@"id"], results[@"id"]];
                } else {
                    [db executeUpdate:@"UPDATE fields SET server_id=?, farm_id=?, name=?, boundary=?, uuid=?, state='', permissions=?, version=? WHERE id=?", field[@"id"], farm[@"id"], field[@"name"], boundary, uuid, field[@"r"], @(RECORD_VERSION_FOR_FIELD), results[@"id"]];
                }
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO fields (server_id, farm_id, name, boundary, uuid, permissions, version) VALUES (?,?,?,?,?,?,?)", field[@"id"], farm[@"id"], field[@"name"], boundary, uuid, field[@"r"], @(RECORD_VERSION_FOR_FIELD)];
            }
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeFields: %@", [db lastErrorMessage]]];
        } else {
            [errors addObject:[NSString stringWithFormat:@"storeFields: No farm with server id %@ for field with server id %@", field[@"farm"], field[@"id"]]];
        }
    }
    
    [Session closeDatabase];

    return errors;
}

// returns an array of modified fields or nil if there are none
+ (NSArray *)modifiedFields {
    NSMutableArray *modifiedRecords = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields WHERE state='updated'"];
    while ([results next]) {
        if (results[@"name"] != [NSNull null]) {
            NSMutableDictionary *record = [[NSMutableDictionary alloc] init];
            
            record[@"id"] = results[@"server_id"];
            record[@"clientId"] = results[@"uuid"];
            FMResultSet *farmResults = [db executeQuery:@"SELECT * FROM farms WHERE id=?", results[@"farm_id"]];
            if ([farmResults next]) record[@"farm"] = farmResults[@"uuid"];
            record[@"name"] = results[@"name"];
            record[@"boundary"] = [NSDictionary dictionaryFromJSONString:results[@"boundary"]];
            
            [modifiedRecords addObject:record];
        }
    }
    
    [Session closeDatabase];
    
    return ([modifiedRecords count] == 0 ? nil : modifiedRecords);
}

// check if there are incomplete field records
+ (BOOL)hasIncompleteFields {
    BOOL error = NO;
    
    FMDatabase *db = [Session openDatabase];
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields WHERE state='updated' AND name IS NULL"];
    if ([results next]) error = YES;
    [Session closeDatabase];
    
    return error;
}

// Returns an array of deleted field server ids or nil if there are none. Will only return records where the state is marked 'deleted' and there is a server_id.
// Records without a server id are handled with a special upload flag.
+ (NSArray *)deletedFields {
    NSMutableArray *deletedRecords = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields WHERE state='deleted'"];
    while ([results next]) {
        [deletedRecords addObject:results[@"server_id"]];
    }
    
    [Session closeDatabase];
    
    return ([deletedRecords count] == 0 ? nil : deletedRecords);
}

// delete fields
+ (void)deleteFields:(NSArray *)fields {
    FMResultSet *results;
    FMDatabase *db = [Session openDatabase];
    
    for (NSNumber *fieldId in fields) {
        results = [db executeQuery:@"SELECT * FROM fields WHERE server_id=?", fieldId];
        if ([results next]) {
            [Report deleteReportsForField:results[@"id"]];
            [db executeUpdate:@"DELETE FROM fields WHERE id=?", results[@"id"]];
        }
    }
    
    results = [db executeQuery:@"SELECT * FROM fields WHERE state='permission_removed'"];
    while ([results next]) {
        [Report deleteReportsForField:results[@"id"]];
        [db executeUpdate:@"DELETE FROM fields WHERE id=?", results[@"id"]];
    }
    
    [Session closeDatabase];
}

// deletes all fields with a given farm id
+ (void)deleteFieldsForFarm:(NSNumber *)farmId {
    FMDatabase *db = [Session openDatabase];
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields WHERE farm_id=?", farmId];
    while ([results next]) {
        [Report deleteReportsForField:results[@"id"]];
        [db executeUpdate:@"DELETE FROM fields WHERE id=?", results[@"id"]];
    }
    [Session closeDatabase];
}

// clear the modified_during_state flags on all records
+ (void)clearModifiedDuringSync {
    NSNumber *trueAsObject = [NSNumber numberWithBool:YES];
    NSNumber *falseAsObject = [NSNumber numberWithBool:NO];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields WHERE modified_during_sync=?", trueAsObject];
    while ([results next]) {
        [db executeUpdate:@"UPDATE fields SET modified_during_sync=? WHERE id=?", falseAsObject, results[@"id"]];
    }
    
    [Session closeDatabase];
}

// returns a Field object given a database query
+ (Field *)fieldWithResultSet:(FMResultSet *)results {
    NSDictionary *boundary = [NSDictionary dictionaryFromJSONString:results[@"boundary"]];
    NSInteger serverId = NSNotFound;
    if (results[@"server_id"] != [NSNull null]) serverId = [results[@"server_id"] integerValue];
    return [[Field alloc] initWithId:[results[@"id"] integerValue] serverId:serverId name:results[@"name"] boundary:boundary farmId:[results[@"farm_id"] integerValue] permissions:results[@"permissions"]];
}

// returns an array of all fields in alphabetical order; if onlyNamedFields is YES than skip blank names
+ (NSArray <Field *> *)allFieldsWithNamesOnly:(BOOL)onlyNamedFields {
    NSMutableArray *fields = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields ORDER BY name ASC"];
    while ([results next]) {
        Field *field = [self fieldWithResultSet:results];
        if (! onlyNamedFields || (onlyNamedFields && field.name.length > 0)) [fields addObject:field];
    }
    
    [Session closeDatabase];
    
    return fields;
}

// Returns an array of all fields in alphabetical order, organized by farm in alphabetical order. Each array object is a dictionary of @"farm" and @"fields" where
// @"farm" is a Farm object and fields is an array of Field objects. If searchText is provided then the same array is returned except filtered to only fields that
// match the search criteria.
+ (NSArray <NSDictionary *> *)allFieldsAndFarmsWithSearch:(NSString *)searchText {
    NSMutableArray *fields = [[NSMutableArray alloc] init];
    NSArray *farms = [Farm allFarmsWithNamesOnly:YES];
    
    FMDatabase *db = [Session openDatabase];
    
    for (Farm *farm in farms) {
        NSNumber *farmIdAsObject = [NSNumber numberWithInteger:farm.dbId];
        NSMutableArray *fieldsForFarm = [[NSMutableArray alloc] init];
        FMResultSet *results;
        if (searchText && searchText.length > 0) {
            // filter the list of dictionary to fields that match the search criteria if there is a search string provided
            NSString *searchString = [NSString stringWithFormat:@"%%%@%%", searchText];
            results = [db executeQuery:@"SELECT * FROM fields WHERE farm_id=? AND state!='deleted' AND name LIKE ? ORDER BY name ASC", farmIdAsObject, searchString];
        } else {
            // show all results
            results = [db executeQuery:@"SELECT * FROM fields WHERE farm_id=? AND state!='deleted' ORDER BY name ASC", farmIdAsObject];
        }
        while ([results next]) {
            Field *field = [self fieldWithResultSet:results];
            if (field.name.length > 0) [fieldsForFarm addObject:field];
        }
        
        if (fieldsForFarm.count > 0) [fields addObject:@{@"farm" : farm, @"fields" : fieldsForFarm}];
    }

    [Session closeDatabase];
    
    return fields;
}

// returns an array of all fields in proximity to the current location
+ (NSArray <Field *> *)allFieldsInProximityToCurrentLocation {
    NSMutableArray *fields = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields WHERE name!='' AND name IS NOT NULL"];
    while ([results next]) {
        Field *field = [self fieldWithResultSet:results];
        [fields addObject:field];
    }
    
    [Session closeDatabase];
    
    return [fields sortedArrayUsingComparator:^NSComparisonResult(Field *f1, Field *f2) {
        return [f1.closestDistanceToHere compare:f2.closestDistanceToHere];
    }];
}

// return the distance from current location to field's closest boundary point in meters
// caches result to avoid repeated recalcs during sort
- (NSNumber *)closestDistanceToHere {
    if (! self.distance) {
        CLLocationCoordinate2D here = CLLocationCoordinate2DMake([Session shared].currentLatitude, [Session shared].currentLongitude);
        
        if (! self.boundary) self.distance = [NSNumber numberWithDouble:MAXFLOAT];
        else {
            [GeoJSON forEachPointIn:self.boundary do:^(double lat, double lon) {
                CLLocationCoordinate2D there = CLLocationCoordinate2DMake(lat, lon);
                double meters = [MapViewController greatCircleDistanceFrom:here to:there];
                if (! self.distance || meters < self.distance.doubleValue) self.distance = [NSNumber numberWithDouble:meters];
            }];
        }
    }
    
    return self.distance;
}

// returns a Field object given a database id
+ (Field *)fieldForId:(NSInteger)fieldId {
    NSNumber *fieldIdAsObject = [NSNumber numberWithInteger:fieldId];
    Field *field;
    
    FMDatabase *db = [Session openDatabase];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM fields WHERE id=?", fieldIdAsObject];
    if ([results next]) {
        field = [self fieldWithResultSet:results];
    }
    
    [Session closeDatabase];
    
    return field;
}

@end
