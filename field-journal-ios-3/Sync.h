//
//  Sync.h
//  field-journal-ios-3
//
//  Created by Elia Freedman on 1/18/17.
//  Copyright © 2017 CHS, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol SyncAuthenticateDelegate <NSObject>
    
- (void)authenticateFailed:(NSString *)message;                     // authenticate failed, reports an error message
- (void)authenticateCompletedWithToken:(NSString *)token;           // authenticate completed with an auth token
    
@end


@protocol SyncDatabaseDelegate <NSObject>
    
- (void)syncBeginDownloadingImage;                                  // sync will begin downloading images
- (void)syncDatabaseCompleted:(NSString *)message;                  // sync database completed, reports an error message or nil if no error
    
@end


@interface Sync : NSObject

+ (Sync *)shared;
@property (retain) id <SyncAuthenticateDelegate> authenticateDelegate;
@property (retain) id <SyncDatabaseDelegate> syncDatabaseDelegate;

// Internet connection
- (void)monitorInternetConnection;                                  // constantly monitors the Internet connection, updating canSynchronize as status changes.
+ (BOOL)syncUrlIsReachable;                                         // returns YES if sync url is reachable with any Internet connection
+ (BOOL)isWifiConnection;                                           // returns YES if the internet connection is wifi

// Synchronization
- (void)login:(NSString*)email withPassword:(NSString*)password;
- (void)syncDatabase;

@end
