//
//  UIView+View.m
//  Field Journal
//
//  Created by Elia Freedman on 1/29/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "UIView+View.h"

@implementation UIView (View)

// change view to be a circle filled with color
- (void)circleOfColor:(UIColor *)color {
    CGFloat size = (self.frame.size.width > self.frame.size.height ? self.frame.size.height : self.frame.size.width);
    self.alpha = 1.0;
    self.layer.cornerRadius = size / 2;
    self.backgroundColor = color;
}

@end
