//
//  NSDictionary+Dictionary.m
//  Field Journal
//
//  Created by Elia Freedman on 1/20/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "NSDictionary+Dictionary.h"

@implementation NSDictionary (Dictionary)

// converts a dictionary containing JSON to a string
- (NSString *)convertJSONDictionaryToString {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
    
    if (! jsonData) {
        NSLog(@"json conversion error: %@", error);
        return nil;
    }

    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

// converts a string containing JSON to a dictionary
+ (NSDictionary *)dictionaryFromJSONString:(NSString *)jsonString {
    if (! jsonString || (NSNull *)jsonString == [NSNull null]) return nil;

    NSError *error;
    NSData *dataObj = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:dataObj options:0 error:&error];
    
    if (! data) {
        NSLog(@"json conversion error: %@", error);
        return nil;
    }
    
    return data;
}

@end
