//
//  Farm.h
//  Field Journal
//
//  Created by Elia Freedman on 1/19/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Coop.h"


@interface Farm : NSObject

// returns an array of all farms in alphabetical order; if onlyNamedFarms is YES than skip blank names
+ (NSArray <Farm *> *)allFarmsWithNamesOnly:(BOOL)onlyNamedFarms;

// Returns an array of all farms in alphabetical order, organized by grower in alphabetical order. Each array object is a dictionary of @"grower" and @"farms" where
// @"grower" is a Grower object and farms is an array of Farm objects.
+ (NSArray <NSDictionary *> *)allFarmsAndGrowers;

+ (Farm *)farmForId:(NSInteger)farmId;          // returns a Farm object given a database id
+ (Farm *)farmForFieldId:(NSInteger)fieldId;    // returns a Farm object given a field's database id

// create and return a new farm given a growerId
+ (Farm *)newFarmForGrowerId:(NSInteger)growerId;

+ (NSArray *)setAvailability:(NSArray *)farms;  // set record availability, returning an array of records needed
+ (NSArray *)storeFarms:(NSArray *)farms;       // store and update farms, returns array of error strings or empty array
+ (NSArray *)storeGrowersForFarms:(NSArray *)farms;     // store grower-farm relationships

+ (void)deleteFarms:(NSArray *)farms;           // delete farms
+ (void)deleteFarmsForGrower:(NSNumber *)growerId;     // deletes all farms with a given grower id

+ (NSArray *)modifiedFarms;                     // returns an array of modified farms or nil if there are none
+ (NSArray *)deletedFarms;                      // returns an array of deleted farms server ids or nil if there are none
+ (BOOL)hasIncompleteFarms;                     // check if there are incomplete farm records
+ (void)clearModifiedDuringSync;                // clear the modified_during_state flags on all records

+ (BOOL)isDuplicateName:(NSString *)name;       // returns YES if this farm is a duplicate of another


@property (nonatomic) NSInteger dbId;           // database id
@property (nonatomic) NSString *name;           // farm name
@property (nonatomic) NSInteger growerId;       // id of the grower
@property (nonatomic) NSString *permissions;    // permissions
@property (nonatomic) NSInteger coopId;         // id of coop farm is member of, or NSNotFound if none

- (Coop *)coop;                                 // returns Coop object this farm is member of, or nil if none

- (void)save;                                                       // save farm changes
- (void)updateGrower:(NSInteger)growerId;                           // updates the grower given a new growerId
- (void)destroy;                                                    // delete farm
- (BOOL)hasPermissionForType:(NSString *)permissionType;            // returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE

@end
