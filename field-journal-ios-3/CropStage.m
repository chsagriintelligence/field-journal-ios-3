//
//  CropStage.m
//  Field Journal
//
//  Created by Elia Freedman on 7/14/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "CropStage.h"
#import "Crop.h"
#import "Session.h"
#import "FMDB.h"


@implementation CropStage

- (id)initWithServerId:(NSInteger)serverId code:(NSString *)code name:(NSString *)name desc:(NSString *)desc ordinal:(NSInteger)ordinal cropId:(NSInteger)cropId {
    self = [super init];
    if (! self) return nil;
    
    _serverId = serverId;
    _code = ((NSNull *)code == [NSNull null] ? @"" : code);
    _name = ((NSNull *)name == [NSNull null] ? @"" : name);
    _desc = ((NSNull *)desc == [NSNull null] ? @"" : desc);
    _ordinal = ordinal;
    _cropId = cropId;

    return self;
}

// returns a CropStage object given a server id
+ (CropStage *)cropStageForServerId:(NSInteger)serverId {
    Session *session = [Session shared];
    __block CropStage *cropStage;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM cropstages WHERE server_id=?", @(serverId)];
        if ([results next]) {
            cropStage = [CropStage cropStageWithResultSet:results];
        }
        [results close];
    
    }];
    
    return cropStage;
}

// store and update crop stages, returns array of error strings or empty array
+ (NSArray *)storeCropStages:(NSArray *)cropStages {
    Session *session = [Session shared];
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *cropStage in cropStages) {
            FMResultSet *crop = [db executeQuery:@"SELECT * FROM crops WHERE server_id=?", cropStage[@"cropId"]];
            [crop next];
            FMResultSet *results = [db executeQuery:@"SELECT * FROM cropstages WHERE server_id=?", cropStage[@"id"]];

            if ([results next]) {
                // update the existing record
                [db executeUpdate:@"UPDATE cropstages SET ordinal=?, crop_id=?, code=?, name=?, description=? WHERE server_id=?", cropStage[@"order"], crop[@"id"], cropStage[@"code"], cropStage[@"name"], cropStage[@"description"], results[@"server_id"]];
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO cropstages (server_id, ordinal, crop_id, code, name, description) VALUES (?,?,?,?,?,?)", cropStage[@"id"], cropStage[@"order"], crop[@"id"], cropStage[@"code"], cropStage[@"name"], cropStage[@"description"]];
            }
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeCropStages: %@", [db lastErrorMessage]]];
            [results close];
            [crop close];
        }
    
    }];
    
    return errors;
}

// returns a Field object given a database query
+ (CropStage *)cropStageWithResultSet:(FMResultSet *)results {
    return [[CropStage alloc] initWithServerId:[results[@"server_id"] integerValue] code:results[@"code"] name:results[@"name"] desc:results[@"description"] ordinal:[results[@"ordinal"] integerValue] cropId:[results[@"crop_id"] integerValue]];
}

// returns an array of all crop stages in ordinal order
+ (NSArray <CropStage *> *)allCropStagesForCropId:(NSInteger)cropId {
    Session *session = [Session shared];
    NSMutableArray *cropStages = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM cropstages WHERE crop_id=? ORDER BY crop_id ASC, ordinal ASC", @(cropId)];
        while ([results next]) {
            CropStage *stage = [CropStage cropStageWithResultSet:results];
            [cropStages addObject:stage];
        }
        [results close];
    
    }];
    
    return cropStages;
}

@end
