//
//  UIImage+Image.m
//  Field Journal
//
//  Created by Elia Freedman on 2/12/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "UIImage+Image.h"
#import "Session.h"


#define MAX_IMAGE_SIZE      1024

@implementation UIImage (Image)

// scales image to max size & stores as png file to the Documents directory with a given name
- (void)storeScaledPNGImage:(NSString *)name {
    UIImage *scaledImage = [self resizeImage:MAX_IMAGE_SIZE];
    
    // save the file to Documents directory
    NSData *imageData = UIImagePNGRepresentation(scaledImage);
    [UIImage storeImage:name withData:imageData];
}

// scales image to max size & stores as jpg file to the Documents directory with a given name
- (void)storeScaledJPEGImage:(NSString *)name {
	UIImage *scaledImage = [self resizeImage:MAX_IMAGE_SIZE];
	
	// save the file to Documents directory
	NSData *imageData = UIImageJPEGRepresentation(scaledImage, 0.95);
    [UIImage storeImage:name withData:imageData];
}

// resize an image where the longer side matches parameter 'size'. The image is resized only if the image can be shrunk.
// If the image will be unchanged or is already smaller than 'size' then the original is returned.
- (UIImage *)resizeImage:(CGFloat)size {
    CGFloat factor;
    UIImage *scaledImage;
    factor = (self.size.width > self.size.height ? size / self.size.width  : size / self.size.height);
    if (factor >= 1.0) return self;
    
    CGSize resize = CGSizeApplyAffineTransform(self.size, CGAffineTransformMakeScale(factor, factor));
    UIGraphicsBeginImageContextWithOptions(resize, YES, self.scale);
    [self drawInRect:CGRectMake(0.0, 0.0, resize.width, resize.height)];
    scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

// return a copy of the image using the given alpha opacity from 0.0 to 1.0 
- (UIImage *)imageUsingAlpha:(CGFloat)alpha {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    CGContextSetAlpha(ctx, alpha);
    CGContextDrawImage(ctx, area, self.CGImage);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

// given the name of an image, retrieve that image from memory, resize it and save it back to
// file storage with the prefix attached in the form "prefix_name". Re-sizing will make the longer
// side equal to size and adjust the shorter side accordingly.
+ (NSString *)scaleImage:(NSString *)name size:(CGFloat)size prefix:(NSString *)prefix {
    UIImage *image = [UIImage retrieveImage:name];
    UIImage *scaledImage = [image resizeImage:size];
    NSString *newName = [NSString stringWithFormat:@"%@_%@", prefix, name];
    [scaledImage storeScaledPNGImage: newName];
    return newName;
}

// returns the path for file with name
+ (NSString *)pathForImage:(NSString *)name {
	//NSLog(@"path for image %@", [[[Session shared] imagesPath] stringByAppendingPathComponent:name]);
    return [[[Session shared] imagesPath] stringByAppendingPathComponent:name];
}

// saves an NSData block as the named file in the images directory
+ (void)storeImage:(NSString *)name withData:(NSData *)imageData {
    NSString* path = [UIImage pathForImage:name];
    [imageData writeToFile:path atomically:YES];
}

// retrieve an image with the given name from the Documents directory
+ (UIImage *)retrieveImage:(NSString *)name {
    NSString* path = [UIImage pathForImage:name];
    return [UIImage imageWithContentsOfFile:path];
}

// returns true if the image exists in the file system
+ (BOOL)imageExists:(NSString *)name {
    NSString* path = [UIImage pathForImage:name];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:path];
}

// delete an image with the given name from the Documents directory
+ (void)deleteImage:(NSString *)name {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* path = [UIImage pathForImage:name];
    [fileManager removeItemAtPath:path error:nil];
}

// convert an image found at filename 'name' to data object
+ (NSData *)imageAsData:(NSString *)name {
    UIImage *image = [self retrieveImage:name];
    NSData *imageData = UIImagePNGRepresentation(image);
    return imageData;
}

// output app support directory to NSLog
+ (void)logOfDocumentsDirectory {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSLog(@"App Support directory: %@", [fileManager contentsOfDirectoryAtPath:[[Session shared] storagePath] error:nil]);
}

// creates and returns a circle of a certain size and color
+ (UIImage *)circleOfSize:(CGSize)size color:(UIColor *)color {
    UIImage *circle;
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    CGRect rect = CGRectMake(0.0, 0.0, size.width, size.height);
    CGContextSetFillColorWithColor(ctx, [color CGColor]);
    CGContextFillEllipseInRect(ctx, rect);
    
    CGContextRestoreGState(ctx);
    circle = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return circle;
}

@end
