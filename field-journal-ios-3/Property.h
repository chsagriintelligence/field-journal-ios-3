//
//  Property.h
//  Field Journal
//
//  Created by Elia Freedman on 2/9/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Property : NSObject

+ (Property *)propertyForId:(NSInteger)propertyId;              // returns a Property object given a database id
+ (Property *)propertyForKey:(NSString *)key;                   // returns a Property object given a property key

+ (NSArray *)storeProperties:(NSArray *)properties;             // store and update properties, returns array of error strings or empty array

@property (nonatomic) NSInteger dbId;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *key;

@end
