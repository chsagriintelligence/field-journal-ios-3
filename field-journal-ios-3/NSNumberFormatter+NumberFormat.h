//
//  NSNumberFormatter+NumberFormat.h
//  Field Journal
//
//  Created by Elia Freedman on 1/26/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumberFormatter (NumberFormat)

+ (NSNumberFormatter *)decimalNumberFormat:(NSUInteger)decimalPlaces;

@end
