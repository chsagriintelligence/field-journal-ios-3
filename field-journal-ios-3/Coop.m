//
//  Coop.m
//  Field Journal
//
//  Created by Rick Huebner on 11/11/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Coop.h"
#import "Session.h"
#import "FMDB.h"
#import "Grower.h"
#import "Field.h"
#import "Farm.h"

@implementation Coop

- (id)initWithId:(NSInteger)dbId name:(NSString *)name permissions:(NSString *)permissions {
    self = [super init];
    if (! self) return nil;
    
    _dbId = dbId;
    _name = ((NSNull *)name == [NSNull null] ? @"" : name);
    _permissions = ((NSNull *)permissions == [NSNull null] ? [[Session shared] permissionForRecordType:RECORD_TYPE_REPORTS] : permissions);
    
    return self;
}

// returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE
- (BOOL)hasPermissionForType:(NSString *)permissionType {
    if (! permissionType || ! self.permissions) return NO;
    return [self.permissions containsString:permissionType];
}

// set record availability, returning an array of records needed
+ (NSArray *)setAvailability:(NSArray *)coops {
    Session *session = [Session shared];
    NSMutableArray *records = [NSMutableArray arrayWithArray:coops];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        // look at every record in the database, decide whether to keep it or remove it, whether to update its permissions, or whether to
        // request it from the server if it doesn't already exist or if the version has changed
        FMResultSet *results = [db executeQuery:@"SELECT * FROM coops"];
        while ([results next]) {
            BOOL found = NO;
            for (NSInteger i = 0; i < records.count; i++) {
                NSDictionary *coop = [records objectAtIndex:i];
                if (results[@"server_id"] != [NSNull null] && [results[@"server_id"] integerValue] == [coop[@"id"] integerValue]) {
                    [db executeUpdate:@"UPDATE coops SET permissions=? WHERE server_id=?", coop[@"r"], results[@"server_id"]];
                    if (RECORD_VERSION_FOR_COOP == [results[@"version"] integerValue]) [records removeObjectAtIndex:i];
                    found = YES;
                    break;
                }
            }
            if (! found) [db executeUpdate:@"UPDATE coops SET state='permission_removed' WHERE server_id=?", results[@"server_id"]];
        }
        [results close];
    
    }];
    
    // only return an array of ids
    NSMutableArray *neededRecords = [NSMutableArray array];
    for (NSInteger i = 0; i < records.count; i++) {
        NSDictionary *coop = [records objectAtIndex:i];
        [neededRecords addObject:coop[@"id"]];
    }
    
    return neededRecords;
}

// store and update coops, returns array of error strings or empty array
+ (NSArray *)storeCoops:(NSArray *)coops {
    Session *session = [Session shared];
    NSMutableArray *errors = [NSMutableArray array];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *coop in coops) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM coops WHERE server_id=?", coop[@"id"]];
            if ([results next]) {
                // update the existing record
                [db executeUpdate:@"UPDATE coops SET name=?, state='', permissions=?, version=? WHERE server_id=?", coop[@"name"], coop[@"r"], @(RECORD_VERSION_FOR_COOP), results[@"server_id"]];
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO coops (server_id, name, permissions, version) VALUES (?,?,?,?)", coop[@"id"], coop[@"name"], coop[@"r"], @(RECORD_VERSION_FOR_COOP)];
            }
            
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeCoops: %@", [db lastErrorMessage]]];
            [results close];
        }
    
    }];
    
    return errors;
}

// delete coops
+ (void)deleteCoops:(NSArray <NSNumber *> *)coops {
    Session *session = [Session shared];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSNumber *coopId in coops) {
            [db executeUpdate:@"DELETE FROM coops WHERE server_id=?", coopId];
        }
        
        [db executeUpdate:@"DELETE FROM coops WHERE state='permission_removed'"];
    
    }];
}

// returns a Coop object given a database query
+ (Coop *)coopWithResultSet:(FMResultSet *)results {
    return [[Coop alloc] initWithId:[results[@"server_id"] integerValue] name:results[@"name"] permissions:results[@"permissions"]];
}

// returns all coops in alphabetical order by name
+ (NSArray <Coop *> *)coops {
    Session *session = [Session shared];
    NSMutableArray *coops = [NSMutableArray array];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM coops WHERE state!='deleted' ORDER BY name ASC"];
        while ([results next]) {
            Coop *coop = [Coop coopWithResultSet:results];
            [coops addObject:coop];
        }
        [results close];
    
    }];
    
    return coops;
}

// returns Coop objects given a grower's id, sorted by name
+ (NSArray <Coop *> *)coopsForGrowerId:(NSInteger)growerId {
    Session *session = [Session shared];
    NSMutableArray *coops = [NSMutableArray array];

    Grower *grower = [Grower growerForId:growerId];
    if (grower.coopIds.count > 0) {
        FMDatabaseQueue *queue = [session databaseQueue];
        [queue inDatabase:^(FMDatabase *db) {
        
            NSString *query = [NSString stringWithFormat:@"SELECT * FROM coops WHERE server_id IN (%@) AND state!='deleted' ORDER BY name ASC", [grower.coopIds componentsJoinedByString:@","]];
            FMResultSet *results = [db executeQuery:query];
            while ([results next]) {
                Coop *coop = [Coop coopWithResultSet:results];
                [coops addObject:coop];
            }
            [results close];
        
        }];
    }
    
    return coops;
}

// returns a Coop object given a coop id
+ (Coop *)coopForId:(NSInteger)coopId {
    Session *session = [Session shared];
    __block Coop *coop;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM coops WHERE server_id=? AND state != 'deleted' LIMIT 1", @(coopId)];
        if ([results next]) {
            coop = [Coop coopWithResultSet:results];
        }
        [results close];
    
    }];
    
    return coop;
}

// returns the coop id for the farm which contains the field, or NSNotFound if error
+ (NSInteger)coopIdForFieldId:(NSInteger)fieldId {
    Field *field = [Field fieldForId:fieldId];
    if (field) {
        Farm *farm = [Farm farmForId:field.farmId];
        if (farm) {
            Coop *coop = [Coop coopForId:farm.coopId];
            if (coop) return coop.dbId;
        }
    }
    
    return NSNotFound;
}

@end
