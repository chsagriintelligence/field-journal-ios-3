//
//  Migration.h
//  Field Journal
//
//  Created by Elia Freedman on 3/10/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Migration : NSObject

+ (void)migrate;

@end
