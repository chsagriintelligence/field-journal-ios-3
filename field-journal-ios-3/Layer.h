//
//  Layer.h
//  Field Journal
//
//  Created by Elia Freedman on 2/4/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIColor;


@interface Layer : NSObject

+ (NSArray <Layer *> *)allObservationLayers;                    // returns an array of observation layers
+ (Layer *)layerForId:(NSInteger)layerId;                       // returns a Layer object given a database id

+ (NSArray *)storeLayers:(NSArray *)layers;                     // store and update layers, returns array of error strings or empty array

@property (nonatomic) NSInteger dbId;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *key;
@property (nonatomic) UIColor *color;
@property (nonatomic) NSArray *properties;

@end
