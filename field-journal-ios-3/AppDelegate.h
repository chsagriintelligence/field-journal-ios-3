//
//  AppDelegate.h
//  field-journal-ios-3
//
//  Created by Elia Freedman on 1/18/17.
//  Copyright © 2017 CHS, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

