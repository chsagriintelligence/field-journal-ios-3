//
//  User.m
//  Field Journal
//
//  Created by Elia Freedman on 7/13/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "User.h"
#import "FMDB.h"
#import "Session.h"


@implementation User

// returns a user's name given a user server id
+ (NSString *)userForId:(NSInteger)userServerId {
    Session *session = [Session shared];
    __block NSString *name = @"unknown";
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM users WHERE server_id=?", @(userServerId)];
        if ([results next]) {
            name = results[@"name"];
        }
        [results close];

    }];
    
    return name;
}

// set record availability, returning an array of records needed
+ (NSArray *)setAvailability:(NSArray *)users {
    Session *session = [Session shared];
    NSMutableArray *records = [[NSMutableArray alloc] initWithArray:users];

    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        // look at every record in the database, decide whether to keep it or remove it, whether to update its permissions, or whether to
        // request it from the server if it doesn't already exist or if the version has changed
        FMResultSet *results = [db executeQuery:@"SELECT * FROM users"];
        while ([results next]) {
            BOOL found = NO;
            for (NSInteger i = 0; i < records.count; i++) {
                NSDictionary *user = [records objectAtIndex:i];
                if (results[@"server_id"] != [NSNull null] && [results[@"server_id"] integerValue] == [user[@"id"] integerValue]) {
                    [db executeUpdate:@"UPDATE users SET permissions=? WHERE server_id=?", user[@"r"], results[@"server_id"]];
                    if (RECORD_VERSION_FOR_USER == [results[@"version"] integerValue]) [records removeObjectAtIndex:i];
                    found = YES;
                    break;
                }
            }
            if (! found) [db executeUpdate:@"UPDATE users SET state='permission_removed' WHERE server_id=?", results[@"server_id"]];
        }
        [results close];
    
    }];
    
    // only return an array of ids
    NSMutableArray *neededRecords = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < records.count; i++) {
        NSDictionary *field = [records objectAtIndex:i];
        [neededRecords addObject:field[@"id"]];
    }
    
    return neededRecords;
}

// store and update users, returns array of error strings or empty array
+ (NSArray *)storeUsers:(NSArray *)users {
    Session *session = [Session shared];
    NSMutableArray *errors = [[NSMutableArray alloc] init];

    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *user in users) {
            NSString *uuid = [user[@"clientId"] uppercaseString];

            FMResultSet *results = [db executeQuery:@"SELECT * FROM users WHERE server_id=?", user[@"id"]];
            if ([results next]) {
                // update the existing record
                [db executeUpdate:@"UPDATE users SET uuid=?, state='', name=?, permissions=?, version=? WHERE server_id=?", uuid, user[@"fullName"], user[@"r"], user[@"v"], results[@"server_id"]];
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO users (server_id, uuid, name, permissions, version) VALUES (?,?,?,?,?)", user[@"id"], uuid, user[@"fullName"], user[@"r"], user[@"v"]];
            }
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeUsers: %@", [db lastErrorMessage]]];
            if ([user[@"isCurrent"] boolValue]) [[Session shared] setCurrentUser:[user[@"id"] integerValue]];
            [results close];
        }
    
    }];
    
    return errors;
}

// delete users
+ (void)deleteUsers:(NSArray *)users {
    Session *session = [Session shared];
    NSInteger currentUser = [session currentUser];

    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSNumber *userId in users) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM users WHERE server_id=?", userId];
            if ([results next]) {
                [db executeUpdate:@"DELETE FROM users WHERE server_id=?", results[@"server_id"]];
            }
            if ([userId integerValue] == currentUser) [[Session shared] setCurrentUser:0];
            [results close];
        }
        
        [db executeUpdate:@"DELETE FROM users WHERE state='permission_removed'"];

    }];
}

@end
