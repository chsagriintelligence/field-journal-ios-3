//
//  UIImage+Image.h
//  Field Journal
//
//  Created by Elia Freedman on 2/12/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Image)

- (void)storeScaledPNGImage:(NSString *)name;   // scales image to max size & stores as png file to the Documents directory with a given name
- (void)storeScaledJPEGImage:(NSString *)name;  // scales image to max size & stores as jpg file to the Documents directory with a given name
- (UIImage *)imageUsingAlpha:(CGFloat)alpha;    // return a copy of the image using the given alpha opacity from 0.0 to 1.0

+ (NSString *)pathForImage:(NSString *)name;    // returns the path for file with name
+ (NSString *)scaleImage:(NSString *)name size:(CGFloat)size prefix:(NSString *)prefix; // re-size an image in memory to 'size' with 'prefix' added to the name in 'prefix_name' format
+ (void)storeImage:(NSString *)name withData:(NSData *)imageData; // saves an NSData block as the named file in the images directory
+ (UIImage *)retrieveImage:(NSString *)name;    // retrieve an image with the given name from the Documents directory
+ (void)deleteImage:(NSString *)name;           // delete an image with the given name from the Documents directory
+ (NSData *)imageAsData:(NSString *)name;       // convert an image found at filename 'name' to data object
+ (void)logOfDocumentsDirectory;                // output documents directory to NSLog

+ (BOOL)imageExists:(NSString *)name;           // returns true if the image exists in the file system

// creates and returns a circle of a certain size and color
+ (UIImage *)circleOfSize:(CGSize)size color:(UIColor *)color;

@end
