//
//  Grower.m
//  Field Journal
//
//  Created by Elia Freedman on 1/19/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Grower.h"
#import "Farm.h"
#import "Session.h"
#import "FMDB.h"
#import "NSString+String.h"
#import "NSArray+Array.h"


@implementation Grower


- (id)initWithId:(NSInteger)dbId firstname:(NSString *)firstname middlename:(NSString *)middlename lastname:(NSString *)lastname company:(NSString *)company email:(NSString *)email permissions:(NSString *)permissions coopIds:(NSArray <NSNumber *> *)coopIds {
    self = [super init];
    if (! self) return nil;
    
    _dbId = dbId;
    _firstName = ((NSNull *)firstname == [NSNull null] ? @"" : firstname);
    _middleName = ((NSNull *)middlename == [NSNull null] ? @"" : middlename);
    _lastName = ((NSNull *)lastname == [NSNull null] ? @"" : lastname);
    _company = ((NSNull *)company == [NSNull null] ? @"" : company);
    _email = ((NSNull *)email == [NSNull null] ? @"" : email);
    _permissions = ((NSNull *)permissions == [NSNull null] ? [[Session shared] permissionForRecordType:RECORD_TYPE_REPORTS] : permissions);
    [self setDisplayName];
    _coopIds = [NSArray arrayWithArray:coopIds];

    return self;
}

- (void)setDisplayName {
    if (self.firstName.length != 0 && self.lastName.length != 0) _displayName = [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
    else if (self.company.length != 0) _displayName = self.company;
    else _displayName = @"";
}

// save grower changes
- (void)save {
    Session *session = [Session shared];
    NSString *firstName = (self.firstName.length == 0 ? (NSString *)[NSNull null] : self.firstName);
    NSString *middleName = (self.middleName.length == 0 ? (NSString *)[NSNull null] : self.middleName);
    NSString *lastName = (self.lastName.length == 0 ? (NSString *)[NSNull null] : self.lastName);
    NSString *company = (self.company.length == 0 ? (NSString *)[NSNull null] : self.company);
    NSString *email = (self.email.length == 0 ? (NSString *)[NSNull null] : self.email);
    NSString *coopIds = (! self.coopIds) ? @"[]" : [self.coopIds convertJSONArrayToString];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        [db executeUpdate:@"UPDATE growers SET state='updated',first_name=?,middle_name=?,last_name=?,company=?,email=?,coop_ids=? WHERE server_id=?", firstName, middleName, lastName, company, email, coopIds, @(self.dbId)];
        if ([db hadError]) {
            NSLog(@"save error: %@", [db lastErrorMessage]);
        } else {
            [self setDisplayName];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_GROWERS object:nil];
        }
    
    }];
}

// delete grower
- (void)destroy {
    Session *session = [Session shared];

    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM growers WHERE server_id=?", @(self.dbId)];
        if ([results next]) {
            if (results[@"server_id"] == [NSNull null]) {
                // hasn't been synced yet, remove it immediately
                [db executeUpdate:@"DELETE FROM growers WHERE server_id=?", @(self.dbId)];
            } else {
                // has been synced, need to alert the server
                [db executeUpdate:@"UPDATE growers SET state='deleted' WHERE server_id=?", @(self.dbId)];
                if ([db hadError]) NSLog(@"save error: %@", [db lastErrorMessage]);
            }
            [Farm deleteFarmsForGrower:@(self.dbId)];
        }
        [results close];
    
    }];
}

// returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE
- (BOOL)hasPermissionForType:(NSString *)permissionType {
    if (! permissionType || ! self.permissions) return NO;
    return [self.permissions containsString:permissionType];
}

// returns array of Coop objects this grower is member of
- (NSArray <Coop *> *)coops {
    return [Coop coopsForGrowerId:self.dbId];
}

// create and return a new grower
+ (Grower *)newGrower {
    Session *session = [Session shared];
    NSString *uuid = [NSString nameForFileType:nil];
    __block Grower *grower;
    
    // new growers default to being in the first defined coop (if any)
    NSArray *coops = [Coop coops];
    NSMutableArray *onlyFirst = [NSMutableArray array];
    if (coops.count > 0) [onlyFirst addObject:@([coops[0] dbId])];
    NSString *coopJson = [onlyFirst convertJSONArrayToString];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        [db executeUpdate:@"INSERT INTO growers (uuid, state, permissions, coop_ids, version) VALUES (?,?,?,?,?)", uuid, @"updated", FULL_PERMISSIONS, coopJson, @(RECORD_VERSION_FOR_GROWER)];
        FMResultSet *results = [db executeQuery:@"SELECT * FROM growers WHERE uuid=?", uuid];
        if ([results next]) {
            grower = [Grower growerWithResultSet:results];
        }
        [results close];
    
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_DATA_GROWERS object:nil];
    
    return grower;
}

// set record availability, returning an array of records needed
+ (NSArray *)setAvailability:(NSArray *)growers {
    Session *session = [Session shared];
    NSMutableArray *records = [[NSMutableArray alloc] initWithArray:growers];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {

        // look at every record in the database, decide whether to keep it or remove it, whether to update its permissions, or whether to
        // request it from the server if it doesn't already exist or if the version has changed
        FMResultSet *results = [db executeQuery:@"SELECT * FROM growers"];
        while ([results next]) {
            BOOL found = NO;
            for (NSInteger i = 0; i < records.count; i++) {
                NSDictionary *grower = [records objectAtIndex:i];
                if (results[@"server_id"] != [NSNull null] && [results[@"server_id"] integerValue] == [grower[@"id"] integerValue]) {
                    [db executeUpdate:@"UPDATE growers SET permissions=? WHERE server_id=?", grower[@"r"], results[@"server_id"]];
                    if (RECORD_VERSION_FOR_GROWER == [results[@"version"] integerValue]) [records removeObjectAtIndex:i];
                    found = YES;
                    break;
                }
            }
            if (! found) [db executeUpdate:@"UPDATE growers SET state='permission_removed' WHERE server_id=?", results[@"server_id"]];
        }
        [results close];

    }];
    
    // only return an array of ids
    NSMutableArray *neededRecords = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < records.count; i++) {
        NSDictionary *grower = [records objectAtIndex:i];
        [neededRecords addObject:grower[@"id"]];
    }
    
    return neededRecords;
}

// store and update growers, returns array of error strings or empty array
+ (NSArray *)storeGrowers:(NSArray *)growers {
    Session *session = [Session shared];
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *grower in growers) {
            NSString *uuid = [grower[@"clientId"] uppercaseString];
            NSString *coopIds = [grower[@"coops"] convertJSONArrayToString];

            FMResultSet *results = [db executeQuery:@"SELECT * FROM growers WHERE uuid=?", uuid];
            if ([results next]) {
                // update the existing record
                if ([results[@"modified_during_sync"] boolValue]) {
                    [db executeUpdate:@"UPDATE growers SET server_id=? WHERE uuid=?", grower[@"id"], results[@"uuid"]];
                } else {
                    [db executeUpdate:@"UPDATE growers SET server_id=?, name=?, state='', first_name=?, middle_name=?, last_name=?, company=?, email=?, coop_ids=?, permissions=?, version=? WHERE uuid=?", grower[@"id"], grower[@"fullName"], grower[@"firstName"], grower[@"middleName"], grower[@"lastName"], grower[@"companyName"], grower[@"email"], coopIds, grower[@"r"], @(RECORD_VERSION_FOR_GROWER), results[@"uuid"]];
                }
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO growers (server_id, name, uuid, first_name, middle_name, last_name, company, email, coop_ids, permissions, version) VALUES (?,?,?,?,?,?,?,?,?,?,?)", grower[@"id"], grower[@"fullName"], uuid, grower[@"firstName"], grower[@"middleName"], grower[@"lastName"], grower[@"companyName"], grower[@"email"], coopIds, grower[@"r"], @(RECORD_VERSION_FOR_GROWER)];
            }
            
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeGrowers: %@", [db lastErrorMessage]]];
            [results close];
        }
    
    }];
    
    return errors;
}

// store grower-farm relationships
+ (NSArray *)storeFarmsForGrowers:(NSArray *)growers {
    Session *session = [Session shared];
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *grower in growers) {
            NSArray *farms = grower[@"farms"];
            for (NSNumber *farm_server_id in farms) {
                FMResultSet *growerResult = [db executeQuery:@"SELECT * FROM growers WHERE server_id=?", grower[@"id"]];
                [growerResult next];
                
                FMResultSet *farmResult = [db executeQuery:@"SELECT * FROM farms WHERE server_id=?", farm_server_id];
                if ([farmResult next]) {
                    FMResultSet *farmGrowerResult = [db executeQuery:@"SELECT * FROM farm_grower WHERE farm_id=? AND grower_id=?", farmResult[@"server_id"], growerResult[@"server_id"]];
                    if (! [farmGrowerResult next]) {
                        [db executeUpdate:@"INSERT INTO farm_grower (farm_id,grower_id) VALUES (?,?)", farmResult[@"server_id"], growerResult[@"server_id"]];
                        if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeGrowerFarm (storeGrowers method): %@", [db lastErrorMessage]]];
                    }
                } else {
                    [errors addObject:[NSString stringWithFormat:@"storeGrowerFarm: No farm %@ for grower server id %@", farm_server_id, grower[@"id"]]];
                }
                [farmResult close];
                [growerResult close];
            }
        }
    
    }];
    
    return errors;
}

// returns an array of modified growers or nil if there are none
+ (NSArray *)modifiedGrowers {
    Session *session = [Session shared];
    NSMutableArray *modifiedRecords = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM growers WHERE state='updated'"];
        while ([results next]) {
            if ((results[@"first_name"] != [NSNull null] && results[@"last_name"] != [NSNull null]) || results[@"company"] != [NSNull null]) {
                NSMutableDictionary *record = [[NSMutableDictionary alloc] init];
                
                record[@"id"] = results[@"server_id"];
                record[@"clientId"] = results[@"uuid"];
                record[@"firstName"] = results[@"first_name"];
                record[@"middleName"] = results[@"middle_name"];
                record[@"lastName"] = results[@"last_name"];
                record[@"companyName"] = results[@"company"];
                record[@"email"] = results[@"email"];
                record[@"coops"] = [NSArray arrayFromJSONString:results[@"coop_ids"]];
                
                [modifiedRecords addObject:record];
            }
        }
        [results close];
    
    }];
    
    return ([modifiedRecords count] == 0 ? nil : modifiedRecords);
}

// check if there are incomplete grower records
+ (BOOL)hasIncompleteGrowers {
    Session *session = [Session shared];
    __block BOOL error = NO;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {

        FMResultSet *results = [db executeQuery:@"SELECT * FROM growers WHERE state='updated' AND first_name IS NULL AND last_name IS NULL AND company IS NULL"];
        if ([results next]) error = YES;
        [results close];

    }];
    
    return error;
}

// Returns an array of deleted grower server ids or nil if there are none. Will only return records where the state is marked 'deleted' and there is a server_id.
// Records without a server id are handled with a special upload flag.
+ (NSArray *)deletedGrowers {
    Session *session = [Session shared];
    NSMutableArray *deletedRecords = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM growers WHERE state='deleted'"];
        while ([results next]) {
            [deletedRecords addObject:results[@"server_id"]];
        }
        [results close];
    
    }];
    
    return ([deletedRecords count] == 0 ? nil : deletedRecords);
}

// delete growers
+ (void)deleteGrowers:(NSArray *)growers {
    Session *session = [Session shared];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSNumber *growerId in growers) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM growers WHERE server_id=?", growerId];
            if ([results next]) {
                [Farm deleteFarmsForGrower:results[@"server_id"]];
                [db executeUpdate:@"DELETE FROM growers WHERE server_id=?", results[@"server_id"]];
            }
            [results close];
        }
        
        FMResultSet *results = [db executeQuery:@"SELECT * FROM growers WHERE state='permission_removed'"];
        while ([results next]) {
            [Farm deleteFarmsForGrower:results[@"server_id"]];
            [db executeUpdate:@"DELETE FROM growers WHERE server_id=?", results[@"server_id"]];
        }
        [results close];
    
    }];
}

// returns a Grower object given a database query
+ (Grower *)growerWithResultSet:(FMResultSet *)results {
    
    return [[Grower alloc] initWithId:[results[@"server_id"] integerValue] firstname:results[@"first_name"] middlename:results[@"middle_name"] lastname:results[@"last_name"] company:results[@"company"] email:results[@"email"] permissions:results[@"permissions"] coopIds:[NSArray arrayFromJSONString:results[@"coop_ids"]]];
}

// clear the modified_during_state flags on all records
+ (void)clearModifiedDuringSync {
    Session *session = [Session shared];
    NSNumber *trueAsObject = [NSNumber numberWithBool:YES];
    NSNumber *falseAsObject = [NSNumber numberWithBool:NO];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM growers WHERE modified_during_sync=?", trueAsObject];
        while ([results next]) {
            [db executeUpdate:@"UPDATE growers SET modified_during_sync=? WHERE server_id=?", falseAsObject, results[@"server_id"]];
        }
        [results close];
    
    }];
}

// returns YES if this grower is a duplicate of another
+ (BOOL)isDuplicateEmail:(NSString *)email {
    Session *session = [Session shared];
    __block BOOL isDuplicate = NO;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *results = [db executeQuery:@"SELECT * FROM growers WHERE email=? AND state!='deleted' COLLATE NOCASE", email];
        if ([results next]) {
            isDuplicate = YES;
        }
        [results close];
    
    }];
    
    return isDuplicate;
}

// returns an array of growers in alphabetical order by displayName; if onlyNamedGrowers is YES than skip blank displayNames
+ (NSArray <Grower *> *)allGrowersWithNamesOnly:(BOOL)onlyNamedGrowers {
    Session *session = [Session shared];
    NSMutableArray *growers = [[NSMutableArray alloc] init];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM growers WHERE state!='deleted'"];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:query];
        while ([results next]) {
            Grower *grower = [self growerWithResultSet:results];
            if (! onlyNamedGrowers || (onlyNamedGrowers && grower.displayName.length > 0)) [growers addObject:grower];
        }
        [results close];
    
    }];
    
    // sort the growers by displayName descending order
    [growers sortUsingComparator:^NSComparisonResult(Grower *grower1, Grower *grower2) {
        return [grower1.displayName compare:grower2.displayName];
    }];
    
    return growers;
}

// returns a Grower object given a database id
+ (Grower *)growerForId:(NSInteger)growerId {
    Session *session = [Session shared];
    __block Grower *grower;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM growers WHERE server_id=? AND state != 'deleted'", @(growerId)];
        if ([results next]) {
            grower = [self growerWithResultSet:results];
        }
        [results close];
    
    }];
    
    return grower;
}

// returns a Grower object given a farm's database id
+ (Grower *)growerForFarmId:(NSInteger)farmId {
    Session *session = [Session shared];
    __block Grower *grower;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM farm_grower WHERE farm_id=?", @(farmId)];
        if ([results next]) {
            FMResultSet *growerResults = [db executeQuery:@"SELECT * FROM growers WHERE server_id=?", results[@"grower_id"]];
            if ([growerResults next]) grower = [self growerWithResultSet:growerResults];
        }
        [results close];
    
    }];
    
    return grower;
}

@end
