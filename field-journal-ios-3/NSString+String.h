//
//  NSString+String.h
//  Field Journal
//
//  Created by Elia Freedman on 2/3/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface NSString (String)

- (CGFloat)floatVal;                                        // returns the float value of an NSString, removes comma thousands separators
- (NSString *)unformatNumberString;                         // remove comma thousands separator from NSStrings
- (NSString *)formatIntegerString;                          // adds comma thousands separator to a NSString integer number

+ (NSString *)nameForFileType:(NSString *)extension;        // Returns a unique filename. If extension is provided then ".<extension>" added to the end of the file name.

- (NSInteger)containedWithinArray:(NSArray *)array;             // returns the index of the matching array element or NSNotFound

+ (NSString *)stringWithDictionary:(NSDictionary *)data;        // return a string consisting of dictionary data. If an error then returns nil.
+ (NSString *)stringWithArray:(NSArray *)data;                  // return a string consisting of array data. If an error then returns nil.
- (NSDictionary *)dictionaryValue;                              // converts a string to a dictionary. If an error then returns nil.
- (NSArray *)arrayValue;                                        // converts a string to an array. If an error then returns nil.

@end
