//
//  Migration.m
//  Field Journal
//
//  Created by Elia Freedman on 3/10/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Migration.h"


@implementation Migration

+ (void)migrate {
    Session *session = [Session shared];    
    NSInteger version = [session migrationVersion];
    NSInteger currentMigrationVersion = version;
    
    //if (version < 2) {                  // v5.0b62 (pre-release)
    //    [self migrationVersion2];
    //    currentMigrationVersion += 1;
    //}
    
    if (version < currentMigrationVersion) [session setMigrationVersion:currentMigrationVersion];
}

//+ (void)migrationVersion2 {
    //FMDatabase *db = [FMDatabase databaseWithPath:[[Session shared] databasePath]];
    //[db open];
    //...
    //[db close];
//}

@end
