//
//  Grower.h
//  Field Journal
//
//  Created by Elia Freedman on 1/19/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Coop.h"


@interface Grower : NSObject

// returns an array of growers in alphabetical order by displayName; if onlyNamedGrowers is YES than skip blank displayNames
+ (NSArray <Grower *> *)allGrowersWithNamesOnly:(BOOL)onlyNamedGrowers;
+ (Grower *)growerForId:(NSInteger)growerId;                        // returns a Grower object given a database id
+ (Grower *)growerForFarmId:(NSInteger)farmId;                      // returns a Grower object given a farm's database id

+ (Grower *)newGrower;                                              // create and return a new grower

+ (NSArray *)setAvailability:(NSArray *)growers;                    // set record availability, returning an array of records needed
+ (NSArray *)storeGrowers:(NSArray *)growers;                       // store and update growers, returns array of error strings or empty array
+ (NSArray *)storeFarmsForGrowers:(NSArray *)growers;               // store grower-farm relationships

+ (void)deleteGrowers:(NSArray *)growers;

+ (NSArray *)modifiedGrowers;                                       // returns an array of modified growers or nil if there are none
+ (NSArray *)deletedGrowers;                                        // returns an array of deleted growers server ids or nil if there are none
+ (BOOL)hasIncompleteGrowers;                                       // check if there are incomplete grower records
+ (void)clearModifiedDuringSync;                                    // clear the modified_during_state flags on all records

+ (BOOL)isDuplicateEmail:(NSString *)email;                         // returns YES if this grower is a duplicate of another


@property (nonatomic) NSInteger dbId;                               // database id
@property (nonatomic) NSString *displayName;                        // display name
@property (nonatomic) NSString *firstName;                          // grower's first name
@property (nonatomic) NSString *middleName;                         // grower's middle name
@property (nonatomic) NSString *lastName;                           // grower's last name
@property (nonatomic) NSString *company;                            // grower's company name
@property (nonatomic) NSString *email;                              // grower's email address
@property (nonatomic) NSString *permissions;                        // permissions
@property (nonatomic) NSArray <NSNumber *> *coopIds;                // array of coop ids grower is member of, may be empty array

- (NSArray <Coop *> *)coops;                                        // returns array of Coop objects this grower is member of

- (void)save;                                                       // save grower changes
- (void)destroy;                                                    // delete grower
- (BOOL)hasPermissionForType:(NSString *)permissionType;            // returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE

@end
