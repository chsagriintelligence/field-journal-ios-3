//
//  CropStage.h
//  Field Journal
//
//  Created by Elia Freedman on 7/14/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CropStage : NSObject

+ (NSArray <CropStage *> *)allCropStagesForCropId:(NSInteger)cropId;    // returns an array of all crop stages in ordinal order

+ (CropStage *)cropStageForServerId:(NSInteger)serverId;                // returns a CropStage object given a server id
+ (NSArray *)storeCropStages:(NSArray *)cropStages;

@property (nonatomic) NSInteger serverId;
@property (nonatomic) NSInteger cropId;
@property (nonatomic) NSString *code;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *desc;
@property (nonatomic) NSInteger ordinal;

@end
