//
//  Layer.m
//  Field Journal
//
//  Created by Elia Freedman on 2/4/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "Layer.h"
#import "Session.h"
#import "FMDB.h"
#import "UIColor+Color.h"


@implementation Layer

- (id)initWithId:(NSInteger)dbId name:(NSString *)name key:(NSString *)key color:(NSString *)color {
    self = [super init];
    if (! self) return nil;
    
    _dbId = dbId;
    _name = ((NSNull *)name == [NSNull null] ? nil : name);
    _key = key;
    _color = ((NSNull *)color == [NSNull null] ? [UIColor grayColor] : [UIColor colorFromHexString:color]);
    
    NSArray *properties;
    if ([key isEqualToString:@"OBSERVATION_STAND_COUNT"]) properties = @[@"ROW_WIDTH", @"INSTRUCTIONS", @"SAMPLES", @"AVERAGE", @"COMMENT"];
    else if ([key isEqualToString:@"OBSERVATION_CORN_YIELD"]) properties = @[@"ROW_WIDTH", @"INSTRUCTIONS", @"SAMPLES", @"HARVEST_EARS", @"BUSHEL_SIZE", @"AVERAGE", @"BUSHELS_PER_ACRE"];
    else if ([key isEqualToString:@"OBSERVATION_DISEASE"]) properties = @[@"NAME", @"DENSITY", @"COMMENT"];
    else if ([key isEqualToString:@"OBSERVATION_WEED"]) properties = @[@"NAME", @"DENSITY", @"STAGE", @"HEIGHT", @"COMMENT"];
    else if ([key isEqualToString:@"OBSERVATION_PEST"]) properties = @[@"NAME", @"DENSITY", @"STAGE", @"INFESTATION_PERCENTAGE", @"COMMENT"];
    else properties = @[@"NAME", @"COMMENT"];
    _properties = properties;

    return self;
}

// store and update layers, returns array of error strings or empty array
+ (NSArray *)storeLayers:(NSArray *)layers {
    Session *session = [Session shared];
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        for (NSDictionary *layer in layers) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM layers WHERE server_id=?", layer[@"id"]];
            if ([results next]) {
                // update the existing record
                [db executeUpdate:@"UPDATE layers SET key=?,name=?,type=?,color=?,permissions=?,display_order=? WHERE server_id=?", layer[@"key"], layer[@"name"], layer[@"type"], layer[@"color"], layer[@"r"], layer[@"displayOrder"], results[@"server_id"]];
            } else {
                // no record found, add a new one
                [db executeUpdate:@"INSERT INTO layers (server_id,key,name,type,color,permissions,display_order) VALUES (?,?,?,?,?,?,?)", layer[@"id"], layer[@"key"], layer[@"name"], layer[@"type"], layer[@"color"], layer[@"r"], layer[@"displayOrder"]];
            }
            if ([db hadError]) [errors addObject:[NSString stringWithFormat:@"storeLayers: %@", [db lastErrorMessage]]];
            [results close];
        }
    
    }];
    
    return errors;
}

// returns a Layer object given a database query
+ (Layer *)layerWithResultSet:(FMResultSet *)results {
    return [[Layer alloc] initWithId:[results[@"server_id"] integerValue] name:results[@"name"] key:results[@"key"] color:results[@"color"]];
}

// returns an array of observation layers
+ (NSArray <Layer *> *)allObservationLayers {
    Session *session = [Session shared];
    NSMutableArray *layers = [[NSMutableArray alloc] init];
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM layers WHERE permissions LIKE '%c%' ORDER BY display_order"];
        while ([results next]) {
            Layer *layer = [self layerWithResultSet:results];
            [layers addObject:layer];
        }
        [results close];
    
    }];
    
    return layers;
}


// returns a Layer object given a database id
+ (Layer *)layerForId:(NSInteger)layerId {
    Session *session = [Session shared];
    __block Layer *layer;
    
    FMDatabaseQueue *queue = [session databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
    
        FMResultSet *results = [db executeQuery:@"SELECT * FROM layers WHERE server_id=?", @(layerId)];
        if ([results next]) {
            layer = [self layerWithResultSet:results];
        }
        [results close];
    
    }];
    
    return layer;
}

@end
