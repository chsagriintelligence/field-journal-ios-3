//
//  Session.h
//  field-journal-ios-3
//
//  Created by Elia Freedman on 1/18/17.
//  Copyright © 2017 CHS, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


typedef enum {
    RecordTypeNone = 0,
    RecordTypeCoop,
    RecordTypeGrower,
    RecordTypeFarm,
    RecordTypeField,
    RecordTypeReport,
    RecordTypeSoilSample,
    
    RecordTypeObservationStandCount,
    RecordTypeObservationCornYield,
    RecordTypeObservationDisease,
    RecordTypeObservationDisorder,
    RecordTypeObservationPest,
    RecordTypeObservationWeed,
    RecordTypeObservationOther
} RecordType;


@interface Session : NSObject <CLLocationManagerDelegate>

+ (Session *)shared;
+ (NSString *)UUID;                                                                             // returns a UUID
+ (NSString *)appVersionAndBuild;                                                               // returns a string representing the app version and build


// database
@property (nonatomic) NSString *storagePath;                                                    // path to on-device storage
@property (nonatomic) NSString *imagesPath;                                                     // path to on-device image storage
@property (nonatomic) NSString *databasePath;                                                   // path to the database
@property (nonatomic) FMDatabaseQueue *databaseQueue;                                           // singleton db queue for multithreading

- (void)updateFiles;                                                                            // check on database, update js and settings if build changed, handle migrations
- (void)createSettings;                                                                         // set the default settings, database must exist first
- (void)destroy;                                                                                // destroy session data


// authenticate
@property (nonatomic) NSString *emailAddress;                                                   // email address
@property (nonatomic) NSString *authToken;                                                      // authorization token or empty string if no token
- (BOOL)hasAuthToken;                                                                           // return YES if there is a token, false otherwise
- (void)deleteAuthToken;                                                                        // delete the authorization token from the keychain
@property (nonatomic) NSString *pinCode;                                                        // pin code required to log into the app
@property (nonatomic) NSString *pinCodeActivatedAt;                                             // last time the pin code was activated


// sync
@property (nonatomic) NSString *domain;                                                         // domain endpoint for the API: testing, production (or empty string) or staging, default: empty string
#define SESSION_DOMAIN_PRODUCTION               @"production"
#define SESSION_DOMAIN_TESTING                  @"testing"
#define SESSION_DOMAIN_STAGING                  @"staging"

@property (nonatomic) NSInteger migrationVersion;                                               // current database migration version
@property (nonatomic) NSInteger serverMigrationVersion;                                         // current server migration version, set to NO_SERVER_MIGRATION_REQUIRED if no migration required at this time
#define NO_SERVER_MIGRATION_REQUIRED            0

@property (nonatomic) NSString *syncedAt;                                                       // UTC datetime of last sync, stored only
@property (nonatomic) NSInteger syncInProgress;                                                 // count of number of syncs in progress
@property (nonatomic) NSArray *syncErrors;                                                      // array of sync error strings


// user and permission
@property (nonatomic) NSInteger currentUser;                                                    // current user's server_id
@property (nonatomic) NSDictionary *userPermissions;                                            // permissions for each record type, combination of [c]reate, [u]pdate, [d]elete or empty string
// for read-only; these are used if not defined for the record itself
- (NSString *)permissionForRecordType:(NSString *)recordType;                                   // returns the permission string (combination of [c]reate, [u]pdate, [d]elete or empty string for
// read-only) for a recordType string, or nil if that recordType doesn't exist
- (BOOL)hasCreatePermissionsForType:(NSString *)recordType;                                     // returns YES if the user has create permissions for record type

#define RECORD_TYPE_USERS                       @"users"
#define RECORD_TYPE_GROWERS                     @"growers"
#define RECORD_TYPE_FARMS                       @"farms"
#define RECORD_TYPE_FIELDS                      @"fields"
#define RECORD_TYPE_REPORTS                     @"reports"
#define RECORD_TYPE_OBSERVATIONS                @"observations"
#define RECORD_TYPE_IMAGES                      @"images"
#define RECORD_TYPE_FIELD_IMAGES                @"fieldImages"
#define RECORD_TYPE_REPORT_PUBLISH_TO_GROWER    @"reportPublishToGrower"
#define RECORD_TYPE_REPORT_PUBLISH_TO_INTEL     @"reportPublishToIntel"
#define RECORD_TYPE_COOPS                       @"coops"

#define PERMISSION_CREATE                       @"c"
#define PERMISSION_READ                         @"r"
#define PERMISSION_UPDATE                       @"u"
#define PERMISSION_DELETE                       @"d"
#define PERMISSION_NONE                         @""


// location
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocation *lastLocation;

- (BOOL)locationServiceEnabled;                                                                 // YES if location services authorized and enabled
- (double)currentLatitude;
- (double)currentLongitude;
- (NSDictionary *)currentLocation;                                                               // dict w/ latest lat/long NSNumber doubles

@end
