//
//  Field.h
//  Field Journal
//
//  Created by Elia Freedman on 1/19/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Field : NSObject


// returns an array of all fields in alphabetical order; if onlyNamedFields is YES than skip blank names
+ (NSArray <Field *> *)allFieldsWithNamesOnly:(BOOL)onlyNamedFields;

// returns an array of all fields in alphabetical order, organized by farm in alphabetical order. If a search string is provided
+ (NSArray <NSDictionary *> *)allFieldsAndFarmsWithSearch:(NSString *)searchText;

// returns an array of all fields in proximity to the current location
+ (NSArray <Field *> *)allFieldsInProximityToCurrentLocation;

+ (Field *)fieldForId:(NSInteger)fieldId;           // returns a Field object given a database id

// create and return a new field given a farmId and a dictionary of latitude and longitude coordinates
+ (Field *)newFieldForFarmId:(NSInteger)farmId coordinates:(NSDictionary *)coordinates;

+ (NSArray *)setAvailability:(NSArray *)fields;     // set record availability, returning an array of records needed
+ (NSArray *)storeFields:(NSArray *)fields;         // store and update fields, assumes farms have been stored first, returns array of error strings or empty array

+ (void)deleteFields:(NSArray *)fields;             // delete fields
+ (void)deleteFieldsForFarm:(NSNumber *)farmId;     // deletes all fields with a given farm id

+ (NSArray *)modifiedFields;                        // returns an array of modified fields or nil if there are none
+ (NSArray *)deletedFields;                         // returns an array of deleted fields server ids or nil if there are none
+ (BOOL)hasIncompleteFields;                        // check if there are incomplete field records
+ (void)clearModifiedDuringSync;                    // clear the modified_during_state flags on all records


@property (nonatomic) NSInteger dbId;               // database id
@property (nonatomic) NSInteger serverId;           // server's database id
@property (nonatomic) NSString *name;               // field name
@property (nonatomic) NSDictionary *boundary;       // field boundary
@property (nonatomic) NSNumber *distance;           // meters from user to nearest boundary point
@property (nonatomic) NSInteger farmId;             // id of the farm
@property (nonatomic) NSString *permissions;        // permissions

- (void)save;                                                       // save field changes
- (void)updateFarm:(NSInteger)farmId;                               // updates the farm given a new farmId
- (void)destroy;                                                    // delete field
- (BOOL)hasPermissionForType:(NSString *)permissionType;            // returns YES if the user has the permission type: PERMISSION_CREATE, PERMISSION_READ, PERMISSION_UPDATE, PERMISSION_DELETE
- (BOOL)isDuplicateName:(NSString *)name;                           // returns YES if this field is a duplicate of another assigned to the same farm

@end
