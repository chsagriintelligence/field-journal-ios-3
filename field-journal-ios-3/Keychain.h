//
//  Keychain.h
//  powerone-ios
//
//  Created by Elia Freedman on 1/7/15.
//  Copyright (c) 2015 Infinity Softworks, Inc. All rights reserved.
//
//  http://stackoverflow.com/questions/5247912/saving-email-password-to-keychain-in-ios
//

#import <Foundation/Foundation.h>

@interface Keychain : NSObject

// can save any data, including an NSDictionary, service is any unique NSString
+ (void)save:(NSString *)service data:(id)data group:(NSString *)group;
+ (id)load:(NSString *)service group:(NSString *)group;
+ (void)destroy:(NSString *)service;

@end
