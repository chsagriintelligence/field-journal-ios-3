//
//  NSDateFormatter+DateFormat.m
//  Field Journal
//
//  Created by Elia Freedman on 1/16/15.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "NSDateFormatter+DateFormat.h"


#define DATE_FORMAT         @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"
#define DATE_FORMAT_PRINTED @"MMM d, yyyy h:mm a"
#define DATE_UTC_TIMEZONE   @"UTC"


@implementation NSDateFormatter (DateFormat)

+ (NSString *)utcDatetimeToLocalDatetime:(NSString *)utcString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // resets the system time zone for the app so will match if there is a time zone change
    [NSTimeZone resetSystemTimeZone];
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:DATE_UTC_TIMEZONE];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:DATE_FORMAT];
    NSDate *utc = [dateFormatter dateFromString:utcString];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:DATE_FORMAT_PRINTED];

    return [dateFormatter stringFromDate:utc];
}

// is utcString date today's date?
+ (BOOL)dateIsToday:(NSString *)utcString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // resets the system time zone for the app so will match if there is a time zone change
    [NSTimeZone resetSystemTimeZone];
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:DATE_UTC_TIMEZONE];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:DATE_FORMAT];
    NSDate *utc = [dateFormatter dateFromString:utcString];

    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:utc];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    if([today isEqualToDate:otherDate]) return true;
    return false;
}

// returns the date-time stamp in UTC for the given date-time as a string
+ (NSString *)timestampInUTCfor:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:DATE_UTC_TIMEZONE];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:DATE_FORMAT];
    return [dateFormatter stringFromDate:date];
}

// returns the current date-time stamp in UTC as a string
+ (NSString *)nowInUTC {
    return [NSDateFormatter timestampInUTCfor:[NSDate date]];
}

@end
