//
//  GeoJSON.h
//  Field Journal
//
//  Created by Elia Freedman on 2/2/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ArcGIS/ArcGIS.h>

@interface GeoJSON : NSObject

// given a latitude, longitude dictionary, return a GeoJSON dictionary
+ (NSDictionary *)pointFromCoordinates:(NSDictionary *)coordinates;

// given an array of @"latitude", @"longitude" dictionaries, return a GeoJSON Polygon dictionary
+ (NSDictionary *)polygonFromCoordinates:(NSArray <NSDictionary *> *)coordinates;
// converts an AGSPolygon to an equivalent GeoJSON polygon
+ (NSDictionary *)polygonFromAGSPolygon:(AGSPolygon *)poly;
// iterates through complex nested shapes, turns into series of simple shapes
+ (void)forEachShapeIn:(NSObject *)container do:(void (^)(NSDictionary *shape))callback;
// iterates through complex nested shapes, turns into series of single points
+ (void)forEachPointIn:(NSObject *)container do:(void (^)(double lat, double lon))callback;

@end
