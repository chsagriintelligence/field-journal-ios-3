//
//  GeoJSON.m
//  Field Journal
//
//  Created by Elia Freedman on 2/2/16.
//  Copyright © 2016 Paradigm isr, Inc. All rights reserved.
//

#import "GeoJSON.h"

@implementation GeoJSON

// given a @"latitude", @"longitude" dictionary, return a GeoJSON Point dictionary
//{
//    "type":"Point",
//    "coordinates": [-99.531117,44.524905]		(lon, lat)
//}
+ (NSDictionary *)pointFromCoordinates:(NSDictionary *)coordinates {
    NSNumber *latitude = coordinates[@"latitude"];
    NSNumber *longitude = coordinates[@"longitude"];
    NSDictionary *geoJSONPoint = @{@"type" : @"Point", @"coordinates" : @[longitude, latitude]};
    return geoJSONPoint;
}

// given an array of @"latitude", @"longitude" dictionaries, return a GeoJSON Polygon dictionary
// polygon is an array of arrays (region) of arrays (point).  The first region is the boundary, later regions are the holes in the
// polygon (probably won't ever see that in this app and this method does not consider any additional holes).
//{
//    "type":"Polygon",
//    "coordinates": [
//        [
//            [-100.193666324327,44.8381104080829],		(lon, lat)
//            [-100.193697206316,44.8381317435771],		(lon, lat)
//            [-100.193666771455,44.8381320617278],		(lon, lat)
//            [-100.183804785577,44.8381914000986],		(lon, lat)
//            [-100.183682589489,44.8381710067169],		(lon, lat)
//            [-100.18355996281,44.8381289602481],		(lon, lat)
//         ]
//    ]
//}
+ (NSDictionary *)polygonFromCoordinates:(NSArray <NSDictionary *> *)coordinates {
    NSMutableArray *points = [[NSMutableArray alloc] init];
    for (NSDictionary *coordinate in coordinates) {
        NSNumber *latitude = coordinate[@"latitude"];
        NSNumber *longitude = coordinate[@"longitude"];
        [points addObject:@[longitude, latitude]];
    }

    NSDictionary *geoJSONPolygon = @{@"type" : @"Polygon", @"coordinates" : @[points]};
    return geoJSONPolygon;
}

// converts an AGSPolygon to an equivalent GeoJSON polygon
+ (NSDictionary *)polygonFromAGSPolygon:(AGSPolygon *)poly {
    NSMutableArray *regions = [NSMutableArray array];
    
    // each ring of AGSPolygon will translate to one region in GeoJSON polygon
    for (NSInteger ring = 0; ring < poly.numRings; ++ring) {
        NSMutableArray *region = [NSMutableArray array];
        NSInteger points = [poly numPointsInRing:ring];
        
        for (NSInteger ptIndex = 0; ptIndex < points; ++ptIndex) {
            // extract next point from AGSPolygon in webMercator format
            AGSPoint *pt = [poly pointOnRing:ring atIndex:ptIndex];
            // convert webMercator coords back to raw lat/lon degrees
            pt = (AGSPoint *)[[AGSGeometryEngine defaultGeometryEngine] projectGeometry:pt
                    toSpatialReference:[AGSSpatialReference wgs84SpatialReference]];
            // convert raw lat/lon to GeoJSON point coordinate pair and add to region
            NSNumber *latitude = [NSNumber numberWithDouble:pt.y];
            NSNumber *longitude = [NSNumber numberWithDouble:pt.x];
            [region addObject:@[longitude, latitude]];
        }
        
        [regions addObject:region];
    }
    
    NSDictionary *geoJSONPolygon = @{@"type" : @"Polygon", @"coordinates" : regions};
    return geoJSONPolygon;
}


// iterates through complex nested shapes, turns into series of simple shapes
// arrays of GeoJSON objects and contents of GeometryCollection objects become series of simple shapes
// MultiPolygon objects become series of simple Polygons
// callback is invoked for each simple Point or Polygon extracted from all contents
+ (void)forEachShapeIn:(NSObject *)container do:(void (^)(NSDictionary *shape))callback {
    if (! container) return;
    if ([container isKindOfClass:[NSDictionary class]]) {
        NSDictionary *geo = (NSDictionary *)container;
        if ([geo[@"type"] isEqualToString:@"GeometryCollection"]) {
            // process each GeoJSON object contained in collection, even if nested
            [GeoJSON forEachShapeIn:geo[@"geometries"] do:callback];
        } else if ([geo[@"type"] isEqualToString:@"MultiPolygon"]) {
            // process each polygon in MultiPolygon as separate simple Polygon objects
            for (NSArray *polygon in geo[@"coordinates"]) {
                callback(@{@"type": @"Polygon", @"coordinates": polygon});
            }
        } else {
            // invoke callback for simple Point or Polygon objects
            callback(geo);
        }
    } else if ([container isKindOfClass:[NSArray class]]) {
        // process each item in array of GeoJSON objects
        NSArray *array = (NSArray *)container;
        for (NSObject *element in array) {
            [GeoJSON forEachShapeIn:element do:callback];
        }
    }
}

// iterates through complex nested shapes, turns into series of single lat/lon points
+ (void)forEachPointIn:(NSObject *)container do:(void (^)(double lat, double lon))callback {
    if (! container) return;
    if ([container isKindOfClass:[NSDictionary class]]) {
        NSDictionary *geo = (NSDictionary *)container;
        if ([geo[@"type"] isEqualToString:@"GeometryCollection"]) {
            // process each GeoJSON object contained in collection, even if nested
            [GeoJSON forEachPointIn:geo[@"geometries"] do:callback];
        } else {
            // process any depth of arrays of point coordinate pairs
            [GeoJSON forEachPointIn:geo[@"coordinates"] do:callback];
        }
    } else if ([container isKindOfClass:[NSArray class]]) {
        NSArray *array = (NSArray *)container;
        if ([array count]) {
            if ([array[0] isKindOfClass:[NSNumber class]]) {
                // finally drilled down to a lat/lon coordinate pair, process it
                callback([array[1] doubleValue], [array[0] doubleValue]);
            } else for (NSObject *element in array) {
                // process nested arrays of arrays
                [GeoJSON forEachPointIn:element do:callback];
            }
        }
    }
}

@end
